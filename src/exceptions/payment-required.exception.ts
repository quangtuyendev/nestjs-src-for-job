import { HttpException } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

export class PaymentRequiredException extends HttpException {
  constructor() {
    super({ response: Constants.MESSAGES.failed.auth.user_not_found }, 402);
  }
}
