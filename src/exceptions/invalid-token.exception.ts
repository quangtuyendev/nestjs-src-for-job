import { BadRequestException } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

export class InvalidTokenException extends BadRequestException {
  constructor() {
    super(Constants.MESSAGES.failed.auth.invalid_token);
  }
}
