import { BadRequestException } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

export class UserNotFoundException extends BadRequestException {
  constructor() {
    super(Constants.MESSAGES.failed.auth.user_not_found);
  }
}
