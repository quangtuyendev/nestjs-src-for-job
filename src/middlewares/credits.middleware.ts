import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
  InternalServerErrorException,
  NestMiddleware,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { HttpStatus } from 'src/common/http-status.utils';
import { Constants } from 'src/constants/Constants';
import { PaymentRequiredException } from 'src/exceptions/payment-required.exception';
import { UserNotFoundException } from 'src/exceptions/user-not-found.exception';
import { IUserDecoded } from 'src/modules/auth/interfaces/user-decoded.interface';
import { FileEntity } from 'src/modules/file/entities/file.entity';
import { CreditRoutesCheck } from 'src/modules/file/enums/credit-route-check.enum';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { FirebaseService } from 'src/providers/firebase/firebase.service';
import { Helpers } from 'src/utils/helpers.utils';
import Logger from 'src/utils/logger.utils';
import { Repository } from 'typeorm';

@Injectable()
export class CreditMiddleware implements NestMiddleware {
  private logger: Logger = new Logger(__filename);

  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<UserEntity>,
    @Inject('FILE_REPOSITORY')
    private readonly fileRepository: Repository<FileEntity>,
    private readonly firebaseService: FirebaseService,
  ) {}

  async use(
    req: Request & { user: IUserDecoded; fileRecord: FileEntity },
    res: Response,
    next: NextFunction,
  ): Promise<any> {
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = await this.firebaseService
        .getAuth()
        .verifyIdToken(token);

      const path = req.route.path;
      const { userId } = decodedToken;
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      if (!user) throw new UserNotFoundException();
      if (user.credits <= 0 && path !== CreditRoutesCheck.filesDownload)
        throw new PaymentRequiredException();
      switch (path) {
        case CreditRoutesCheck.filesVerify:
          try {
            const { id } = req.params;
            const file = await this.fileRepository.findOne({
              where: { id: parseInt(id) },
              relations: {
                user: true,
              },
            });
            if (!file) {
              throw new BadRequestException(
                Constants.MESSAGES.failed.file.file_not_found,
              );
            }
            if (decodedToken.userId !== file.user.id) {
              throw new ForbiddenException(
                Constants.MESSAGES.failed.file.verify_not_allowed,
              );
            }
            const { status, requiredAmount } = Helpers.checkUserCredits(
              user.credits,
              file.total,
            );
            if (status) {
              return next();
            }
            return res.status(200).json({
              status: HttpStatus.failed,
              error: {
                ...Constants.MESSAGES.failed.user
                  .additional_verify_credit_required,
                additionalInfos: {
                  credits: requiredAmount,
                },
              },
            });
          } catch (error) {
            throw error;
          }
        case CreditRoutesCheck.filesQuickVerify:
          try {
            const { status, requiredAmount } = Helpers.checkUserCredits(
              user.credits,
              req.body.emails.length,
            );
            if (status) return next();
            return res.status(200).json({
              status: HttpStatus.failed,
              error: {
                ...Constants.MESSAGES.failed.user
                  .additional_verify_credit_required,
                additionalInfos: {
                  credits: requiredAmount,
                },
              },
            });
          } catch (error) {
            throw error;
          }
        case CreditRoutesCheck.filesDownload:
          try {
            const { id } = req.params;
            const file = await this.fileRepository.findOne({
              where: { id: parseInt(id) },
              relations: {
                user: true,
              },
            });
            if (!file) {
              throw new BadRequestException(
                Constants.MESSAGES.failed.file.file_not_found,
              );
            }
            if (decodedToken.userId !== file.user.id) {
              throw new ForbiddenException(
                Constants.MESSAGES.failed.file.download_not_allowed,
              );
            }
            if (file.downloadAvailable) {
              req.fileRecord = file;
              return next();
            }
            const { status, requiredAmount } = Helpers.checkUserCredits(
              user.credits,
              file.credits,
            );
            if (status) {
              await this.userRepository.decrement(
                {
                  id: user.id,
                },
                'credits',
                file.credits,
              );
              await this.fileRepository.update(file.id, {
                downloadAvailable: true,
              });
              req.fileRecord = file;
              return next();
            }
            return res.status(200).json({
              status: HttpStatus.failed,
              error: {
                ...Constants.MESSAGES.failed.user
                  .additional_download_credit_required,
                additionalInfos: {
                  credits: requiredAmount,
                },
              },
            });
          } catch (error) {
            throw error;
          }
        case CreditRoutesCheck.filesUpload:
        case CreditRoutesCheck.filesCheckDuplicate:
          return next();
        default:
          throw new BadRequestException();
      }
    } catch (error) {
      this.logger.error(error);
      throw new InternalServerErrorException(error);
    }
  }
}
