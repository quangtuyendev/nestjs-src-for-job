export default () => ({
  port: process.env.PORT,
  host: process.env.HOST,
  nodeEnv: process.env.NODE_ENV,
  cookieSignSecret: process.env.COOKIES_SIGN_SECRET,
  database: {
    dbService: process.env.DB_SERVICE,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    name: process.env.DB_NAME,
  },
  paddle: {
    vendorId: process.env.VENDOR_ID,
    vendorAuthCode: process.env.VENDOR_AUTH_CODE,
    baseApiUri: process.env.PADDLE_BASE_API_URI,
    publicKey: process.env.PADDLE_PUBLIC_KEY,
  },
  email: {
    senderClientId: process.env.EMAIL_SENDER_CLIENT_ID,
    senderClientSecret: process.env.EMAIL_SENDER_CLIENT_SECRET,
    refreshToken: process.env.EMAIL_SENDER_REFRESH_TOKEN,
    oauthUri: process.env.EMAIL_SENDER_OAUTH_URI,
  },
  kafka: {
    clientId: process.env.KAFKA_CLIENT_ID,
    brokers: process.env.KAFKA_BROKERS,
  },
  paypal: {
    clientId: process.env.PAYPAL_CLIENT_ID,
    appSecret: process.env.PAYPAL_APP_SECRET,
    baseUri: process.env.PAYPAL_BASE_URI,
  },
  firebase: {
    apiKey: process.env.FIREBASE_API_KEY,
    authUri: process.env.FIREBASE_AUTH_URI,
    secureTokenUri: process.env.FIREBASE_SECURE_TOKEN_URI,
  },
  gcp: {
    bucketName: process.env.BUCKET_NAME,
  },
});
