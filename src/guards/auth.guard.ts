import {
  CanActivate,
  ExecutionContext,
  HttpException,
  Injectable,
} from '@nestjs/common';
import { Constants } from 'src/constants/Constants';
import { FirebaseAuthErrorCode } from 'src/modules/auth/enums/index.enum';
import { FirebaseService } from 'src/providers/firebase/firebase.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly firebaseService: FirebaseService) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    try {
      const token = req.headers.authorization?.split(' ')[1];
      const decodedToken = await this.firebaseService
        .getAuth()
        .verifyIdToken(token);
      req.user = {
        uid: decodedToken.uid,
        userId: decodedToken.userId,
        email: decodedToken.email,
        token,
      };
      return true;
    } catch (error) {
      let errorCode: number;
      let customError: { message: string; code?: number };
      switch (error.code) {
        case FirebaseAuthErrorCode.idTokenExpired:
          errorCode = 401;
          customError = {
            message: error.code,
          };
          break;
        case FirebaseAuthErrorCode.invalidIdToken:
          errorCode = 401;
          customError = {
            message: error.code,
          };
          break;
        case FirebaseAuthErrorCode.authArgumentError:
          errorCode = 400;
          customError = {
            message: error.code,
            code: Constants.APP_CUSTOM_CODES.firebaseTokenArgumentError,
          };
          break;
        default:
          errorCode = 500;
          customError = {
            message: error.code,
          };
          break;
      }
      throw new HttpException({ response: customError }, errorCode);
    }
  }
}
