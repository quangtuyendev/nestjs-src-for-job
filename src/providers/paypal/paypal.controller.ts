import { Body, Controller, Post } from '@nestjs/common';
import { HttpStatus } from 'src/common/http-status.utils';
import { PaypalService } from './paypal.service';

@Controller('')
export class PaypalController {
  constructor(private readonly paypalService: PaypalService) {}

  @Post('webhook')
  async webhook(
    @Body('event_type') eventType: string,
    @Body('resource') { id }: { id: string },
  ): Promise<{ status: HttpStatus }> {
    const response = await this.paypalService.webhook(eventType, { id });
    return response;
  }
}
