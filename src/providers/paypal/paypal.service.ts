import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Inject,
  Injectable,
  InternalServerErrorException,
  forwardRef,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  Order,
  PurchaseUnit,
} from '@paypal/checkout-server-sdk/lib/orders/lib';
import { SubscriptionDetail } from '@paypal/paypal-js';
import { HttpStatus } from 'src/common/http-status.utils';
import { TransactionEntity } from 'src/modules/transaction/entities/transaction.entity';
import { UserService } from 'src/modules/user/user.service';
import Logger from 'src/utils/logger.utils';
import { Repository } from 'typeorm';
import { PaypalWebhookEvents } from './enums/paypal-webhook-events.enum';
import { ICaptureSubscription } from './interfaces/capture-subscription.interface';

@Injectable()
export class PaypalService {
  private logger: Logger = new Logger(__filename);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    @Inject(forwardRef(() => UserService))
    private userService: UserService,
    @Inject('TRANSACTION_REPOSITORY')
    private readonly transactionRepository: Repository<TransactionEntity>,
  ) {}

  async webhook(eventType: string, resource: { id: string }): Promise<any> {
    const FUNC_NAME = 'webhook';
    this.logger.info(
      `${FUNC_NAME}: ${JSON.stringify({
        eventType,
        resource,
      })}`,
    );
    const { id } = resource;
    try {
      if (eventType === PaypalWebhookEvents.subscriptionActivated) {
        const existingTransaction = await this.transactionRepository.findOne({
          where: { orderId: id },
        });
        if (existingTransaction) {
          this.logger.debug(`${FUNC_NAME}: Order Id: "${id}" already exist`);
          return {
            status: HttpStatus.failed,
          };
        }
        const captureData = await this.captureSubscription(id);
        if (!captureData.status) {
          throw new BadRequestException({
            message: captureData.name.toLowerCase(),
          });
        }
        await this.userService.createTransaction(captureData);
        return {
          status: HttpStatus.success,
        };
      }
      return {
        status: HttpStatus.failed,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async createOrder(orderInfos: Partial<PurchaseUnit>): Promise<Order> {
    const FUNC_NAME = 'createOrder';
    this.logger.info(`${FUNC_NAME}: ${JSON.stringify(orderInfos)}`);
    try {
      const accessToken = await this.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v2/checkout/orders`;
      const { data } = await this.httpService.axiosRef.post(
        url,
        {
          intent: 'CAPTURE',
          purchase_units: [orderInfos],
        },
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
            'PayPal-Client-Metadata-Id': orderInfos.custom_id,
          },
        },
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async createSubscription(subscriptionInfos: {
    plan_id: string;
    custom_id: string;
    subscriber: {
      email_address: string;
    };
  }): Promise<SubscriptionDetail['value']> {
    const FUNC_NAME = 'createSubscription';
    this.logger.info(`${FUNC_NAME}: ${JSON.stringify(subscriptionInfos)}`);
    try {
      const accessToken = await this.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v1/billing/subscriptions`;
      const { data } = await this.httpService.axiosRef.post(
        url,
        {
          ...subscriptionInfos,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
            Prefer: 'return=representation',
          },
        },
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async capturePayment(orderId: string): Promise<any> {
    const FUNC_NAME = 'capturePayment';
    this.logger.info(`${FUNC_NAME}: ${orderId}`);
    try {
      const accessToken = await this.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v2/checkout/orders/${orderId}/capture`;
      const { data } = await this.httpService.axiosRef.post(
        url,
        {},
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
            Prefer: 'return=representation',
          },
        },
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async captureSubscription(
    subscriptionId: string,
  ): Promise<ICaptureSubscription> {
    const FUNC_NAME = 'captureSubscription';
    this.logger.info(`${FUNC_NAME}: ${subscriptionId}`);
    try {
      const accessToken = await this.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v1/billing/subscriptions/${subscriptionId}`;
      const { data } = await this.httpService.axiosRef.get(url, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${accessToken}`,
          Prefer: 'return=representation',
        },
      });
      return data;
    } catch (error) {
      throw error;
    }
  }

  async cancelUserSubscription(
    subscriptionId: string,
    reason: string,
  ): Promise<{ status: boolean }> {
    const FUNC_NAME = 'cancelUserSubscription';
    this.logger.info(`${FUNC_NAME}: ${subscriptionId}`);
    try {
      const accessToken = await this.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v1/billing/subscriptions/${subscriptionId}/cancel`;
      const response = await this.httpService.axiosRef.post(
        url,
        {
          reason,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${accessToken}`,
          },
        },
      );
      return {
        status: response.status !== 204 ? false : true,
      };
    } catch (error) {
      throw error;
    }
  }

  async generateAccessToken(): Promise<string> {
    const auth = Buffer.from(
      this.configService.get('paypal').clientId +
        ':' +
        this.configService.get('paddle').appSecret,
    ).toString('base64');
    const response = await fetch(
      `${this.configService.get('paypal').baseUri}/v1/oauth2/token`,
      {
        method: 'POST',
        body: 'grant_type=client_credentials',
        headers: {
          Authorization: `Basic ${auth}`,
        },
      },
    );
    const data = await response.json();
    return data.access_token;
  }
}
