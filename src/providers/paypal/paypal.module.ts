import { Module, forwardRef } from '@nestjs/common';
import { PaypalService } from './paypal.service';
import { HttpModule } from '@nestjs/axios';
import { DatabaseModule } from '../typeorm/database.module';
import { DataSource } from 'typeorm';
import { TransactionEntity } from 'src/modules/transaction/entities/transaction.entity';
import { UserModule } from 'src/modules/user/user.module';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    DatabaseModule,
    forwardRef(() => UserModule),
  ],
  providers: [
    PaypalService,
    {
      provide: 'TRANSACTION_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(TransactionEntity),
      inject: ['DATA_SOURCE'],
    },
  ],
  exports: [PaypalService],
})
export class PaypalModule {}
