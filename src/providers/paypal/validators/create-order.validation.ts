import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  amount: Joi.number()
    .integer()
    .min(Constants.PRICING.payAsYouGo.min)
    .max(Constants.PRICING.payAsYouGo.max)
    .required()
    .custom((value, helpers) => {
      if (value % Constants.PRICING.payAsYouGo.step !== 0) {
        return helpers.message(
          Constants.MESSAGES.failed.checkout.amount_divisible.message as any,
        );
      }
      return value;
    })
    .messages({
      'any.required':
        Constants.MESSAGES.failed.checkout.amount_required.message,
      'number.min': Constants.MESSAGES.failed.checkout.amount_range.message,
      'number.max': Constants.MESSAGES.failed.checkout.amount_range.message,
    }),
});
export class CreateOrderValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
