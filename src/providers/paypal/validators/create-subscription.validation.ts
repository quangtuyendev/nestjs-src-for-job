import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  subscriptionPlanId: Joi.string()
    .valid(
      ...Object.values([
        Constants.PRICING.monthly.basic.subscriptionPlanId,
        Constants.PRICING.monthly.standard.subscriptionPlanId,
        Constants.PRICING.monthly.premium.subscriptionPlanId,
      ]),
    )
    .required()
    .messages({
      'any.required':
        Constants.MESSAGES.failed.checkout.subscription_plan_id_required
          .message,
      'any.only':
        Constants.MESSAGES.failed.checkout.invalid_subscription_plan_id.message,
    }),
});
export class CreateSubscriptionValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
