import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  subscriptionId: Joi.string().required().messages({
    'any.required':
      Constants.MESSAGES.failed.checkout.subscription_id_required.message,
  }),
});

export class CaptureSubscriptionValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
