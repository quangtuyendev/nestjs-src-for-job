export interface ICaptureOrder {
  id: string;
  status: string;
  create_time: string;
  payment_source: {
    paypal: {
      name: { given_name: string; surname: string };
      address: { country_code: string };
      email_address: string;
      account_id: string;
    };
  };
  purchase_units: string;
}
