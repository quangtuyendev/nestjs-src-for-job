export interface ICaptureSubscription {
  id: string;
  plan_id: string;
  status: string;
  create_time: string;
  subscriber: {
    name: { given_name: string; surname: string };
    payer_id: string;
    email_address: string;
    shipping_address: {
      address: { country_code: string };
    };
  };
  billing_info: {
    last_payment: {
      amount: { currency_code: string; value: string };
    };
    next_billing_time: string;
  };
  custom_id: string;
  name: string;
}
