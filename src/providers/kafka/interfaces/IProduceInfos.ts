import { Message } from '@nestjs/microservices/external/kafka.interface';

export interface IProduceInfos {
  topic: string;
  messages: Message[];
}
