export interface IConsumeConfig {
  topic: string;
  groupId: string;
  fromBeginning: boolean;
  autoCommit: boolean;
  autoCommitThreshold: number;
  partitionsConsumedConcurrently: number;
  sessionTimeout: number;
}
