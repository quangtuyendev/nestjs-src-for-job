import { DynamicModule, Module, forwardRef } from '@nestjs/common';
import { KafkaService } from './kafka.service';
import { DatabaseModule } from '../typeorm/database.module';
import { FileEntity } from 'src/modules/file/entities/file.entity';
import { DataSource } from 'typeorm';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { FileModule } from 'src/modules/file/file.module';
import { UserPerformanceStatsEntity } from 'src/modules/performance-stats/entities/performance-stats.entity';
import { MailerModule } from '../mailer/mailer.module';
import { EmailVerifyEntity } from 'src/modules/email-verify/entities/email-verify.entity';
import { ResetPasswordEntity } from 'src/modules/reset-password/entities/reset-password.entity';

@Module({})
export class KafkaModule {
  static register(): DynamicModule {
    return {
      imports: [DatabaseModule, forwardRef(() => FileModule), MailerModule],
      module: KafkaModule,
      providers: [
        KafkaService,
        {
          provide: 'FILE_REPOSITORY',
          useFactory: (dataSource: DataSource) =>
            dataSource.getRepository(FileEntity),
          inject: ['DATA_SOURCE'],
        },
        {
          provide: 'USER_REPOSITORY',
          useFactory: (dataSource: DataSource) =>
            dataSource.getRepository(UserEntity),
          inject: ['DATA_SOURCE'],
        },
        {
          provide: 'USER_PERFORMANCE_STATS_REPOSITORY',
          useFactory: (dataSource: DataSource) =>
            dataSource.getRepository(UserPerformanceStatsEntity),
          inject: ['DATA_SOURCE'],
        },
        {
          provide: 'EMAIL_VERIFY_REPOSITORY',
          useFactory: (dataSource: DataSource) =>
            dataSource.getRepository(EmailVerifyEntity),
          inject: ['DATA_SOURCE'],
        },
        {
          provide: 'RESET_PASSWORD_REPOSITORY',
          useFactory: (dataSource: DataSource) =>
            dataSource.getRepository(ResetPasswordEntity),
          inject: ['DATA_SOURCE'],
        },
      ],
      exports: [KafkaService],
    };
  }
}
