import {
  Inject,
  Injectable,
  OnModuleDestroy,
  OnModuleInit,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  CompressionTypes,
  Consumer,
  EachMessagePayload,
  Kafka,
  Producer,
} from 'kafkajs';
import { Constants } from 'src/constants/Constants';
import { EmailVerifyEntity } from 'src/modules/email-verify/entities/email-verify.entity';
import { FileEntity } from 'src/modules/file/entities/file.entity';
import { FileErrorReason } from 'src/modules/file/enums/file-error-reason.enum';
import { FileProcessingStatus } from 'src/modules/file/enums/file-processing-status.enum';
import { FileService } from 'src/modules/file/file.service';
import { UserPerformanceStatsEntity } from 'src/modules/performance-stats/entities/performance-stats.entity';
import { ResetPasswordEntity } from 'src/modules/reset-password/entities/reset-password.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { AuthUtils } from 'src/utils/auth.utils';
import { Helpers } from 'src/utils/helpers.utils';
import Logger from 'src/utils/logger.utils';
import { Repository } from 'typeorm';
import { EmailSendStatus } from '../mailer/enums/email-sending-status.enum';
import { MailerService } from '../mailer/mailer.service';
import { IProduceInfos } from './interfaces/IProduceInfos';

@Injectable()
export class KafkaService implements OnModuleInit, OnModuleDestroy {
  private logger: Logger = new Logger(__filename);

  private client: Kafka;
  private producer: Producer;
  private chunksCombineConsumer: Consumer;
  private fileCleanConsumer: Consumer;
  private verifyFileConsumer: Consumer;
  private uploadFileConsumer: Consumer;
  private emailConfirmConsumer: Consumer;
  private emailResetPasswordConsumer: Consumer;

  constructor(
    private readonly configService: ConfigService,
    private readonly fileService: FileService,
    private readonly mailerService: MailerService,
    @Inject('FILE_REPOSITORY')
    private readonly fileRepository: Repository<FileEntity>,
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<UserEntity>,
    @Inject('USER_PERFORMANCE_STATS_REPOSITORY')
    private readonly userPerformanceStatsRepository: Repository<UserPerformanceStatsEntity>,
    @Inject('EMAIL_VERIFY_REPOSITORY')
    private readonly emailVerifyRepository: Repository<EmailVerifyEntity>,
    @Inject('RESET_PASSWORD_REPOSITORY')
    private readonly resetPasswordRepository: Repository<ResetPasswordEntity>,
  ) {
    this.client = new Kafka({
      clientId: this.configService.get('kafka').clientId,
      brokers: this.configService.get('kafka').brokers.split(','),
      retry: {
        retries: 10,
      },
    });
    this.producer = this.client.producer();
    this.chunksCombineConsumer = this.client.consumer({
      groupId: Constants.KAFKA_CONFIG.groups.chunksUploaded,
      sessionTimeout: 30000,
    });
    this.fileCleanConsumer = this.client.consumer({
      groupId: Constants.KAFKA_CONFIG.groups.fileClean,
      sessionTimeout: 30000,
    });
    this.verifyFileConsumer = this.client.consumer({
      groupId: Constants.KAFKA_CONFIG.groups.emailVerify,
      sessionTimeout: 30000,
    });
    this.uploadFileConsumer = this.client.consumer({
      groupId: Constants.KAFKA_CONFIG.groups.cloudUploadedVerified,
      sessionTimeout: 30000,
    });
    this.emailConfirmConsumer = this.client.consumer({
      groupId: Constants.KAFKA_CONFIG.groups.emailConfirm,
      sessionTimeout: 30000,
    });
    this.emailResetPasswordConsumer = this.client.consumer({
      groupId: Constants.KAFKA_CONFIG.groups.emailResetPassword,
      sessionTimeout: 30000,
    });
  }

  public async sendMessage(infos: IProduceInfos): Promise<void> {
    await this.producer.send({
      topic: infos.topic,
      compression: CompressionTypes.GZIP,
      messages: infos.messages,
    });
  }

  public async connect(): Promise<void> {
    await this.producer.connect();

    await this.chunksCombineConsumer.connect();
    await this.fileCleanConsumer.connect();
    await this.verifyFileConsumer.connect();
    await this.uploadFileConsumer.connect();
    await this.emailConfirmConsumer.connect();
    await this.emailResetPasswordConsumer.connect();

    await this.chunksCombineConsumer.subscribe({
      topic: Constants.KAFKA_CONFIG.topics.chunksUploaded.name,
      fromBeginning: false,
    });

    await this.fileCleanConsumer.subscribe({
      topic: Constants.KAFKA_CONFIG.topics.fileClean.name,
      fromBeginning: false,
    });

    await this.verifyFileConsumer.subscribe({
      topic: Constants.KAFKA_CONFIG.topics.emailVerify.name,
      fromBeginning: false,
    });

    await this.uploadFileConsumer.subscribe({
      topic: Constants.KAFKA_CONFIG.topics.cloudUploadedVerified.name,
      fromBeginning: false,
    });

    await this.emailConfirmConsumer.subscribe({
      topic: Constants.KAFKA_CONFIG.topics.emailConfirm.name,
      fromBeginning: false,
    });

    await this.emailResetPasswordConsumer.subscribe({
      topic: Constants.KAFKA_CONFIG.topics.emailResetPassword.name,
      fromBeginning: false,
    });
  }

  public async disconnect(): Promise<void> {
    await this.producer.disconnect();

    await this.chunksCombineConsumer.disconnect();
    await this.fileCleanConsumer.disconnect();
    await this.verifyFileConsumer.disconnect();
    await this.uploadFileConsumer.disconnect();
    await this.emailConfirmConsumer.disconnect();
    await this.emailResetPasswordConsumer.disconnect();
  }

  public async onModuleInit(): Promise<void> {
    await this.connect();
    await this.chunksCombineConsumer
      .run({
        autoCommit: Constants.KAFKA_CONFIG.topics.chunksUploaded.autoCommit,
        autoCommitThreshold:
          Constants.KAFKA_CONFIG.topics.chunksUploaded.autoCommitThreshold,
        partitionsConsumedConcurrently:
          Constants.KAFKA_CONFIG.topics.chunksUploaded
            .partitionsConsumedConcurrently,
        eachMessage: async (payload: EachMessagePayload): Promise<void> => {
          const FUNC_NAME = 'chunksCombineConsumer';
          this.logger.debug(`${FUNC_NAME}: merge file processing...`);
          const { message } = payload;
          const { id, folderName, fileName, originFileName } = JSON.parse(
            message.value.toString(),
          );
          try {
            const currentFile = await this.fileRepository.findOne({
              where: { id },
            });
            if (!currentFile) {
              return this.logger.debug('No file write previous');
            }
            if (
              currentFile.status !== FileProcessingStatus.chunksUploaded &&
              currentFile.status !== FileProcessingStatus.mergeFileFailed
            )
              return;
            await this.fileService.chunksCombiner(
              folderName,
              fileName,
              originFileName.split('.').pop().toLowerCase(),
            );
            await this.sendMessage({
              topic: Constants.KAFKA_CONFIG.topics.fileClean.name,
              messages: [
                {
                  value: JSON.stringify({
                    id,
                    folderName,
                    fileName,
                  }),
                },
              ],
            });
          } catch (error) {
            try {
              let errorInfoUpdate = {};
              if (error.message === FileErrorReason.readingError) {
                errorInfoUpdate = {
                  status: FileProcessingStatus.fileError,
                  errorReason: FileErrorReason.readingError,
                };
              } else {
                errorInfoUpdate = {
                  status: FileProcessingStatus.mergeFileFailed,
                };
              }
              await this.fileService.updateMergeFileStatus(id, {
                ...errorInfoUpdate,
                lastModifiedStatus: new Date(),
              });
            } catch (error) {
              this.logger.error(`${FUNC_NAME}: ${error}`);
            }
            this.logger.error(`${FUNC_NAME}: ${error}`);
          }
        },
      })
      .catch((error) => {
        this.logger.error(`Consumer connecting error: ${error}`);
      });
    await this.fileCleanConsumer
      .run({
        autoCommit: Constants.KAFKA_CONFIG.topics.fileClean.autoCommit,
        autoCommitThreshold:
          Constants.KAFKA_CONFIG.topics.fileClean.autoCommitThreshold,
        partitionsConsumedConcurrently:
          Constants.KAFKA_CONFIG.topics.fileClean
            .partitionsConsumedConcurrently,
        eachMessage: async (payload: EachMessagePayload): Promise<void> => {
          const FUNC_NAME = 'fileCleanConsumer';
          this.logger.debug(`${FUNC_NAME}: clean file processing...`);
          const { message } = payload;
          const { id, folderName, fileName } = JSON.parse(
            message.value.toString(),
          );
          try {
            const currentFile = await this.fileRepository.findOne({
              where: { id },
            });
            if (!currentFile) {
              return this.logger.debug('No file write previous');
            }
            if (
              currentFile.status === FileProcessingStatus.mergeFileSuccess ||
              currentFile.status === FileProcessingStatus.fileError
            )
              return;
            const fileInfosUploaded = await this.fileService.fileCleaner(
              folderName,
              fileName,
            );
            await this.fileService.updateMergeFileStatus(id, fileInfosUploaded);
          } catch (error) {
            try {
              let errorInfoUpdate = {};
              if (Object.values(FileErrorReason).includes(error.message)) {
                errorInfoUpdate = {
                  status: FileProcessingStatus.fileError,
                  errorReason: error.message,
                };
              } else {
                errorInfoUpdate = {
                  status: FileProcessingStatus.mergeFileFailed,
                };
              }
              await this.fileService.updateMergeFileStatus(id, {
                ...errorInfoUpdate,
                lastModifiedStatus: new Date(),
              });
            } catch (error) {
              this.logger.error(`${FUNC_NAME}: ${error}`);
            }
            this.logger.error(`${FUNC_NAME}: ${error}`);
          }
        },
      })
      .catch((error) => {
        this.logger.error(`Consumer connecting error: ${error}`);
      });
    await this.verifyFileConsumer
      .run({
        autoCommit: Constants.KAFKA_CONFIG.topics.emailVerify.autoCommit,
        autoCommitThreshold:
          Constants.KAFKA_CONFIG.topics.emailVerify.autoCommitThreshold,
        partitionsConsumedConcurrently:
          Constants.KAFKA_CONFIG.topics.emailVerify
            .partitionsConsumedConcurrently,
        eachMessage: async (payload: EachMessagePayload): Promise<void> => {
          const FUNC_NAME = 'verifyFileConsumer';
          this.logger.debug(`${FUNC_NAME}: verify email processing...`);
          const { message } = payload;
          const parsedValue = JSON.parse(message.value.toString());
          const { id } = parsedValue;
          try {
            const currentFile = await this.fileRepository.findOne({
              where: {
                id,
                status: FileProcessingStatus.fileVerifying,
              },
            });
            if (!currentFile) {
              return this.logger.debug(`${FUNC_NAME}: No file found`);
            }
            await this.fileService.emailVerifyHandler(parsedValue);
          } catch (error) {
            try {
              await this.fileRepository.update(id, {
                status: FileProcessingStatus.fileVerifyFailed,
                lastModifiedStatus: new Date(),
              });
            } catch (error) {
              this.logger.error(`${FUNC_NAME}: deleting... ${error.message}`);
            }
            this.logger.error(`${FUNC_NAME}: ${error.stack}`);
          }
        },
      })
      .catch((error) => {
        this.logger.error(`Consumer connecting error: ${error.stack}`);
      });
    await this.uploadFileConsumer
      .run({
        autoCommit:
          Constants.KAFKA_CONFIG.topics.cloudUploadedVerified.autoCommit,
        autoCommitThreshold:
          Constants.KAFKA_CONFIG.topics.cloudUploadedVerified
            .autoCommitThreshold,
        partitionsConsumedConcurrently:
          Constants.KAFKA_CONFIG.topics.cloudUploadedVerified
            .partitionsConsumedConcurrently,
        eachMessage: async (payload: EachMessagePayload): Promise<void> => {
          const FUNC_NAME = 'uploadFileConsumer';
          this.logger.debug(`${FUNC_NAME}: upload file processing...`);
          const { message } = payload;
          const {
            id,
            folderName,
            fileName,
            validRows,
            invalidRows,
            unknownRows,
            syntaxErrorRows,
          } = JSON.parse(message.value.toString());
          try {
            const currentFile = await this.fileRepository.findOne({
              where: { id, status: FileProcessingStatus.fileVerifySuccess },
              relations: {
                user: true,
              },
            });
            if (!currentFile) {
              return this.logger.debug(`${FUNC_NAME}: No file found`);
            }
            await this.fileService.verifiedUploader(folderName, fileName);
            const chargeCredits = Helpers.getChargeCredits({
              valid: currentFile.valid,
              invalid: currentFile.invalid,
              syntaxError: currentFile.syntaxError,
            });
            currentFile.status = FileProcessingStatus.makeFileSuccess;
            currentFile.lastModifiedStatus = new Date();
            currentFile.verifyingProgress = 100;
            currentFile.verifiedAt = new Date();
            currentFile.credits = chargeCredits;
            if (!currentFile.downloadAvailable) {
              const user = await this.userRepository.findOne({
                where: { id: currentFile.user.id },
                relations: {
                  performanceStats: true,
                },
              });
              const totalVerified =
                validRows + invalidRows + unknownRows + syntaxErrorRows;
              if (user.credits >= chargeCredits) {
                currentFile.downloadAvailable = true;
              }
              await this.userPerformanceStatsRepository.update(
                user.performanceStats.id,
                {
                  total: totalVerified + user.performanceStats.total,
                  valid: validRows + user.performanceStats.valid,
                  invalid: invalidRows + user.performanceStats.invalid,
                  unknown: unknownRows + user.performanceStats.unknown,
                  syntaxError:
                    syntaxErrorRows + user.performanceStats.syntaxError,
                },
              );
              await this.userRepository.decrement(
                {
                  id: user.id,
                },
                'credits',
                chargeCredits,
              );
            }
            await this.fileRepository.update(currentFile.id, currentFile);
          } catch (error) {
            this.logger.error(`${FUNC_NAME}: ${error.stack}`);
            await this.fileRepository.update(id, {
              status: FileProcessingStatus.makeFileFailed,
              lastModifiedStatus: new Date(),
            });
          }
        },
      })
      .catch((error) => {
        this.logger.error(`Consumer connecting error: ${error.stack}`);
      });
    await this.emailConfirmConsumer
      .run({
        autoCommit: Constants.KAFKA_CONFIG.topics.emailConfirm.autoCommit,
        autoCommitThreshold:
          Constants.KAFKA_CONFIG.topics.emailConfirm.autoCommitThreshold,
        partitionsConsumedConcurrently:
          Constants.KAFKA_CONFIG.topics.emailConfirm
            .partitionsConsumedConcurrently,
        eachMessage: async (payload: EachMessagePayload): Promise<void> => {
          const FUNC_NAME = 'emailConfirmConsumer';
          this.logger.debug(`${FUNC_NAME}: send verify email processing...`);
          const { message } = payload;
          const { id, status, email, token } = JSON.parse(
            message.value.toString(),
          );
          try {
            if (status !== EmailSendStatus.sent) {
              const verifyUrl = AuthUtils.getVerifyUrl(token);
              await this.mailerService.emailConfirmSender(email, verifyUrl);
              await this.emailVerifyRepository.update(id, {
                status: EmailSendStatus.sent,
              });
            }
          } catch (error) {
            this.logger.error(`${FUNC_NAME}: ${error}`);
            await this.emailVerifyRepository.update(id, {
              status: EmailSendStatus.failed,
            });
          }
        },
      })
      .catch((error) => {
        this.logger.error(`Consumer connecting error: ${error}`);
      });
    await this.emailResetPasswordConsumer
      .run({
        autoCommit: Constants.KAFKA_CONFIG.topics.emailResetPassword.autoCommit,
        autoCommitThreshold:
          Constants.KAFKA_CONFIG.topics.emailResetPassword.autoCommitThreshold,
        partitionsConsumedConcurrently:
          Constants.KAFKA_CONFIG.topics.emailResetPassword
            .partitionsConsumedConcurrently,
        eachMessage: async (payload: EachMessagePayload): Promise<void> => {
          const FUNC_NAME = 'emailResetPasswordConsumer';
          this.logger.debug(
            `${FUNC_NAME}: send email reset password processing...`,
          );
          const { message } = payload;
          const { id, status, email, code } = JSON.parse(
            message.value.toString(),
          );
          try {
            if (status !== EmailSendStatus.sent) {
              await this.mailerService.emailResetPasswordSender(email, code);
              await this.resetPasswordRepository.update(id, {
                status: EmailSendStatus.sent,
              });
            }
          } catch (error) {
            this.logger.error(`${FUNC_NAME}: ${error}`);
            await this.resetPasswordRepository.update(id, {
              status: EmailSendStatus.failed,
            });
          }
        },
      })
      .catch((error) => {
        this.logger.error(`Consumer connecting error: ${error}`);
      });
  }

  public async onModuleDestroy(): Promise<void> {
    await this.disconnect();
  }
}
