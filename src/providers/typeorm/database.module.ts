import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DataSource } from 'typeorm';

@Module({
  providers: [
    {
      provide: 'DATA_SOURCE',
      useFactory: async (configService: ConfigService) => {
        const database = configService.get('database');
        const dataSource = new DataSource({
          type: database.dbService,
          host: database.host,
          port: database.port,
          username: database.username,
          password: database.password,
          database: database.name,
          entities: [__dirname + '/../../**/*.entity.{js,ts}'],
          synchronize: true,
        });
        return dataSource.initialize();
      },
      inject: [ConfigService],
    },
  ],
  exports: ['DATA_SOURCE'],
})
export class DatabaseModule {}
