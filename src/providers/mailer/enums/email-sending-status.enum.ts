export enum EmailSendStatus {
  pending = 'pending',
  sent = 'sent',
  failed = 'failed',
}
