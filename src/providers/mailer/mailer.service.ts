import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { gmail_v1, google } from 'googleapis';
import { Constants } from 'src/constants/Constants';
import { AuthService } from 'src/modules/auth/auth.service';
import Logger from 'src/utils/logger.utils';
import { EmailTemplates } from 'src/templates/email.template';

const { OAuth2 } = google.auth;

@Injectable()
export class MailerService {
  private oAuth2Client: any;
  private gmail: gmail_v1.Gmail;
  private logger: Logger = new Logger(__filename);

  constructor(
    private readonly configService: ConfigService,
    @Inject(forwardRef(() => AuthService))
    private authService: AuthService,
  ) {
    this.oAuth2Client = new OAuth2(
      this.configService.get('email').senderClientId,
      this.configService.get('email').senderClientSecret,
      this.configService.get('email').oauthUri,
    );

    const { tokens } = this.oAuth2Client.refreshToken(
      this.configService.get('email').refreshToken,
    );
    this.oAuth2Client.setCredentials(tokens);

    this.gmail = google.gmail({
      version: 'v1',
      auth: this.oAuth2Client,
    });
  }

  public async emailConfirmSender(
    recipient: string,
    verifyUrl: string,
  ): Promise<void> {
    const FUNC_NAME = 'emailConfirmSender';
    try {
      const isLimitExceed = await this.authService.isLimitSendEmailConfirm(
        recipient,
      );
      if (isLimitExceed) {
        return this.logger.info(`${FUNC_NAME}: Reach maximum send`);
      }

      const message = [
        `From: ${Constants.WEBSITE_NAME} <${Constants.ADMIN_EMAIL}>`,
        `To: ${recipient}`,
        'Subject: Confirm signup request',
        'Content-Type: text/html; charset=utf-8',
        '',
        EmailTemplates.templateEmailConfirm(verifyUrl),
      ].join('\n');

      const encodedMessage = Buffer.from(message)
        .toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_');

      await this.gmail.users.messages.send({
        userId: Constants.ADMIN_EMAIL,
        requestBody: {
          raw: encodedMessage,
        },
      });
      this.logger.info(`${FUNC_NAME}: Message sent to ${recipient}`);
    } catch (err) {
      throw err;
    }
  }

  public async emailResetPasswordSender(recipient: string, code: string) {
    try {
      const FUNC_NAME = 'emailResetPasswordSender';
      const isLimitExceed = await this.authService.isLimitSendCodeResetPassword(
        recipient,
      );
      if (isLimitExceed) {
        return this.logger.info(`${FUNC_NAME}: Reach maximum send`);
      }
      const message = [
        `From: ${Constants.WEBSITE_NAME} <${Constants.ADMIN_EMAIL}>`,
        `To: ${recipient}`,
        'Subject: Confirm reset password request',
        'Content-Type: text/html; charset=utf-8',
        '',
        EmailTemplates.templateResetPassword(code),
      ].join('\n');

      const encodedMessage = Buffer.from(message)
        .toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_');

      await this.gmail.users.messages.send({
        userId: Constants.ADMIN_EMAIL,
        requestBody: {
          raw: encodedMessage,
        },
      });
      this.logger.info(`${FUNC_NAME}: Message sent to ${recipient}`);
    } catch (err) {
      throw err;
    }
  }
}
