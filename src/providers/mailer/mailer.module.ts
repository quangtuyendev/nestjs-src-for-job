import { Module, forwardRef } from '@nestjs/common';
import { AuthModule } from 'src/modules/auth/auth.module';
import { MailerService } from './mailer.service';

@Module({
  imports: [forwardRef(() => AuthModule)],
  providers: [MailerService],
  exports: [MailerService],
})
export class MailerModule {}
