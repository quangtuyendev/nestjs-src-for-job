import { Injectable } from '@nestjs/common';
import FirebaseAdmin from 'firebase-admin';
import { join } from 'path';
import * as fs from 'fs';

@Injectable()
export class FirebaseService {
  public admin: any;

  constructor() {
    this.admin = FirebaseAdmin.initializeApp({
      credential: FirebaseAdmin.credential.cert(
        JSON.parse(
          fs.readFileSync(join('.') + '/services_account.json', {
            encoding: 'utf-8',
          }),
        ),
      ),
    });
  }

  getAuth(): any {
    return this.admin.auth();
  }
}
