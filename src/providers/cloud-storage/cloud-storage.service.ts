import { Storage } from '@google-cloud/storage';
import { Injectable } from '@nestjs/common';
import { join } from 'path';

@Injectable()
export class CloudStorageService {
  public storage: Storage;

  constructor() {
    this.storage = new Storage({
      keyFilename: join('.') + '/services_account.json',
    });
  }

  public getInstance(): Storage {
    return this.storage;
  }
}
