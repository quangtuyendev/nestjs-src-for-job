import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import configs from './configs/configs';
import { Constants } from './constants/Constants';
import { HttpExceptionFilter } from './filters/http-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('v1');

  app.use(cookieParser());

  app.useGlobalFilters(new HttpExceptionFilter());

  const whitelist: string[] = [Constants.APP_BASE_URI];
  const corsOptions = {
    origin: function (
      origin,
      callback: (param: Error | null, status?: boolean | undefined) => void,
    ) {
      if (whitelist.indexOf(origin) !== -1 || !origin) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
    credentials: true,
    exposedHeaders: 'Content-Disposition',
  };

  app.enableCors(corsOptions);

  await app.listen(configs().port, () => {
    console.log(`Server running on port: ${configs().port}`);
  });
}

bootstrap();
