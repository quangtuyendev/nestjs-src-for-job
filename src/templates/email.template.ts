import { Constants } from 'src/constants/Constants';

export class EmailTemplates {
  static templateEmailConfirm(verifyUrl: string): string {
    return `
            <!DOCTYPE html>
            <html lang="en">
            
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Email Confirmation</title>
            </head>
            
            <body style="background-color: #f3f3f9;">
                <div class="row">
                    <div class="col-12">
                        <table class="body-wrap"
                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: transparent; margin: 0;">
                            <tbody>
                                <tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <td style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                                        valign="top"></td>
                                    <td class="container" width="600"
                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
                                        valign="top">
                                        <div class="content"
                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                            <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action"
                                                itemscope="" itemtype="http://schema.org/ConfirmAction"
                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; margin: 0; border: none;">
                                                <tbody>
                                                    <tr style="font-family: 'Roboto', sans-serif; font-size: 14px; margin: 0;">
                                                        <td class="content-wrap"
                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; color: #495057; font-size: 14px; vertical-align: top; margin: 0;padding: 30px; box-shadow: 0 3px 15px rgba(30,32,37,.06); ;border-radius: 7px; background-color: #fff;"
                                                            valign="top">
                                                            <meta itemprop="name" content="Confirm Email"
                                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                <tbody>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                                                                            valign="top">
                                                                            <div style="text-align: center;margin-bottom: 15px;">
                                                                                <img src="https://app.emailcheckerplus.com/static/images/logo_dark.png"
                                                                                    alt="Logo dark" height="40">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 24px; vertical-align: top; margin: 0; padding: 0 0 10px;  text-align: center;"
                                                                            valign="top">
                                                                            <h4
                                                                                style="font-family: 'Roboto', sans-serif; font-weight: 500; margin: 0;">
                                                                                Verify your email</h4>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="font-family: 'Roboto', sans-serif; color: #878a99; box-sizing: border-box; font-size: 15px; vertical-align: top; margin: 0; padding: 0 0 26px; text-align: center;"
                                                                            valign="top">
                                                                            <p style="margin-bottom: 0; line-height: 22px;">Please
                                                                                verify your email
                                                                                address to get started using our service.</p>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block" itemprop="handler" itemscope=""
                                                                            itemtype="http://schema.org/HttpActionHandler"
                                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 22px; text-align: center;"
                                                                            valign="top">
                                                                            <a href="${verifyUrl}" itemprop="url"
                                                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: .8125rem; color: #FFF; text-decoration: none; font-weight: 400; text-align: center; cursor: pointer; display: inline-block; border-radius: .25rem; text-transform: capitalize; background-color: #008aff ; margin: 0; border-color: #008aff ; border-style: solid; border-width: 1px; padding: .5rem .9rem;">Verify
                                                                                Your Email</a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="color: #878a99; text-align: center;font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0; padding-top: 5px"
                                                                            valign="top">
                                                                            <p style="margin-bottom: 10px;">Or verify using this
                                                                                link:
                                                                            </p>
                                                                            <a href="${verifyUrl}"
                                                                                target="_blank">${verifyUrl.slice(
                                                                                  0,
                                                                                  50,
                                                                                )}...</a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div
                                                style="text-align: center; margin: 25px auto 0px auto;font-family: 'Roboto', sans-serif;">
                                                <h4 style="font-weight: 500; line-height: 1.5;font-family: 'Roboto', sans-serif;">
                                                    Need
                                                    Help?</h4>
                                                <p style="color: #878a99; line-height: 1.5;">Have questions? No worries! <br> Send
                                                    feedback to
                                                    <a href="mailto:${
                                                      Constants.ADMIN_EMAIL
                                                    }?Subject=Support" target="_top" style="font-weight: 500;">${
      Constants.ADMIN_EMAIL
    }</a>
                                                </p>
                                                <p
                                                    style="font-family: 'Roboto', sans-serif; font-size: 14px;color: #98a6ad; margin: 0px;">
                                                    ${new Date().getFullYear()} ${
      Constants.WEBSITE_NAME
    }. All rights
                                                    reserved.</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </body>
            
            </html>`;
  }

  static templateResetPassword(code: string): string {
    return `
            <!DOCTYPE html>
            <html lang="en">
            
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Reset password</title>
            </head>
            
            <body style="background-color: #f3f3f9;">
                <div class="row">
                    <div class="col-12">
                        <table class="body-wrap"
                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: transparent; margin: 0;">
                            <tbody>
                                <tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <td style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                                        valign="top"></td>
                                    <td class="container" width="600"
                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
                                        valign="top">
                                        <div class="content"
                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                            <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action"
                                                itemscope="" itemtype="http://schema.org/ConfirmAction"
                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; margin: 0; border: none;">
                                                <tbody>
                                                    <tr style="font-family: 'Roboto', sans-serif; font-size: 14px; margin: 0;">
                                                        <td class="content-wrap"
                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; color: #495057; font-size: 14px; vertical-align: top; margin: 0;padding: 30px; box-shadow: 0 3px 15px rgba(30,32,37,.06); ;border-radius: 7px; background-color: #fff;"
                                                            valign="top">
                                                            <meta itemprop="name" content="Confirm Email"
                                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                <tbody>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                                                                            valign="top">
                                                                            <div style="text-align: center;margin-bottom: 15px;">
                                                                                <img src="https://app.emailcheckerplus.com/static/images/logo_dark.png"
                                                                                    alt="Logo dark" height="40">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 24px; vertical-align: top; margin: 0; padding: 0 0 10px;  text-align: center;"
                                                                            valign="top">
                                                                            <h4
                                                                                style="font-family: 'Roboto', sans-serif; font-weight: 500; margin: 0;">
                                                                                Reset your password</h4>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block"
                                                                            style="font-family: 'Roboto', sans-serif; color: #878a99; box-sizing: border-box; font-size: 15px; vertical-align: top; margin: 0; padding: 0 0 26px; text-align: center;"
                                                                            valign="top">
                                                                            <p style="margin-bottom: 0; line-height: 22px;">We
                                                                                received a request to
                                                                                reset your password. To reset your password, please
                                                                                click button below and follow the instructions
                                                                                on our website.
                                                                            </p>
                                                                        </td>
                                                                    </tr>
                                                                    <tr
                                                                        style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                                        <td class="content-block" itemprop="handler" itemscope=""
                                                                            itemtype="http://schema.org/HttpActionHandler"
                                                                            style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 22px; text-align: center;"
                                                                            valign="top">
                                                                            <a href="${
                                                                              Constants.APP_BASE_URI
                                                                            }/reset-password?code=${code}"
                                                                                itemprop="url"
                                                                                style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: .8125rem; color: #FFF; text-decoration: none; font-weight: 400; text-align: center; cursor: pointer; display: inline-block; border-radius: .25rem; text-transform: capitalize; background-color: #008aff ; margin: 0; border-color: #008aff ; border-style: solid; border-width: 1px; padding: .5rem .9rem;">Reset
                                                                                Password</a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div
                                                style="text-align: center; margin: 25px auto 0px auto;font-family: 'Roboto', sans-serif;">
                                                <h4 style="font-weight: 500; line-height: 1.5;font-family: 'Roboto', sans-serif;">
                                                    Need
                                                    Help?</h4>
                                                <p style="color: #878a99; line-height: 1.5;">Have questions? No worries! <br> Send
                                                    feedback to
                                                    <a href="mailto:${
                                                      Constants.ADMIN_EMAIL
                                                    }?Subject=Support" target="_top"
                                                        style="font-weight: 500;">${
                                                          Constants.ADMIN_EMAIL
                                                        }</a>
                                                </p>
                                                <p
                                                    style="font-family: 'Roboto', sans-serif; font-size: 14px;color: #98a6ad; margin: 0px;">
                                                    ${new Date().getFullYear()} ${
      Constants.WEBSITE_NAME
    }. All rights
                                                    reserved.</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </body>
            
            </html>`;
  }
}
