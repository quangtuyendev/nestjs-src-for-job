import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import configs from './configs/configs';
import { AuthModule } from './modules/auth/auth.module';
import { FileModule } from './modules/file/file.module';
import { UserModule } from './modules/user/user.module';
import { FirebaseModule } from './providers/firebase/firebase.module';
import { DatabaseModule } from './providers/typeorm/database.module';

@Module({
  imports: [
    DatabaseModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configs],
    }),
    AuthModule,
    UserModule,
    FileModule,
    FirebaseModule,
  ],
})
export class AppModule {}
