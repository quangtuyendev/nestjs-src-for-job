import configs from 'src/configs/configs';

export class Constants {
  static WEBSITE_NAME = 'EmailCheckerPlus';
  static DOMAIN_NAME = 'emailcheckerplus.com';

  static APP_BASE_URI =
    configs().nodeEnv === 'production'
      ? `https://app.${Constants.DOMAIN_NAME}`
      : 'http://localhost:3000';

  static APP_HOSTNAME =
    configs().nodeEnv === 'production'
      ? `app.${Constants.DOMAIN_NAME}`
      : 'localhost';

  static COOKIE_DOMAIN_SIGNED =
    configs().nodeEnv === 'production'
      ? `.${Constants.DOMAIN_NAME}`
      : 'localhost';

  static SUPPORT_EMAIL_ADDRESS = `support@${Constants.DOMAIN_NAME}`;

  static UPLOAD_FILE_CONFIGS = {
    uploadPath: 'uploads',
    maxFiles: 1,
    fieldNameSize: 100,
    fileNameLength: 210,
    fileType: ['csv', 'txt', 'xlsx', 'xls'],
    chunkSize: 1 * 1024 * 1024, // 1MB
  };

  static APP_CUSTOM_CODES = {
    additionalCreditRequired: 1000,
    tooManyUploadRequest: 1001,
    paymentRequired: 1002,
    internalServerError: 1003,
    firebaseTokenArgumentError: 1004,
    maxFileNameLength: 1005,
  };

  static MESSAGES = {
    success: {
      file: {
        upload_success: 'upload_success',
        file_delete_success: 'file_delete_success',
        no_data_available: 'no_data_available',
      },
      quickVerification: {},
      auth: {
        signin_request_unverify: 'signin_request_unverify',
        registration_success: 'registration_success',
        signin_success: 'signin_success',
        signup_success: 'signup_success',
        signout_success: 'signout_success',
        update_info_success: 'update_info_success',
        verified: 'verified',
        profile_already_completed: 'profile_already_completed',
        refresh_token_success: 'refresh_token_success',
        update_password_success: 'update_password_success',
        reset_password_success: 'reset_password_success',
        send_reset_code_success: 'send_reset_code_success',
      },
      user: {
        subscription_downgrade_success: 'subscription_downgrade_success',
        no_subscription_subscribed: 'no_subscription_subscribed',
      },
    },
    failed: {
      file: {
        file_empty: {
          message: 'file_empty',
        },
        max_file_name_length: {
          code: Constants.APP_CUSTOM_CODES.maxFileNameLength,
          message: 'max_file_name_length',
        },
        invalid_file_type: {
          message: 'invalid_file_type',
        },
        file_content_is_malicious: {
          message: 'file_content_is_malicious',
        },
        upload_not_allowed: {
          message: 'upload_not_allowed',
        },
        max_file_size: {
          message: 'max_file_size',
        },
        too_many_download_request: {
          message: 'too_many_download_request',
        },
        too_many_upload_request: {
          message: 'too_many_upload_request',
          code: Constants.APP_CUSTOM_CODES.tooManyUploadRequest,
        },
        too_many_verify_request: {
          message: 'too_many_verify_request',
        },
        too_many_status_check_request: {
          message: 'too_many_status_check_request',
        },
        file_not_found: {
          message: 'file_not_found',
        },
        one_or_more_ids_not_exist: {
          message: 'one_or_more_ids_not_exist',
        },
        delete_not_allowed: {
          message: 'delete_not_allowed',
        },
        delete_not_allowed_file_processing: {
          message: 'delete_not_allowed_file_processing',
        },
        page_limit_min: {
          message: 'page_limit_min',
        },
        page_limit_max: {
          message: 'page_limit_max',
        },
        page_limit_required: {
          message: 'page_limit_required',
        },
        page_min: {
          message: 'page_min',
        },
        page_required: {
          message: 'page_required',
        },
        search_max: {
          message: 'search_max',
        },
        unique_identifier_required: {
          message: 'unique_identifier_required',
        },
        id_required: {
          message: 'id_required',
        },
        option_required: {
          message: 'option_required',
        },
        option_only: {
          message: 'option_only',
        },
        ids_min: {
          message: 'ids_min',
        },
        ids_required: {
          message: 'ids_required',
        },
        ids_empty: {
          message: 'ids_empty',
        },
        breakdown_required: {
          message: 'breakdown_required',
        },
        breakdown_only: {
          message: 'breakdown_only',
        },
        label_required: {
          message: 'label_required',
        },
        label_only: {
          message: 'label_only',
        },
        file_already_process: {
          message: 'file_already_process',
        },
        invalid_request: {
          message: 'invalid_request',
        },
        download_not_allowed: {
          message: 'download_not_allowed',
        },
        verify_not_allowed: {
          message: 'verify_not_allowed',
        },
      },
      quickVerification: {
        quick_verification_array_min: {
          message: 'quick_verification_array_min',
        },
        quick_verification_array_max: {
          message: 'quick_verification_array_max',
        },
        quick_verification_emails_required: {
          message: 'quick_verification_emails_required',
        },
        too_many_verify_request: {
          message: 'too_many_verify_request',
        },
      },
      auth: {
        too_many_registration_request: {
          message: 'too_many_registration_request',
        },
        too_many_attempts_try_later: {
          message: 'too_many_attempts_try_later',
        },
        invalid_token: {
          message: 'invalid_token',
        },
        invalid_custom_token: {
          message: 'invalid_custom_token',
        },
        email_already_registered: {
          message: 'email_already_registered',
        },
        password_contains_space: {
          message: 'password_contains_space',
        },
        password_include_email: {
          message: 'password_include_email',
        },
        user_not_found: {
          message: 'user_not_found',
        },
        fullname_required: {
          message: 'fullname_required',
        },
        fullname_empty: {
          message: 'fullname_empty',
        },
        fullname_min_length_4: {
          message: 'fullname_min_length_4',
        },
        fullname_max_length_256: {
          message: 'fullname_max_length_256',
        },
        fullname_invalid_characters: {
          message: 'fullname_invalid_characters',
        },
        email_required: {
          message: 'email_required',
        },
        email_empty: {
          message: 'email_empty',
        },
        email_invalid: {
          message: 'email_invalid',
        },
        email_min_length_4: {
          message: 'email_min_length_4',
        },
        email_max_length_255: {
          message: 'email_max_length_255',
        },
        password_required: {
          message: 'password_required',
        },
        password_empty: {
          message: 'password_empty',
        },
        password_min_length_8: {
          message: 'password_min_length_8',
        },
        password_max_length_30: {
          message: 'password_max_length_30',
        },
        password_complexity_requirement_not_met: {
          message: 'password_complexity_requirement_not_met',
        },
        current_password_required: {
          message: 'current_password_required',
        },
        current_password_empty: {
          message: 'current_password_empty',
        },
        current_password_min_length_8: {
          message: 'current_password_min_length_8',
        },
        current_password_max_length_30: {
          message: 'current_password_max_length_30',
        },
        current_password_complexity_requirement_not_met: {
          message: 'current_password_complexity_requirement_not_met',
        },
        new_password_required: {
          message: 'new_password_required',
        },
        new_password_empty: {
          message: 'new_password_empty',
        },
        new_password_min_length_8: {
          message: 'new_password_min_length_8',
        },
        new_password_max_length_30: {
          message: 'new_password_max_length_30',
        },
        new_password_complexity_requirement_not_met: {
          message: 'new_password_complexity_requirement_not_met',
        },
        new_password_cannot_match_current_password: {
          message: 'new_password_cannot_match_current_password',
        },
        timezone_required: {
          message: 'timezone_required',
        },
        timezone_empty: {
          message: 'timezone_empty',
        },
        timezone_only: {
          message: 'timezone_only',
        },
        invalid_is_show_guide: {
          message: 'invalid_is_show_guide',
        },
        invalid_password: {
          message: 'invalid_password',
        },
        email_not_found: {
          message: 'email_not_found',
        },
      },
      user: {
        transaction_page_limit_min: {
          message: 'transaction_page_limit_min',
        },
        transaction_page_limit_max: {
          message: 'transaction_page_limit_max',
        },
        transaction_page_limit_required: {
          message: 'transaction_page_limit_required',
        },
        transaction_page_min: {
          message: 'transaction_page_min',
        },
        transaction_page_required: {
          message: 'transaction_page_required',
        },
        additional_verify_credit_required: {
          code: Constants.APP_CUSTOM_CODES.additionalCreditRequired,
          message: 'additional_verify_credit_required',
        },
        additional_download_credit_required: {
          code: Constants.APP_CUSTOM_CODES.additionalCreditRequired,
          message: 'additional_download_credit_required',
        },
        subscription_cancel_failed: {
          message: 'subscription_cancel_failed',
        },
      },
      checkout: {
        order_id_required: {
          message: 'order_id_required',
        },
        amount_required: {
          message: 'amount_required',
        },
        amount_range: {
          message: 'amount_range',
        },
        amount_divisible: {
          message: 'amount_divisible',
        },
        subscription_id_required: {
          message: 'subscription_id_required',
        },
        subscription_plan_id_required: {
          message: 'subscription_plan_id_required',
        },
        invalid_subscription_plan_id: {
          message: 'invalid_subscription_plan_id',
        },
        cannot_create_order: {
          message: 'cannot_create_order',
        },
        transaction_already_exist: {
          message: 'transaction_already_exist',
        },
        subscription_cancel_reason_required: {
          message: 'subscription_cancel_reason_required',
        },
        subscription_cancel_reason_min: {
          message: 'subscription_cancel_reason_min',
        },
        subscription_cancel_reason_max: {
          message: 'subscription_cancel_reason_max',
        },
      },
      common: {
        internal_server_error: {
          code: Constants.APP_CUSTOM_CODES.internalServerError,
          message: 'internal_server_error',
        },
        payment_required: {
          code: Constants.APP_CUSTOM_CODES.paymentRequired,
          message: 'payment_required',
        },
        forbidden: {
          message: 'forbidden',
        },
        too_many_request: {
          message: 'too_many_request',
        },
      },
    },
  };

  static HTTP_RESPONSE_STATUS_MESSAGE = {
    success: 'Success',
    failed: 'Failed',
  };

  static KAFKA_CONFIG = {
    topics: {
      chunksUploaded: {
        name: 'chunksUploaded',
        autoCommit: true,
        autoCommitThreshold: 1,
        partitionsConsumedConcurrently: 2,
      },
      emailVerify: {
        name: 'emailVerify',
        autoCommit: true,
        autoCommitThreshold: 1,
        partitionsConsumedConcurrently: 2,
      },
      fileClean: {
        name: 'fileClean',
        autoCommit: true,
        autoCommitThreshold: 1,
        partitionsConsumedConcurrently: 2,
      },
      cloudUploadedVerified: {
        name: 'cloudUploadedVerified',
        autoCommit: true,
        autoCommitThreshold: 1,
        partitionsConsumedConcurrently: 2,
      },
      emailConfirm: {
        name: 'emailConfirm',
        autoCommit: true,
        autoCommitThreshold: 1,
        partitionsConsumedConcurrently: 2,
      },
      emailResetPassword: {
        name: 'emailResetPassword',
        autoCommit: true,
        autoCommitThreshold: 1,
        partitionsConsumedConcurrently: 2,
      },
    },
    groups: {
      chunksUploaded: 'chunksUploaded',
      emailVerify: 'emailVerify',
      fileClean: 'fileClean',
      cloudUploadedVerified: 'cloudUploadedVerified',
      emailConfirm: 'emailConfirm',
      emailResetPassword: 'emailResetPassword',
    },
  };

  static FILE_STATUS = {
    chunksUploaded: 'chunks_uploaded',
    mergeFileFailed: 'merge_file_failed',
    mergeFileSuccess: 'merge_file_success',
    fileVerifying: 'file_verifying',
    fileVerifyFailed: 'file_verify_failed',
    fileVerifySuccess: 'file_verify_success',
    makeFileFailed: 'make_file_failed',
    makeFileSuccess: 'make_file_success',
    fileError: 'file_error',
  };

  static TEMPORARY_FILE_EXT = '.tmp';

  static EMAILS_VERIFY_PER_REQUEST = 300;
  static PROCESS_AFTER_PER = 3;

  static ADMIN_EMAIL = `support@${Constants.DOMAIN_NAME}`;

  static EMAIL_HEADERS = {
    email: 'email',
    emails: 'emails',
    emailAddress: 'emailaddress',
    emailAddresses: 'emailaddresses',
  };

  static DEFAULT_HEADER_TXT = 'Email address';

  static PM2_PROCESS_NAME = 'BACKEND_API';

  static LIMIT_EMAILS_SEND_PER_HOUR = 3;

  static PRICING = {
    verifyConvertRatio: 3000,
    payAsYouGo: {
      step: 10,
      min: 10,
      max: 1000,
      default: 100,
    },
    monthly: {
      free: {
        name: 'Free',
        label: 'Month',
        verify: 1000,
        price: 0,
        payPerMonths: 1,
        discount: 0,
        subscriptionPlanId: null,
      },
      basic: {
        name: 'Basic',
        label: 'Month',
        price: 100,
        payPerMonths: 1,
        discount: 10,
        subscriptionPlanId: 'P-3F611788K5131553HMQ3WGNQ',
      },
      standard: {
        name: 'Standard',
        label: 'Month',
        price: 500,
        payPerMonths: 1,
        discount: 10,
        subscriptionPlanId: 'P-7RG63383E7500220FMQ3WGXY',
      },
      premium: {
        name: 'Premium',
        label: 'Month',
        price: 1000,
        payPerMonths: 1,
        discount: 10,
        subscriptionPlanId: 'P-10D80043BV529512PMQ3WG7Q',
      },
    },
  };
  static CREDITS_PER_USD = 3000;
  static FREE_REGISTRATION_CREDITS = 1000;

  static ADDITIONAL_COLUMNS = [
    {
      key: `${Constants.WEBSITE_NAME} Verification Status`,
      header: `${Constants.WEBSITE_NAME} Verification Status`,
    },
    {
      key: `${Constants.WEBSITE_NAME} Free Account Status`,
      header: `${Constants.WEBSITE_NAME} Free Account Status`,
    },
    {
      key: `${Constants.WEBSITE_NAME} Account`,
      header: `${Constants.WEBSITE_NAME} Account`,
    },
    {
      key: `${Constants.WEBSITE_NAME} Domain`,
      header: `${Constants.WEBSITE_NAME} Domain`,
    },
  ];

  static FREE_EMAILS_DOMAINS = [
    'gmail.com',
    'yahoo.com',
    'outlook.com',
    'aol.com',
    'icloud.com',
    'protonmail.com',
    'mail.com',
    'zoho.com',
    'yandex.com',
    'tutanota.com',
    'gmx.com',
    'mail.ru',
    'hushmail.com',
    'fastmail.com',
    'inbox.com',
    'rediffmail.com',
    'rocketmail.com',
    'att.net',
    'comcast.net',
  ];

  static EMAIL_VERIFIED_STATUS = {
    valid: 'Valid',
    invalid: 'Invalid',
    syntaxError: 'Syntax error',
    unknown: 'Unknown',
    haveAvatar: 'Valid (have avatar)',
  };

  static TYPO_ERROR_DOMAINS = [
    'gamil.com',
    'gmai.com',
    'gmial.com',
    'gmal.com',
    'gmaill.com',
    'gmail.cmo',
    'gmaol.com',
    'gmail.ocm',
    'outlok.com',
    'outloook.com',
    'outloock.com',
    'outllok.com',
    'outlok.com',
    'outlok.com',
    'outook.com',
    'oulook.com',
    'outlokk.com',
    'outlook.cmo',
    'hotmai.com',
    'hotmial.com',
    'homtail.com',
    'hormail.com',
    'hoymail.com',
    'hotamil.com',
    'hotmal.com',
    'hotmil.com',
    'hotmaill.com',
    'hotmail.cmo',
    'yaho.com',
    'yhaoo.com',
    'yaho0.com',
    'yahooo.com',
    'yahho.com',
    'yaoo.com',
    'yhaoo.com',
    'yhaoho.com',
    'yhoo.com',
    'yahoo.cmo',
    'protomail.com',
    'protnomail.com',
    'protonamil.com',
    'protonnail.com',
    'protonmaill.com',
    'protonmal.com',
    'protonmil.com',
    'protonmail.cmo',
    'porotonmail.com',
    'protonme.com',
    'protom.me',
    'protnome.com',
    'protonamail.com',
    'protonnme.com',
    'protonmail.me.com',
    'protonmal.com',
    'protonmi.com',
    'protonemail.com',
    'proton.mecom',
    'al.com',
    'ao.com',
    'aool.com',
    'all.com',
    'aol.cmo',
    'aoi.com',
    'akl.com',
    'aol.cm',
    'apl.com',
    'a0l.com',
    'gmc.com',
    'gxm.com',
    'gnx.com',
    'gmx.cmo',
    'gmx.co',
    'gmx.con',
    'gmz.com',
    'gmx.comm',
    'gmx.vom',
    'iclou.com',
    'icoud.com',
    'icloud.cmo',
    'iloud.com',
    'iclooud.com',
    'icluod.com',
    'iCloud.con',
    'icluod.com',
    'icloud.co',
    'iclloud.com',
    'yadex.com',
    'yendex.com',
    'yandex.cmo',
    'yandx.com',
    'yndex.com',
    'yandex.con',
    'yandec.com',
    'yandex.co',
    'yandex.cm',
  ];

  static PAYPAL_PURCHASE_STATUSES = {
    createOrderSuccess: 'CREATED',
    captureOrderSuccess: 'COMPLETED',

    createSubscriptionSuccess: 'APPROVAL_PENDING',
    captureSubscriptionSuccess: 'ACTIVE',
  };
}
