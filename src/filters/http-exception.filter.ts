import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { Response } from 'express';
import { IHttpErrorResponse } from 'src/common/http-error-response.common';
import { HttpStatus } from 'src/common/http-status.utils';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    const message = exception.getResponse() as {
      response: IHttpErrorResponse;
      message: string;
    };
    response.status(status).json({
      status: HttpStatus.failed,
      error:
        typeof message.response === 'undefined'
          ? { message: message.message }
          : {
              code: message.response.code,
              message: message.response.message,
            },
    });
  }
}
