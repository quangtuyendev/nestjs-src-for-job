import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  limit: Joi.number().integer().min(10).max(100).required().messages({
    'number.base':
      Constants.MESSAGES.failed.user.transaction_page_limit_required.message,
    'number.min':
      Constants.MESSAGES.failed.user.transaction_page_limit_min.message,
    'number.max':
      Constants.MESSAGES.failed.user.transaction_page_limit_max.message,
    'any.required':
      Constants.MESSAGES.failed.user.transaction_page_limit_required.message,
  }),
  page: Joi.number().integer().min(0).required().messages({
    'number.base':
      Constants.MESSAGES.failed.user.transaction_page_required.message,
    'number.min': Constants.MESSAGES.failed.user.transaction_page_min.message,
    'any.required':
      Constants.MESSAGES.failed.user.transaction_page_required.message,
  }),
});
export class GetTransactionsValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
