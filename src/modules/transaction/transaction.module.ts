import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/providers/typeorm/database.module';
import { DataSource } from 'typeorm';
import { TransactionEntity } from './entities/transaction.entity';
import { TransactionService } from './transaction.service';

@Module({
  imports: [DatabaseModule],
  providers: [
    TransactionService,
    {
      provide: 'TRANSACTION_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(TransactionEntity),
      inject: ['DATA_SOURCE'],
    },
  ],
  exports: [TransactionService],
})
export class TransactionModule {}
