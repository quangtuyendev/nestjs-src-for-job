import {
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { Repository } from 'typeorm';
import Logger from '../../utils/logger.utils';
import { TransactionDto } from './dtos/transaction.dto';
import { TransactionEntity } from './entities/transaction.entity';

@Injectable()
export class TransactionService {
  private logger: Logger = new Logger(__filename);

  constructor(
    @Inject('TRANSACTION_REPOSITORY')
    private readonly transactionRepository: Repository<TransactionEntity>,
  ) {}

  async getTransactionByUser(
    userId: number,
    page: number,
    limit: number,
  ): Promise<{
    items: TransactionDto[];
    totalCount: number;
  }> {
    try {
      const FUNC_NAME = '/getTransactionByUser';
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({
          limit,
          page,
        })}`,
      );
      const transactions = await this.transactionRepository.find({
        where: { user: { id: userId } },
        order: {
          createdAt: 'DESC',
        },
        skip: page * limit,
        take: limit,
      });
      const totalCount = await this.transactionRepository.count({
        where: { user: { id: userId } },
      });
      const convertedTransactions = plainToInstance(
        TransactionDto,
        transactions,
        {
          excludeExtraneousValues: true,
        },
      );
      return {
        items: convertedTransactions,
        totalCount,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
