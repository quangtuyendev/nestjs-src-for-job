import { Expose } from 'class-transformer';

export class TransactionDto {
  @Expose()
  id: number;

  @Expose()
  status: string;

  @Expose()
  country: string;

  @Expose()
  currency: string;

  @Expose()
  amount: string;

  @Expose()
  customerName: string;

  @Expose()
  customerEmail: string;

  @Expose()
  orderId: string;

  @Expose()
  paymentMethod: string;

  @Expose()
  purchaseId: string;

  @Expose()
  credits: number;

  @Expose()
  payerId: string;

  @Expose()
  nextBillDate: string;

  @Expose()
  nextPaymentAmount: string;

  @Expose()
  planName: string;

  @Expose()
  subscriptionId: string;

  @Expose()
  subscriptionPlanId: string;
}
