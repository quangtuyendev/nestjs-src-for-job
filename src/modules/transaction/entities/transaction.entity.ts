import { BaseEntity } from 'src/common/base.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity({ name: 'user_transactions' })
export class TransactionEntity extends BaseEntity {
  @ManyToOne(() => UserEntity, (user) => user.transactions)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({ name: 'status' })
  status: string;

  @Column({ name: 'country' })
  country: string;

  @Column({ name: 'currency' })
  currency: string;

  @Column({ name: 'amount' })
  amount: string;

  @Column({ name: 'customer_name' })
  customerName: string;

  @Column({ name: 'customer_email' })
  customerEmail: string;

  @Column({ name: 'order_id' })
  orderId: string;

  @Column({ name: 'payment_method' })
  paymentMethod: string;

  @Column({ name: 'purchase_id' })
  purchaseId: string;

  @Column({ name: 'credits' })
  credits: number;

  @Column({ name: 'payer_id' })
  payerId: string;

  @Column({ name: 'next_bill_date' })
  nextBillDate: string;

  @Column({ name: 'next_payment_amount' })
  nextPaymentAmount: string;

  @Column({ name: 'plan_name' })
  planName: string;

  @Column({ name: 'subscription_id' })
  subscriptionId: string;

  @Column({ name: 'subscription_plan_id' })
  subscriptionPlanId: string;
}
