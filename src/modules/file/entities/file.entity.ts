import { BaseEntity } from 'src/common/base.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { FileProcessingStatus } from '../enums/file-processing-status.enum';

@Entity({ name: 'files' })
export class FileEntity extends BaseEntity {
  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column({ name: 'unique_identifier', nullable: true })
  uniqueIdentifier?: string;

  @Column({ name: 'origin_file_name' })
  originFileName: string;

  @Column({ name: 'file_name' })
  fileName: string;

  @Column({ name: 'folder_name' })
  folderName: string;

  @Column({ name: 'file_size' })
  fileSize: number;

  @Column({ name: 'total', nullable: true, default: 0 })
  total?: number;

  @Column({ name: 'duplicate', nullable: true, default: 0 })
  duplicate?: number;

  @Column({ name: 'valid', nullable: true, default: 0 })
  valid?: number;

  @Column({ name: 'invalid', nullable: true, default: 0 })
  invalid?: number;

  @Column({ name: 'unknown', nullable: true, default: 0 })
  unknown?: number;

  @Column({ name: 'syntax_error', nullable: true, default: 0 })
  syntaxError?: number;

  @Column({ name: 'avatar_count', nullable: true, default: 0 })
  avatarCount?: number;

  @Column({ name: 'status' })
  status: FileProcessingStatus;

  @Column({ name: 'last_modified_status' })
  lastModifiedStatus: Date;

  @Column({ name: 'verifying_progress', nullable: true, default: 0 })
  verifyingProgress?: number;

  @Column({ name: 'verified_at', nullable: true })
  verifiedAt?: Date;

  @Column({ name: 'error_reason', nullable: true })
  errorReason?: string;

  @Column({ name: 'download_available', nullable: true })
  downloadAvailable?: boolean;

  @Column({ name: 'credits', nullable: true, default: 0 })
  credits?: number;
}
