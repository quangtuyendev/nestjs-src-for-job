import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';
import { FileDownloadSuffix } from '../enums/file-download-suffix.enum';

const IdSchema = Joi.object({
  id: Joi.string().required().messages({
    'any.required': Constants.MESSAGES.failed.file.id_required.message,
  }),
});

const OptionSchema = Joi.object({
  option: Joi.string()
    .valid(...Object.values(FileDownloadSuffix).slice(0, -2))
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.file.option_required.message,
      'any.only': Constants.MESSAGES.failed.file.option_only.message,
    }),
});

export class FileDownloadIdValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await IdSchema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}

export class FileDownloadOptionValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await OptionSchema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
