import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  emails: Joi.array()
    .items(Joi.string().required())
    .min(1)
    .max(1000)
    .required(),
}).messages({
  'array.min':
    Constants.MESSAGES.failed.quickVerification.quick_verification_array_min
      .message,
  'array.max':
    Constants.MESSAGES.failed.quickVerification.quick_verification_array_max
      .message,
  'any.required':
    Constants.MESSAGES.failed.quickVerification
      .quick_verification_emails_required.message,
});
export class QuickVerifyValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
