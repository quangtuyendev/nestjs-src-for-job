import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';
import { DataRangeFilter } from '../enums/data-range-filter.enum';
import { ChartBreakdowns } from '../enums/chart-breakdown-fitler.enum';

const Schema = Joi.object({
  breakdown: Joi.string()
    .valid(...Object.values(ChartBreakdowns))
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.file.breakdown_required.message,
      'any.only': Constants.MESSAGES.failed.file.breakdown_only.message,
    }),
  label: Joi.string()
    .valid(
      ...[
        DataRangeFilter.today,
        DataRangeFilter.yesterday,
        DataRangeFilter.last7Days,
        DataRangeFilter.thisWeek,
        DataRangeFilter.thisMonth,
        DataRangeFilter.lastMonth,
        DataRangeFilter.thisYear,
      ],
    )
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.file.label_required.message,
      'any.only': Constants.MESSAGES.failed.file.label_only.message,
    }),
});
export class ChartDataInputValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
