import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  limit: Joi.number().integer().min(10).max(100).required().messages({
    'number.base': Constants.MESSAGES.failed.file.page_limit_required.message,
    'number.min': Constants.MESSAGES.failed.file.page_limit_min.message,
    'number.max': Constants.MESSAGES.failed.file.page_limit_max.message,
    'any.required': Constants.MESSAGES.failed.file.page_limit_required.message,
  }),
  page: Joi.number().integer().min(0).required().messages({
    'number.base': Constants.MESSAGES.failed.file.page_required.message,
    'number.min': Constants.MESSAGES.failed.file.page_min.message,
    'any.required': Constants.MESSAGES.failed.file.page_required.message,
  }),
  search: Joi.string().max(210).allow('').messages({
    'string.max': Constants.MESSAGES.failed.file.search_max.message,
  }),
});

export class GetFileListValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      const { searchValue: search, page, limit } = value;
      await Schema.validateAsync({
        search,
        page,
        limit,
      });
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
