import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  ids: Joi.array().items().min(1).required().messages({
    'array.min': Constants.MESSAGES.failed.file.ids_min.message,
    'any.required': Constants.MESSAGES.failed.file.ids_required.message,
    'array.empty': Constants.MESSAGES.failed.file.ids_empty.message,
  }),
});

export class FileDeleteValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
