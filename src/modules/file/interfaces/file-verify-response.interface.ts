import { FileProcessingStatus } from '../enums/file-processing-status.enum';

export interface IFileVerifyResponse {
  id: number;
  status: FileProcessingStatus;
  verifyingProgress: number;
}
