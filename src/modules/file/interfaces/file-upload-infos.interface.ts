export interface IFileUploadInfos {
  resumableIdentifier: string;
  resumableChunkNumber: string;
  resumableTotalChunks: string;
  resumableTotalSize: string;
  resumableFilename: string;
  folderName: string;
}
