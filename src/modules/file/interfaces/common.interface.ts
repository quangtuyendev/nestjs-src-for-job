export interface ITimeInfo {
  startDate: Date;
  endDate: Date;
  startHour: number;
  endHour: number;
  timezone: string;
}

export interface IAddMissingTimeInput {
  chartData: { values: any[]; labels: string[] };
  breakdown: string;
  timeInfo: ITimeInfo;
}

export interface ICookieConfig {
  id: number;
  authorization: string;
  cookie: string;
  retry: number;
  lastUsed: number;
}
