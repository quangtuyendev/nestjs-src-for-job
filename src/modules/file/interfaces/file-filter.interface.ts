export interface IFileFilter {
  limit: string;
  page: string;
  searchValue: string;
  dateFilter: string;
  statusFilter: string;
}
