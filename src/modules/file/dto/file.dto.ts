import { Expose } from 'class-transformer';
import { FileProcessingStatus } from '../enums/file-processing-status.enum';
import { BaseDto } from 'src/common/base.dto';

export class FileDto extends BaseDto {
  @Expose()
  id: number;

  @Expose()
  uniqueIdentifier: string;

  @Expose()
  originFileName: string;

  @Expose()
  fileName: string;

  @Expose()
  folderName: string;

  @Expose()
  fileSize: number;

  @Expose()
  total: number;

  @Expose()
  duplicate: number;

  @Expose()
  valid: number;

  @Expose()
  invalid: number;

  @Expose()
  unknown: number;

  @Expose()
  syntaxError: number;

  @Expose()
  avatarCount: number;

  @Expose()
  status: FileProcessingStatus;

  @Expose()
  lastModifiedStatus: Date;

  @Expose()
  verifyingProgress: number;

  @Expose()
  verifiedAt: Date;

  @Expose()
  errorReason: string;

  @Expose()
  downloadAvailable: boolean;

  @Expose()
  credits: number;
}
