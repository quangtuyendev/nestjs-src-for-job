export enum FileProcessingStatus {
  chunksUploaded = 'chunks_uploaded',
  mergeFileFailed = 'merge_file_failed',
  mergeFileSuccess = 'merge_file_success',
  fileVerifying = 'file_verifying',
  fileVerifyFailed = 'file_verify_failed',
  fileVerifySuccess = 'file_verify_success',
  makeFileFailed = 'make_file_failed',
  makeFileSuccess = 'make_file_success',
  fileError = 'file_error',
}
