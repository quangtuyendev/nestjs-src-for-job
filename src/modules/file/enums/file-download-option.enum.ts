export enum FileDownloadOptions {
  valid = 'valid',
  invalid = 'invalid',
  allResults = 'all_results',
  haveAvatar = 'have_avatar',
  unknown = 'unknown',
  syntaxError = 'syntax_error',
}
