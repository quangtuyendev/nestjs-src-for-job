export enum CreditRoutesCheck {
  filesDownload = '/v1/files/download/:id',
  filesVerify = '/v1/files/verify/:id',
  filesQuickVerify = '/v1/files/quick-verification',
  filesUpload = '/v1/files/upload',
  filesCheckDuplicate = '/v1/files/check-duplicate/:uniqueIdentifier',
}
