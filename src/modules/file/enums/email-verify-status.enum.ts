export enum EmailVerifyStatus {
  valid = 'Valid',
  invalid = 'Invalid',
  syntaxError = 'Syntax error',
  unknown = 'Unknown',
  haveAvatar = 'Valid (have avatar)',
}
