export enum DataRangeFilter {
  today = 'today',
  yesterday = 'yesterday',
  thisWeek = 'this_week_mon_today',
  last7Days = 'last_7_days_including_today',
  lastWeek = 'last_week_mon_sun',
  thisMonth = 'this_month',
  lastMonth = 'last_month',
  thisYear = 'this_year',
  last30Days = 'last_30_days',
  lastYear = 'last_year',
}
