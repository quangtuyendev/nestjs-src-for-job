export enum FileErrorReason {
  readingError = 'reading_error',
  fileEmpty = 'file_empty',
  missingHeader = 'missing_header',
  noDataFound = 'noDataFound',
}
