import { HttpModule } from '@nestjs/axios';
import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
  forwardRef,
} from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import * as fs from 'fs';
import * as multer from 'multer';
import { join } from 'path';
import { Constants } from 'src/constants/Constants';
import { CreditMiddleware } from 'src/middlewares/credits.middleware';
import { CloudStorageModule } from 'src/providers/cloud-storage/cloud-storage.module';
import { FirebaseModule } from 'src/providers/firebase/firebase.module';
import { KafkaModule } from 'src/providers/kafka/kafka.module';
import { DatabaseModule } from 'src/providers/typeorm/database.module';
import { RateLimiter } from 'src/utils/rate-limiter';
import { DataSource } from 'typeorm';
import { UserPerformanceStatsEntity } from '../performance-stats/entities/performance-stats.entity';
import { UserEntity } from '../user/entities/user.entity';
import { FileEntity } from './entities/file.entity';
import { FileController } from './file.controller';
import { FileService } from './file.service';

@Module({
  imports: [
    forwardRef(() => KafkaModule.register()),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    MulterModule.registerAsync({
      useFactory: () => ({
        storage: multer.diskStorage({
          destination: (req, _, cb: (err: null, path: string) => void) => {
            const folderName = req.query.folderName;
            const folderPath =
              join('.') +
              `/src/${Constants.UPLOAD_FILE_CONFIGS.uploadPath}/` +
              folderName;
            if (!fs.existsSync(folderPath)) {
              fs.mkdirSync(folderPath);
            }
            cb(null, folderPath);
          },
          filename: (
            req,
            _,
            cb: (err: Error | null, path?: string | null) => void,
          ) => {
            const uniqueId = req.body.resumableIdentifier;
            const chunkNumber = req.body.resumableChunkNumber;
            const filename = `${uniqueId}-${chunkNumber}${Constants.TEMPORARY_FILE_EXT}`;
            cb(null, filename);
          },
        }),
      }),
    }),
    CloudStorageModule,
    DatabaseModule,
    FirebaseModule,
  ],
  controllers: [FileController],
  providers: [
    FileService,
    {
      provide: 'FILE_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(FileEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'USER_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(UserEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'USER_PERFORMANCE_STATS_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(UserPerformanceStatsEntity),
      inject: ['DATA_SOURCE'],
    },
  ],
  exports: ['FILE_REPOSITORY', FileService],
})
export class FileModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        RateLimiter.apply(
          1000,
          Constants.MESSAGES.failed.file.too_many_upload_request,
          60,
        ),
      )
      .forRoutes({ path: 'files/upload', method: RequestMethod.POST })
      .apply(
        RateLimiter.apply(
          100,
          Constants.MESSAGES.failed.common.too_many_request,
        ),
      )
      .forRoutes(
        { path: 'files', method: RequestMethod.GET },
        { path: 'files/delete', method: RequestMethod.POST },
        {
          path: 'files/chart-data/:breakdown/:label',
          method: RequestMethod.GET,
        },
      )
      .apply(
        RateLimiter.apply(
          10,
          Constants.MESSAGES.failed.file.too_many_verify_request,
        ),
      )
      .forRoutes(
        {
          path: 'files/check-duplicate/:uniqueIdentifier',
          method: RequestMethod.GET,
        },
        { path: 'files/verify/:id', method: RequestMethod.POST },
      )
      .apply(
        RateLimiter.apply(
          10,
          Constants.MESSAGES.failed.quickVerification.too_many_verify_request,
        ),
      )
      .forRoutes({
        path: 'files/verify/quick-verification',
        method: RequestMethod.POST,
      })
      .apply(
        RateLimiter.apply(
          10,
          Constants.MESSAGES.failed.file.too_many_download_request,
        ),
      )
      .forRoutes({ path: 'files/download/:id', method: RequestMethod.POST })
      .apply(
        RateLimiter.apply(
          100,
          Constants.MESSAGES.failed.file.too_many_status_check_request,
        ),
      )
      .forRoutes({ path: 'files/status', method: RequestMethod.POST })
      .apply(CreditMiddleware)
      .forRoutes(
        { path: 'files/upload', method: RequestMethod.POST },
        { path: 'files/download/:id', method: RequestMethod.POST },
        {
          path: 'files/check-duplicate/:uniqueIdentifier',
          method: RequestMethod.GET,
        },
        { path: 'files/verify/:id', method: RequestMethod.POST },
        {
          path: 'files/quick-verification',
          method: RequestMethod.POST,
        },
      );
  }
}
