import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  Inject,
  Injectable,
  InternalServerErrorException,
  forwardRef,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as archiver from 'archiver';
import { plainToInstance } from 'class-transformer';
import * as concat from 'concat-stream';
import { Response } from 'express';
import { parse, parseString } from 'fast-csv';
import * as fs from 'fs';
import { filter, find, map, uniqBy } from 'lodash';
import * as momentTz from 'moment-timezone';
import { join } from 'path';
import { HttpStatus } from 'src/common/http-status.utils';
import { Constants } from 'src/constants/Constants';
import { UserNotFoundException } from 'src/exceptions/user-not-found.exception';
import { CloudStorageService } from 'src/providers/cloud-storage/cloud-storage.service';
import { KafkaService } from 'src/providers/kafka/kafka.service';
import { CsvWriter } from 'src/utils/csv-writer.utils';
import { Helpers } from 'src/utils/helpers.utils';
import { pipeline } from 'stream';
import { Between, ILike, In, Repository } from 'typeorm';
import XLSX from 'xlsx';
import Logger from '../../utils/logger.utils';
import { UserPerformanceStatsEntity } from '../performance-stats/entities/performance-stats.entity';
import { UserEntity } from '../user/entities/user.entity';
import { FileDto } from './dto/file.dto';
import { FileEntity } from './entities/file.entity';
import { DataRangeFilter } from './enums/data-range-filter.enum';
import { EmailVerifyStatus } from './enums/email-verify-status.enum';
import { FileDownloadOptions } from './enums/file-download-option.enum';
import { FileDownloadSuffix } from './enums/file-download-suffix.enum';
import { FileErrorReason } from './enums/file-error-reason.enum';
import { FileProcessingStatus } from './enums/file-processing-status.enum';
import { IFileFilter } from './interfaces/file-filter.interface';
import { IFileUploadInfos } from './interfaces/file-upload-infos.interface';
import { IFileVerifyResponse } from './interfaces/file-verify-response.interface';

@Injectable()
export class FileService {
  private logger: Logger = new Logger(__filename);

  constructor(
    private readonly httpService: HttpService,
    @Inject('FILE_REPOSITORY')
    private readonly fileRepository: Repository<FileEntity>,
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<UserEntity>,
    @Inject('USER_PERFORMANCE_STATS_REPOSITORY')
    private readonly userPerformanceStatsRepository: Repository<UserPerformanceStatsEntity>,
    @Inject(forwardRef(() => KafkaService))
    private readonly kafkaService: KafkaService,
    private readonly configService: ConfigService,
    private readonly cloudStorage: CloudStorageService,
  ) {}

  async uploadFile(
    userId: number,
    body: IFileUploadInfos,
  ): Promise<Partial<{ message: string; statusCode: number }>> {
    const FUNC_NAME = 'uploadFile';
    try {
      const uniqueId = body.resumableIdentifier;
      const chunkNumber = parseInt(body.resumableChunkNumber);
      const totalChunks = parseInt(body.resumableTotalChunks);
      const fileSize = parseInt(body.resumableTotalSize);
      const originFileName = body.resumableFilename;
      const folderName = body.folderName;

      const fileName = Helpers.changeFileExt(originFileName);
      if (chunkNumber === totalChunks) {
        let file = await this.fileRepository.findOne({
          where: {
            uniqueIdentifier: uniqueId,
            folderName,
          },
        });
        if (!file) {
          const user = await this.userRepository.findOne({
            where: { id: userId },
          });
          file = await this.fileRepository.save({
            user,
            originFileName,
            fileName,
            fileSize,
            folderName,
            uniqueIdentifier: uniqueId,
            status: FileProcessingStatus.chunksUploaded,
            lastModifiedStatus: new Date(),
            verifyingProgress: 0,
          });
        }

        try {
          await this.kafkaService.sendMessage({
            topic: Constants.KAFKA_CONFIG.topics.chunksUploaded.name,
            messages: [
              {
                value: JSON.stringify({
                  id: file.id,
                  fileName,
                  folderName,
                  originFileName,
                }),
              },
            ],
          });
        } catch (error) {
          await this.fileRepository.update(file.id, {
            status: FileProcessingStatus.mergeFileFailed,
          });
          this.logger.error(`${FUNC_NAME}: ${error}`);
        }
        return {
          message: Constants.MESSAGES.success.file.upload_success,
        };
      } else {
        return {
          statusCode: 200,
        };
      }
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      throw new InternalServerErrorException(error);
    }
  }

  async getFiles(
    userId: number,
    { limit, page, searchValue, dateFilter, statusFilter }: IFileFilter,
  ): Promise<{
    items: FileDto[];
    totalCount: number;
  }> {
    const FUNC_NAME = 'getFiles';
    try {
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({
          limit,
          page,
          searchValue,
          dateFilter,
          statusFilter,
        })}`,
      );
      const filter = {
        user: { id: userId },
        originFileName: ILike(`%${searchValue || ''}%`),
        status: undefined,
        createdAt: undefined,
      };
      if (statusFilter) {
        const parsedStatus = statusFilter.split(',');
        filter.status = In(parsedStatus);
      }
      if (dateFilter) {
        const currentDate = new Date();
        const dayOfWeek = currentDate.getDay();
        switch (dateFilter) {
          case DataRangeFilter.today:
            filter.createdAt = Between(
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate(),
                0,
                0,
                0,
                0,
              ),
              currentDate,
            );
            break;

          case DataRangeFilter.yesterday:
            const yesterday = new Date(currentDate);
            yesterday.setDate(currentDate.getDate() - 1);
            filter.createdAt = Between(
              new Date(
                yesterday.getFullYear(),
                yesterday.getMonth(),
                yesterday.getDate(),
                0,
                0,
                0,
                0,
              ),
              new Date(
                yesterday.getFullYear(),
                yesterday.getMonth(),
                yesterday.getDate(),
                23,
                59,
                59,
                999,
              ),
            );
            break;

          case DataRangeFilter.thisWeek:
            const monday = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDate() - dayOfWeek + 1,
            );
            filter.createdAt = Between(
              new Date(
                monday.getFullYear(),
                monday.getMonth(),
                monday.getDate(),
                0,
                0,
                0,
                0,
              ),
              currentDate,
            );
            break;

          case DataRangeFilter.last7Days:
            const last7Days = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDate() - 6,
            );
            filter.createdAt = Between(
              new Date(
                last7Days.getFullYear(),
                last7Days.getMonth(),
                last7Days.getDate(),
                0,
                0,
                0,
                0,
              ),
              currentDate,
            );
            break;

          case DataRangeFilter.lastWeek:
            const lastWeekMonday = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDate() - dayOfWeek - 6,
            );
            const lastWeekSunday = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDate() - dayOfWeek,
            );
            filter.createdAt = Between(
              new Date(
                lastWeekMonday.getFullYear(),
                lastWeekMonday.getMonth(),
                lastWeekMonday.getDate(),
                0,
                0,
                0,
                0,
              ),
              new Date(
                lastWeekSunday.getFullYear(),
                lastWeekSunday.getMonth(),
                lastWeekSunday.getDate(),
                23,
                59,
                59,
                999,
              ),
            );
            break;

          case DataRangeFilter.thisMonth:
            filter.createdAt = Between(
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                1,
                0,
                0,
                0,
                0,
              ),
              new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate(),
                23,
                59,
                59,
                999,
              ),
            );
            break;

          case DataRangeFilter.lastMonth:
            const lastMonth = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth() - 1,
              1,
            );
            const lastMonthLastDay = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              0,
            );
            filter.createdAt = Between(lastMonth, lastMonthLastDay);
            break;

          case DataRangeFilter.last30Days:
            const last30Days = new Date(
              currentDate.getFullYear(),
              currentDate.getMonth(),
              currentDate.getDate() - 29,
            );
            filter.createdAt = Between(last30Days, currentDate);
            break;

          case DataRangeFilter.thisYear:
            filter.createdAt = Between(
              new Date(currentDate.getFullYear(), 0, 1),
              currentDate,
            );
            break;

          case DataRangeFilter.lastYear:
            filter.createdAt = Between(
              new Date(currentDate.getFullYear() - 1, 0, 1),
              new Date(currentDate.getFullYear() - 1, 11, 31, 23, 59, 59, 999),
            );
            break;

          default:
            break;
        }
      }
      const cleanFilter = Helpers.removeUndefinedProperties(filter);
      const files = await this.fileRepository.find({
        where: cleanFilter,
        order: {
          createdAt: 'DESC',
        },
        skip: parseInt(page) * parseInt(limit),
        take: parseInt(limit),
      });
      const totalCount = await this.fileRepository.count({
        where: cleanFilter,
      });
      const convertedFiles = plainToInstance(FileDto, files, {
        excludeExtraneousValues: true,
      });
      return {
        items: convertedFiles,
        totalCount,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      throw new InternalServerErrorException(error);
    }
  }

  async checkDuplicate(uniqueIdentifier: string): Promise<{
    status: HttpStatus;
  }> {
    try {
      const FUNC_NAME = 'checkDuplicate';
      this.logger.info(`${FUNC_NAME}: ${uniqueIdentifier}`);
      const file = await this.fileRepository.findOne({
        where: {
          uniqueIdentifier,
        },
      });
      if (file) {
        return {
          status: HttpStatus.failed,
        };
      }
      return {
        status: HttpStatus.success,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async fileVerify(id: number): Promise<IFileVerifyResponse> {
    const FUNC_NAME = 'fileVerify';
    this.logger.info(`${FUNC_NAME}: ${id}`);
    const file = await this.fileRepository.findOne({ where: { id } });
    try {
      let topic;
      const initialFileStatus = file.status;
      switch (initialFileStatus) {
        case FileProcessingStatus.mergeFileSuccess:
        case FileProcessingStatus.fileVerifyFailed:
          file.status = FileProcessingStatus.fileVerifying;
          topic = Constants.KAFKA_CONFIG.topics.emailVerify.name;
          break;
        case FileProcessingStatus.makeFileFailed:
          topic = Constants.KAFKA_CONFIG.topics.cloudUploadedVerified.name;
          file.status = FileProcessingStatus.fileVerifySuccess;
          break;
        default:
          throw new BadRequestException(
            Constants.MESSAGES.failed.file.invalid_request,
          );
      }
      await this.fileRepository.update(id, {
        lastModifiedStatus: new Date(),
        status: file.status,
      });

      try {
        await this.kafkaService.sendMessage({
          topic,
          messages: [
            {
              value: JSON.stringify({
                id: file.id,
                folderName: file.folderName,
                fileName: file.fileName,
              }),
            },
          ],
        });
        return {
          id: file.id,
          status: file.status,
          verifyingProgress: file.verifyingProgress,
        };
      } catch (error) {
        this.logger.error(`${FUNC_NAME}: ${error}`);
        this.fileRepository.update(id, { status: initialFileStatus });
        throw error;
      }
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async quickVerify(
    userId: number,
    emails: string[],
    res: Response,
  ): Promise<void | {
    code: number;
    message: string;
    additionalInfos: {
      credits: number;
    };
  }> {
    const FUNC_NAME = 'quickVerify';
    try {
      this.logger.info(FUNC_NAME);
      const folderName = `${userId}_${Date.now()}`;
      const fileName = `${
        Constants.WEBSITE_NAME
      }_quick_verification_${Date.now()}.csv`;
      const verifiedAt = new Date();
      const {
        total,
        valid,
        invalid,
        unknown,
        syntaxError,
        avatarCount,
        status,
        lastModifiedStatus,
        verifyingProgress,
      } = await this.quickVerification(emails, folderName, fileName);
      const chargeCredits = Helpers.getChargeCredits({
        valid,
        invalid,
        syntaxError,
      });
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      const { status: creditAvailableStatus, requiredAmount } =
        Helpers.checkUserCredits(user.credits, chargeCredits);
      if (creditAvailableStatus) {
        const inputDir = `${join('.')}/src/uploads/${folderName}`;
        const files = fs.readdirSync(inputDir);
        if (!files?.length) {
          throw new BadRequestException('No file found');
        }
        const filesRemoveDuplicate = map(files, async (f) => {
          const filePath = join(inputDir, f);
          await Helpers.duplicateLineRemover(filePath);
        });
        await Promise.all(filesRemoveDuplicate);
        const downloadFile = `${Constants.WEBSITE_NAME}_${FileDownloadOptions.allResults}.zip`;
        const archive = archiver('zip', { zlib: { level: 9 } });
        res.attachment(downloadFile);
        archive.pipe(res);
        archive.directory(inputDir, false);
        archive.finalize();
        archive.on('finish', async () => {
          const user = await this.userRepository.findOne({
            where: { id: userId },
            relations: {
              performanceStats: true,
            },
          });
          await this.fileRepository.save({
            user,
            originFileName: fileName,
            fileName,
            folderName,
            fileSize: 0,
            valid,
            total,
            invalid,
            unknown,
            syntaxError,
            avatarCount,
            status,
            lastModifiedStatus,
            verifyingProgress,
            verifiedAt,
            downloadAvailable: true,
            credits: chargeCredits,
          });
          await this.userPerformanceStatsRepository.update(
            user.performanceStats.id,
            {
              total: total + user.performanceStats.total,
              valid: valid + user.performanceStats.valid,
              invalid: invalid + user.performanceStats.invalid,
              unknown: unknown + user.performanceStats.unknown,
              syntaxError: syntaxError + user.performanceStats.syntaxError,
            },
          );
          await this.userRepository.decrement(
            {
              id: user.id,
            },
            'credits',
            chargeCredits,
          );
          await this.verifiedUploader(folderName, fileName);
          this.logger.info(
            `${FUNC_NAME}: "Upload quick verification files successfully"`,
          );
        });
      } else {
        return {
          ...Constants.MESSAGES.failed.user.additional_verify_credit_required,
          additionalInfos: {
            credits: requiredAmount,
          },
        };
      }
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error.stack}`);
      throw new InternalServerErrorException(error);
    }
  }

  async downloadFile(
    id: number,
    option: FileDownloadOptions,
    file: FileEntity,
    res: Response,
  ): Promise<any> {
    try {
      const FUNC_NAME = 'downloadFile';
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({
          id,
          option,
        })}`,
      );
      const { folderName, fileName } = file;
      const fileNameWithoutExt = fileName.slice(0, -4);
      await this.fileDownloader(folderName, fileNameWithoutExt, option);
      const folderPath = join('.') + '/src/uploads';
      const inputDir = `${folderPath}/${folderName}`;
      if (option === FileDownloadOptions.allResults) {
        const downloadFile = `${fileNameWithoutExt}_${FileDownloadOptions.allResults}.zip`;
        const archive = archiver('zip', { zlib: { level: 9 } });
        res.attachment(downloadFile);
        archive.pipe(res);
        archive.directory(inputDir, false);
        archive.finalize();
        archive.on('finish', async () => {
          await Helpers.removeFilesInDir(inputDir).catch((error) => {
            this.logger.error(`${FUNC_NAME}: ${error}`);
          });
        });
      } else {
        const filePath = `${inputDir}/${fileNameWithoutExt}_${option}.csv`;
        this.logger.info(filePath);
        return res.download(filePath);
      }
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async deleteFile(
    userIdReq: number,
    ids: string[],
  ): Promise<{ message: string }> {
    const FUNC_NAME = 'deleteFile';
    this.logger.info(`${FUNC_NAME}: ${JSON.stringify(ids)}`);
    try {
      const promises = map(ids, (i) => {
        return new Promise(async (resolve, reject) => {
          const file = await this.fileRepository.findOne({
            where: { id: i },
            relations: {
              user: true,
            },
          });
          if (!file)
            return reject(
              new Error(Constants.MESSAGES.failed.file.file_not_found.message),
            );
          if (file.user.id !== userIdReq)
            return reject(
              new Error(
                Constants.MESSAGES.failed.file.delete_not_allowed.message,
              ),
            );
          resolve(file);
        });
      });
      const filesToDelete = await Promise.all(promises);
      const isDeleteNotAllowed = find(filesToDelete, (f) =>
        [
          FileProcessingStatus.chunksUploaded,
          FileProcessingStatus.mergeFileFailed,
          FileProcessingStatus.fileVerifying,
          FileProcessingStatus.fileVerifySuccess,
        ].includes(f.status),
      );
      if (isDeleteNotAllowed) {
        throw new ConflictException(
          Constants.MESSAGES.failed.file.delete_not_allowed_file_processing,
        );
      }
      const folderToRemove = map(filesToDelete, (f) => {
        const inputDir = `${join('.')}/src/uploads/${f.folderName}`;
        return new Promise<void>((resolve) => {
          fs.rm(inputDir, { recursive: true }, (error) => {
            if (error) {
              this.logger.error(`${FUNC_NAME}: ${error}`);
            }
            resolve();
          });
        });
      });
      await Promise.all(folderToRemove);
      await this.fileRepository.softDelete(ids);
      return {
        message: Constants.MESSAGES.success.file.file_delete_success,
      };
    } catch (error) {
      if (
        error.message === Constants.MESSAGES.failed.file.file_not_found.message
      ) {
        throw new BadRequestException(
          Constants.MESSAGES.failed.file.one_or_more_ids_not_exist,
        );
      } else if (
        error.message ===
        Constants.MESSAGES.failed.file.delete_not_allowed.message
      ) {
        throw new ForbiddenException(
          Constants.MESSAGES.failed.file.delete_not_allowed,
        );
      }
      throw new InternalServerErrorException(error);
    }
  }

  async checkFileStatus(
    userId: number,
    ids: string[],
  ): Promise<{
    files: FileDto[];
    remainCredits: number;
  }> {
    try {
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      if (!user) throw new UserNotFoundException();
      const files = await this.fileRepository.find({
        where: { id: In(ids) },
      });
      const convertedFiles = plainToInstance(FileDto, files, {
        excludeExtraneousValues: true,
      });
      return {
        files: convertedFiles,
        remainCredits: user.credits,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getChartData(
    userId: number,
    breakdown: string,
    label: string,
  ): Promise<
    | {
        values: any[];
        labels: string[];
        breakdown: string;
      }
    | { message: string }
  > {
    const FUNC_NAME = 'getChartData';
    this.logger.info(
      `${FUNC_NAME}: ${JSON.stringify({
        breakdown,
        label,
      })}`,
    );
    try {
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      if (!user) throw new UserNotFoundException();
      const { timezone } = user;
      let startHour, endHour, startDate, endDate;
      switch (label) {
        case DataRangeFilter.today:
          startHour = 0;
          endHour = momentTz().tz(timezone).hours();
          startDate = momentTz().tz(timezone).startOf('day');
          endDate = momentTz().tz(timezone).endOf('day');
          break;
        case DataRangeFilter.yesterday:
          startHour = 0;
          endHour = 23;
          startDate = momentTz()
            .tz(timezone)
            .subtract(1, 'days')
            .startOf('day');
          endDate = momentTz().tz(timezone).subtract(1, 'days').endOf('day');
          break;
        case DataRangeFilter.thisWeek:
          startDate = momentTz().tz(timezone).clone().startOf('week');
          endDate = momentTz().tz(timezone).endOf('day');
          break;
        case DataRangeFilter.last7Days:
          startDate = momentTz()
            .tz(timezone)
            .subtract(6, 'days')
            .startOf('day');
          endDate = momentTz().tz(timezone).endOf('day');
          break;
        case DataRangeFilter.thisMonth:
          startDate = momentTz().tz(timezone).startOf('month');
          endDate = momentTz().tz(timezone).endOf('day');
          break;
        case DataRangeFilter.lastMonth:
          startDate = momentTz()
            .tz(timezone)
            .subtract(1, 'months')
            .startOf('month');
          endDate = momentTz()
            .tz(timezone)
            .subtract(1, 'months')
            .endOf('month');
          break;
        case DataRangeFilter.thisYear:
          startDate = momentTz().tz(timezone).startOf('year');
          endDate = momentTz().tz(timezone).endOf('day');
          break;
        default:
          break;
      }
      console.log({
        startHour,
        endHour,
        startDate,
        endDate,
      });
      const queryBuilder = await this.fileRepository.createQueryBuilder('file');

      queryBuilder
        .select([
          `DATE_FORMAT(file.verifiedAt, ${
            breakdown === 'HOUR' ? "'%Y-%m-%d %H'" : "'%Y-%m-%d'"
          })`,
          'SUM(file.total) as total',
          'SUM(file.valid) as valid',
          'SUM(file.invalid) as invalid',
          'SUM(file.unknown) as unknown',
          'SUM(file.avatarCount) as avatarCount',
        ])
        .where('file.user = :userId', { userId })
        .andWhere('file.verifiedAt >= :startDate', {
          startDate: startDate.toDate(),
        })
        .andWhere('file.verifiedAt <= :endDate', { endDate: endDate.toDate() })
        .groupBy(
          `DATE_FORMAT(file.verifiedAt, ${
            breakdown === 'HOUR' ? "'%Y-%m-%d %H'" : "'%Y-%m-%d'"
          })`,
        )
        .orderBy(
          `DATE_FORMAT(file.verifiedAt, ${
            breakdown === 'HOUR' ? "'%Y-%m-%d %H'" : "'%Y-%m-%d'"
          })`,
        );

      const files = await queryBuilder.getRawMany();

      const labels = files.map(
        (file) => file["DATE_FORMAT(`file`.`verified_at`, '%Y-%m-%d %H')"],
      );
      const totalData = files.map((file) => parseInt(file.total));
      const validData = files.map((file) => parseInt(file.valid));
      const avatarCountData = files.map((file) => parseInt(file.avatarCount));
      const invalidData = files.map((file) => parseInt(file.invalid));
      const unknownData = files.map((file) => parseInt(file.unknown));

      const values = [
        {
          label: 'total',
          data: totalData,
        },
        {
          label: 'valid',
          data: validData,
        },
        {
          label: 'avatarCount',
          data: avatarCountData,
        },
        {
          label: 'invalid',
          data: invalidData,
        },
        {
          label: 'unknown',
          data: unknownData,
        },
      ];

      if (!files.length)
        return {
          message: Constants.MESSAGES.success.file.no_data_available,
        };
      const fullTimeInfo = Helpers.addMissingTime(
        {
          values,
          labels,
        },
        breakdown,
        {
          startHour,
          endHour,
          startDate,
          endDate,
          timezone,
        },
      );
      return fullTimeInfo;
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error.stack}`);
      throw new InternalServerErrorException(error);
    }
  }

  async getEmailVerifiedResults(
    emailsListVerify,
    inputDir,
    fileName,
    fileHeader,
    emailColumnName,
  ): Promise<{
    validItems: any[];
    haveAvatarItems: any[];
    invalidItems: any[];
    unknownItems: any[];
  }> {
    const FUNC_NAME = 'getEmailVerifiedResults';
    let validItems = [];
    let haveAvatarItems = [];
    let invalidItems = [];
    const unknownItems = [];
    const unProcessItems = [];
    if (emailsListVerify.length) {
      try {
        const listVerified = await this.verifier(
          emailsListVerify.map((e) => e.verifying),
        );
        const itemsToWrite = map(
          emailsListVerify,
          async ({ verifying, ...row }, index) => {
            try {
              const email = find(listVerified, (l) => l.email === verifying);
              if (email) {
                if (!email.email.includes('@gmail.com')) {
                  unProcessItems[index] = {
                    index,
                    email: row[emailColumnName],
                    row,
                    type: email.photoUri
                      ? EmailVerifyStatus.haveAvatar
                      : EmailVerifyStatus.valid,
                  };
                } else {
                  if (email.photoUri) {
                    haveAvatarItems[index] = row;
                  }
                  validItems[index] = row;
                }
              } else if (verifying.includes('@gmail.com')) {
                invalidItems[index] = row;
              } else {
                unProcessItems[index] = {
                  index,
                  email: row[emailColumnName],
                  row,
                };
              }
            } catch (error) {
              throw error;
            }
          },
        );
        await Promise.all(itemsToWrite);

        for (const item of unProcessItems) {
          if (item) {
            const { index, email, row, type } = item;
            const isTypoError = Helpers.checkTypoError(email);
            if (isTypoError) {
              invalidItems[index] = row;
            } else {
              const status = await Helpers.checkDomainMXRecord(email);
              if (status) {
                switch (type) {
                  case EmailVerifyStatus.valid:
                    validItems[index] = row;
                    break;
                  case EmailVerifyStatus.haveAvatar:
                    haveAvatarItems[index] = row;
                    validItems[index] = row;
                    break;
                  default:
                    unknownItems.push(row);
                    break;
                }
              } else {
                invalidItems[index] = row;
              }
            }
          }
        }
        validItems = filter(validItems, (i) => i);
        haveAvatarItems = filter(haveAvatarItems, (i) => i);
        invalidItems = filter(invalidItems, (i) => i);

        this.writeEmails(
          inputDir,
          fileName,
          fileHeader,
          validItems,
          haveAvatarItems,
          invalidItems,
          unknownItems,
          emailColumnName,
        );
      } catch (error) {
        this.logger.error(`${FUNC_NAME}: ${error}`);
        throw error;
      }
    }
    return { validItems, haveAvatarItems, invalidItems, unknownItems };
  }

  private async writeEmails(
    inputDir: string,
    fileName: string,
    fileHeader: { header: string; key: string }[],
    validItems: any[],
    haveAvatarItems: any[],
    invalidItems: any[],
    unknownItems: any[],
    emailColumnName: string,
  ): Promise<void> {
    const validPath = `${inputDir}/${fileName}_${FileDownloadSuffix.valid}.csv`;
    const invalidPath = `${inputDir}/${fileName}_${FileDownloadSuffix.invalid}.csv`;
    const haveAvatarPath = `${inputDir}/${fileName}_${FileDownloadSuffix.haveAvatar}.csv`;
    const unknownPath = `${inputDir}/${fileName}_${FileDownloadSuffix.unknown}.csv`;
    await Promise.all([
      CsvWriter.write(
        validPath,
        fileHeader,
        Helpers.additionalVerifyColumns(
          validItems,
          EmailVerifyStatus.valid,
          emailColumnName,
        ),
      ),
      CsvWriter.write(
        haveAvatarPath,
        fileHeader,
        Helpers.additionalVerifyColumns(
          haveAvatarItems,
          EmailVerifyStatus.haveAvatar,
          emailColumnName,
        ),
      ),
      CsvWriter.write(
        invalidPath,
        fileHeader,
        Helpers.additionalVerifyColumns(
          invalidItems,
          EmailVerifyStatus.invalid,
          emailColumnName,
        ),
      ),
      CsvWriter.write(
        unknownPath,
        fileHeader,
        Helpers.additionalVerifyColumns(
          unknownItems,
          EmailVerifyStatus.unknown,
          emailColumnName,
        ),
      ),
    ]);
  }

  private async verifier(emails: string[]): Promise<
    {
      email: string;
      photoUri: string;
    }[]
  > {
    // Business logic removed!
  }

  public async chunksCombiner(
    folderName: string,
    fileName: string,
    fileExt: string,
  ): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const FUNC_NAME = 'chunksCombiner';
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({ folderName, fileName, fileExt })}`,
      );

      const folderPath = `${join('.')}/src/uploads`;
      const inputDir = `${folderPath}/${folderName}`;
      const outputFile = `${inputDir}/${fileName}`;

      if (!fs.existsSync(outputFile)) {
        try {
          const chunkFileNames = fs.readdirSync(inputDir);
          const chunkFilePaths = chunkFileNames.map((file) =>
            join(inputDir, file),
          );

          chunkFilePaths.sort(
            (a, b) =>
              parseInt(a.split('-').pop()) - parseInt(b.split('-').pop()),
          );

          const writable = fs.createWriteStream(outputFile);
          const readStreams = chunkFilePaths
            .filter((c) => c.endsWith(Constants.TEMPORARY_FILE_EXT))
            .map((chunk) => fs.createReadStream(chunk));

          const buffers = [];
          for (let i = 0; i < readStreams.length; i++) {
            // Use concat-stream to collect all data from the ReadStream
            const readStream = readStreams[i];
            readStream.pipe(
              concat((buffer) => {
                if (i === 0 && fileExt === 'txt') {
                  buffer = Buffer.concat([Buffer.from('\n'), buffer]);
                }
                buffers[i] = buffer;
              }),
            );
          }

          // Wait for all ReadStreams to finish
          await Promise.all(
            readStreams.map(
              (rs) => new Promise((resolve) => rs.on('end', resolve)),
            ),
          )
            .then(() => {
              pipeline(buffers, writable, async (error) => {
                if (error) {
                  throw error;
                } else {
                  if (fileExt !== 'csv') {
                    try {
                      const workBook = XLSX.readFile(outputFile, {
                        raw: true,
                        FS: ',',
                      });
                      if (fileExt === 'txt') {
                        const headerRow = [Constants.DEFAULT_HEADER_TXT];
                        const worksheet =
                          workBook.Sheets[workBook.SheetNames[0]];
                        XLSX.utils.sheet_add_aoa(worksheet, [headerRow], {
                          origin: 0,
                        });
                      }
                      XLSX.writeFile(
                        workBook,
                        Helpers.changeFileExt(outputFile),
                        {
                          bookType: 'csv',
                        },
                      );
                    } catch (error) {
                      throw new Error(FileErrorReason.readingError);
                    }
                  }
                  const unlinkPromises = chunkFilePaths.map((filePath) => {
                    return new Promise<void>((resolve, reject) => {
                      fs.unlink(filePath, (error) => {
                        if (error) {
                          reject(error);
                        } else {
                          resolve();
                        }
                      });
                    });
                  });
                  await Promise.all(unlinkPromises)
                    .then(() => {
                      this.logger.debug('Files merged successfully');
                      resolve();
                    })
                    .catch((error) => {
                      this.logger.error(`${FUNC_NAME}: ${error}`);
                      return reject(error);
                    });
                }
              });
            })
            .catch((error) => {
              this.logger.error(`${FUNC_NAME}: ${error}`);
              reject(error);
            });
        } catch (error) {
          this.logger.error(`${FUNC_NAME}: ${error}`);
          return reject(error);
        }
      } else {
        this.logger.debug('File combine already');
        resolve();
      }
    });
  }

  public async emailVerifyHandler({
    id,
    folderName,
    fileName,
  }: {
    id: number;
    folderName: string;
    fileName: string;
  }): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const FUNC_NAME = 'emailVerifyHandler';
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({ id, folderName, fileName })}`,
      );
      const folderPath = `${join('.')}/src/uploads`;
      const inputDir = `${folderPath}/${folderName}`;
      const fileNameWithoutExt = fileName.slice(0, -4);
      const isFileDeleted = !fs.existsSync(inputDir);
      if (!isFileDeleted) {
        try {
          const files = fs.readdirSync(inputDir);
          try {
            const filesToRemove = filter(
              files,
              (f) => !f.endsWith(Constants.TEMPORARY_FILE_EXT),
            );
            await this.removeWriteInterrupted(filesToRemove, inputDir);
            const filteredFiles = filter(files, (f) =>
              f.endsWith(Constants.TEMPORARY_FILE_EXT),
            ).sort(
              (a, b) =>
                parseInt(a.split('_').pop()) - parseInt(b.split('_').pop()),
            );
            let validRows = 0;
            let invalidRows = 0;
            let unknownRows = 0;
            let syntaxErrorRows = 0;
            let avatarCountRows = 0;
            const totalFileRequest = filteredFiles.length;
            for (let i = 0; i < totalFileRequest; i++) {
              if (isFileDeleted) {
                throw new Error('File delete previous');
              }
              const filePath = join(inputDir, filteredFiles[i]);
              if (filePath.endsWith(Constants.TEMPORARY_FILE_EXT)) {
                await new Promise<void>(async (resolve, reject) => {
                  const rows = [];
                  const fileContent = fs.readFileSync(filePath, 'utf8');
                  parseString(fileContent, {
                    headers: true,
                    discardUnmappedColumns: true,
                  })
                    .on('error', (error) => {
                      reject(error);
                    })
                    .on('data', (row) => rows.push(row))
                    .on('end', async () => {
                      const emailColumnName = Helpers.findEmailHeaderColumn(
                        rows[0],
                      );
                      const fileHeader = map(Object.keys(rows[0]), (r) => ({
                        key: r,
                        header: r,
                      })).concat(Constants.ADDITIONAL_COLUMNS);
                      try {
                        const syntaxData = Helpers.emailsSyntaxValidAndInvalid(
                          rows,
                          emailColumnName,
                        );
                        if (syntaxData.invalidRows.length) {
                          const syntaxErrorPath = `${inputDir}/${fileNameWithoutExt}_${FileDownloadOptions.syntaxError}.csv`;
                          await CsvWriter.write(
                            syntaxErrorPath,
                            fileHeader,
                            Helpers.additionalVerifyColumns(
                              syntaxData.invalidRows,
                              EmailVerifyStatus.syntaxError,
                              emailColumnName,
                            ),
                          );
                          syntaxErrorRows += syntaxData.invalidRows.length;
                        }
                        const emailsListVerify = map(
                          syntaxData.validRows,
                          (e) => ({
                            ...e,
                            verifying: Helpers.cleanGmailDomain(
                              e[emailColumnName],
                            ),
                          }),
                        );
                        const {
                          validItems,
                          haveAvatarItems,
                          invalidItems,
                          unknownItems,
                        } = await this.getEmailVerifiedResults(
                          emailsListVerify,
                          inputDir,
                          fileNameWithoutExt,
                          fileHeader,
                          emailColumnName,
                        );
                        validRows += validItems.length;
                        avatarCountRows += haveAvatarItems.length;
                        invalidRows += invalidItems.length;
                        unknownRows += unknownItems.length;
                        if (i % Constants.PROCESS_AFTER_PER === 0) {
                          const verifyingProgress = Math.round(
                            (i / totalFileRequest) * 100,
                          );
                          const file = await this.fileRepository.findOne({
                            where: { id },
                          });
                          if (!file) {
                            return reject(new Error('File not found'));
                          }
                          file.verifyingProgress = verifyingProgress;
                          await this.fileRepository.save(file);
                        }
                      } catch (error) {
                        this.logger.error(`${FUNC_NAME}: ${error}`);
                        return reject(error);
                      }
                      resolve();
                    });
                });
              }
            }
            const file = await this.fileRepository.findOne({
              where: { id },
            });
            if (!file) {
              return reject(new Error('File not found'));
            }
            file.valid = validRows;
            file.invalid = invalidRows;
            file.unknown = unknownRows;
            file.syntaxError = syntaxErrorRows;
            file.avatarCount = avatarCountRows;
            file.status = FileProcessingStatus.fileVerifySuccess;
            file.lastModifiedStatus = new Date();
            await this.fileRepository.save(file);
            try {
              await this.kafkaService.sendMessage({
                topic: Constants.KAFKA_CONFIG.topics.cloudUploadedVerified.name,
                messages: [
                  {
                    value: JSON.stringify({
                      id,
                      fileName,
                      folderName,
                      validRows,
                      invalidRows,
                      unknownRows,
                      syntaxErrorRows,
                    }),
                  },
                ],
              });
              resolve();
              const unlinkPromises = filteredFiles.map((file) => {
                const filePath = `${inputDir}/${file}`;
                return new Promise<void>((resolve, reject) => {
                  fs.unlink(filePath, (error) => {
                    if (error) {
                      this.logger.error(`${FUNC_NAME}: ${error}`);
                      reject(error);
                    } else {
                      resolve();
                    }
                  });
                });
              });
              Promise.all(unlinkPromises).catch((error) => {
                this.logger.error(`${FUNC_NAME}: ${error}`);
              });
            } catch (error) {
              reject(error);
            }
          } catch (error) {
            this.logger.error(`${FUNC_NAME}: ${error}`);
            reject(error);
          }
        } catch (error) {
          this.logger.error(
            `${FUNC_NAME}: Error getting directory information`,
          );
          throw error;
        }
      } else {
        this.logger.error(`${FUNC_NAME}: File delete previous`);
        reject(new Error('File delete previous'));
      }
    });
  }

  public async removeWriteInterrupted(files: string[], inputDir: string) {
    const FUNC_NAME = 'removeWriteInterrupted';
    const unlinkPromises = files.map((file) => {
      const filePath = `${inputDir}/${file}`;
      return new Promise<void>((resolve, reject) => {
        fs.unlink(filePath, (error) => {
          if (error) {
            this.logger.error(`${FUNC_NAME}: ${error}`);
            reject(error);
          } else {
            resolve();
          }
        });
      });
    });
    return await Promise.all(unlinkPromises);
  }

  public async fileCleaner(
    folderName: string,
    fileName: string,
  ): Promise<{
    duplicate: number;
    total: number;
    status: FileProcessingStatus;
    lastModifiedStatus: Date;
  }> {
    return new Promise((resolve, reject) => {
      const FUNC_NAME = 'fileCleaner';
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({ folderName, fileName })}`,
      );
      const inputFile = `${join('.')}/src/uploads/${folderName}/${fileName}`;

      if (!fs.existsSync(inputFile)) return;

      try {
        const rows = [];
        fs.createReadStream(inputFile)
          .pipe(
            parse({
              headers: (headers) => {
                headers.map((el, i, ar) => {
                  if (ar.indexOf(el) !== i || !el.trim()) {
                    headers[i] = `${el}${Constants.DOMAIN_NAME}_${i}`;
                  }
                });
                return headers;
              },
              discardUnmappedColumns: true,
            }),
          )
          .on('error', (error) => {
            this.logger.error(`${FUNC_NAME}: ${error}`);
            return reject(new Error(FileErrorReason.readingError));
          })
          .on('data', (row) => rows.push(row))
          .on('end', async () => {
            if (!rows.length) {
              return reject(new Error(FileErrorReason.fileEmpty));
            }
            const columnToCheck = Helpers.findEmailHeaderColumn(rows[0]);
            if (!columnToCheck) {
              return reject(new Error(FileErrorReason.missingHeader));
            }
            let removeEmptyIndex = 0;
            while (removeEmptyIndex < rows.length) {
              const row = rows[removeEmptyIndex];
              if (!row[columnToCheck] || !row[columnToCheck].trim()) {
                rows.splice(removeEmptyIndex, 1);
              } else {
                row[columnToCheck] = row[columnToCheck].trim().toLowerCase();
                removeEmptyIndex++;
              }
            }
            const uniqueEmails = uniqBy(rows, (row) => row[columnToCheck]);

            if (!uniqueEmails.length) {
              return reject(new Error(FileErrorReason.noDataFound));
            }
            const numDuplicates = rows.length - uniqueEmails.length;

            const fileHeader = map(
              Object.keys(rows[0]).filter((o) => o),
              (r) => ({
                key: r,
                header: r,
              }),
            );
            for (
              let i = 0;
              i < uniqueEmails.length;
              i += Constants.EMAILS_VERIFY_PER_REQUEST
            ) {
              const batch = uniqueEmails.slice(
                i,
                i + Constants.EMAILS_VERIFY_PER_REQUEST,
              );
              const batchFilename = `${inputFile.slice(0, -4)}_${
                i / Constants.EMAILS_VERIFY_PER_REQUEST + 1
              }${Constants.TEMPORARY_FILE_EXT}`;
              if (!fs.existsSync(batchFilename)) {
                await CsvWriter.write(batchFilename, fileHeader, batch);
              }
            }
            resolve({
              duplicate: numDuplicates,
              total: rows.length,
              status: FileProcessingStatus.mergeFileSuccess,
              lastModifiedStatus: new Date(),
            });
            fs.unlink(inputFile, (error) => {
              if (error) {
                this.logger.error(`${FUNC_NAME}: ${error}`);
              }
            });
          });
      } catch (error) {
        this.logger.error(`${FUNC_NAME}: ${error}`);
        reject(new Error(FileErrorReason.readingError));
      }
    });
  }

  public async fileDownloader(
    folderName: string,
    fileNameWithoutExt: string,
    option: FileDownloadOptions,
  ): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const FUNC_NAME = 'fileDownloader';
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({
          folderName,
          fileNameWithoutExt,
          option,
        })}`,
      );
      const uploadDir = `${join('.')}/src/uploads`;

      const downloadOptions = {
        downloadPath: `${folderName}/${fileNameWithoutExt}_${option}.csv`,
      };

      const bucketName = this.configService.get('gcp').bucketName;
      try {
        if (FileDownloadOptions.allResults === option) {
          const [files] = await this.cloudStorage
            .getInstance()
            .bucket(bucketName)
            .getFiles({
              prefix: folderName,
            });
          for (const file of files) {
            if (!file.name.endsWith(`${fileNameWithoutExt}.csv`)) {
              const filePath = `${uploadDir}/${file.name}`;
              await file.download({
                destination: filePath,
              });
            }
          }
        } else {
          await this.cloudStorage
            .getInstance()
            .bucket(bucketName)
            .file(downloadOptions.downloadPath)
            .download({
              destination: `${uploadDir}/${downloadOptions.downloadPath}`,
            });
        }
        this.logger.debug('File download successfully');
        resolve();
      } catch (error) {
        this.logger.error(`${FUNC_NAME}: ${error}`);
        reject(error);
      }
    });
  }

  public async quickVerification(
    emails: string[],
    folderName: string,
    fileName: string,
  ): Promise<{
    total: number;
    valid: number;
    invalid: number;
    unknown: number;
    syntaxError: number;
    avatarCount: number;
    status: FileProcessingStatus;
    lastModifiedStatus: Date;
    verifyingProgress: number;
  }> {
    const FUNC_NAME = 'quickVerification';
    this.logger.info(FUNC_NAME);
    const folderPath = `${join('.')}/src/uploads`;
    const inputDir = `${folderPath}/${folderName}`;
    const fileNameWithoutExt = fileName.slice(0, -4);
    try {
      fs.mkdirSync(inputDir);
    } catch (error) {
      throw error;
    }
    let validRows = 0;
    let invalidRows = 0;
    let unknownRows = 0;
    let syntaxErrorRows = 0;
    let avatarCountRows = 0;

    const emailColumnName = Constants.DEFAULT_HEADER_TXT;
    const itemsToVerify = map(emails, (i) => ({ [emailColumnName]: i }));
    const totalRequest = Math.ceil(
      itemsToVerify.length / Constants.EMAILS_VERIFY_PER_REQUEST,
    );
    for (let i = 0; i < totalRequest; i++) {
      try {
        const start = i * Constants.EMAILS_VERIFY_PER_REQUEST;
        const end = start + Constants.EMAILS_VERIFY_PER_REQUEST;
        const items = itemsToVerify.slice(start, end);
        const fileHeader = [
          { key: emailColumnName, header: emailColumnName },
          ...Constants.ADDITIONAL_COLUMNS,
        ];
        const syntaxData = Helpers.emailsSyntaxValidAndInvalid(
          items,
          emailColumnName,
        );
        if (syntaxData.invalidRows.length) {
          const validPath = `${inputDir}/${fileNameWithoutExt}_${FileDownloadOptions.syntaxError}.csv`;
          await CsvWriter.write(
            validPath,
            fileHeader,
            Helpers.additionalVerifyColumns(
              syntaxData.invalidRows,
              EmailVerifyStatus.syntaxError,
              emailColumnName,
            ),
          );
          syntaxErrorRows += syntaxData.invalidRows.length;
        }
        const emailsListVerify = map(syntaxData.validRows, (e) => ({
          ...e,
          verifying: Helpers.cleanGmailDomain(e[emailColumnName]),
        }));
        const { validItems, haveAvatarItems, invalidItems, unknownItems } =
          await this.getEmailVerifiedResults(
            emailsListVerify,
            inputDir,
            fileNameWithoutExt,
            fileHeader,
            emailColumnName,
          );
        validRows += validItems.length;
        avatarCountRows += haveAvatarItems.length;
        invalidRows += invalidItems.length;
        unknownRows += unknownItems.length;
      } catch (error) {
        this.logger.error(`${FUNC_NAME}: ${error}`);
        throw error;
      }
    }
    return {
      total: validRows + invalidRows + unknownRows + syntaxErrorRows,
      valid: validRows,
      invalid: invalidRows,
      unknown: unknownRows,
      syntaxError: syntaxErrorRows,
      avatarCount: avatarCountRows,
      status: FileProcessingStatus.makeFileSuccess,
      lastModifiedStatus: new Date(),
      verifyingProgress: 100,
    };
  }

  public async verifiedUploader(
    folderName: string,
    fileName: string,
  ): Promise<void> {
    const FUNC_NAME = 'verifiedUploader';
    this.logger.info(
      `${FUNC_NAME}: ${JSON.stringify({ folderName, fileName })}`,
    );
    const bucketName = this.configService.get('gcp').bucketName;
    const folderPath = `${join('.')}/src/uploads`;
    const inputDir = `${folderPath}/${folderName}`;

    try {
      const files = fs.readdirSync(inputDir);
      if (!files?.length) {
        throw new Error('No file found');
      }
      files
        .filter((file) => file.endsWith('.csv'))
        .forEach(async (file) => {
          try {
            const filePath = join(inputDir, file);
            await Helpers.duplicateLineRemover(filePath);
            const uploadPath = `${folderName}/${file}`;
            const uploadOptions = {
              destination: uploadPath,
              public: false,
            };
            try {
              const file = await this.cloudStorage
                .getInstance()
                .bucket(bucketName)
                .upload(filePath, uploadOptions);
              // @ts-expect-error: `name` property is in file object;
              this.logger.debug(`File ${file.name} uploaded to ${bucketName}.`);
              fs.unlink(filePath, (error) => {
                if (error) {
                  this.logger.error(`${FUNC_NAME}: ${error}`);
                }
              });
            } catch (error) {
              if (error) {
                this.logger.error(`${FUNC_NAME}: ${error}`);
                throw new Error(`Error uploading file: ${error.message}`);
              }
            }
          } catch (error) {
            throw error;
          }
        });
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      throw new Error(`Error getting directory content: ${error.message}`);
    }
  }

  public async updateMergeFileStatus(
    id: number,
    updateInfo: Partial<FileDto>,
  ): Promise<void> {
    const FUNC_NAME = 'updateMergeFileStatus';
    try {
      this.logger.info(
        `${FUNC_NAME}: ${JSON.stringify({ id, ...updateInfo })}`,
      );
      const file = await this.fileRepository.update(id, { ...updateInfo });
      if (!file) {
        this.logger.debug(`${FUNC_NAME}: File not found`);
        throw new Error('File not found');
      }
    } catch (error) {
      this.logger.debug(`${FUNC_NAME}: ${error}`);
      throw error;
    }
  }
}
