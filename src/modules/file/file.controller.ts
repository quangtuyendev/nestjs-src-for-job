import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Request, Response } from 'express';
import { IHttpErrorResponse } from 'src/common/http-error-response.common';
import { HttpStatus } from 'src/common/http-status.utils';
import { IUserDecoded } from '../auth/interfaces/user-decoded.interface';
import { FileDto } from './dto/file.dto';
import { FileEntity } from './entities/file.entity';
import { DataRangeFilter } from './enums/data-range-filter.enum';
import { FileDownloadOptions } from './enums/file-download-option.enum';
import { FileService } from './file.service';
import { IFileFilter } from './interfaces/file-filter.interface';
import { IFileUploadInfos } from './interfaces/file-upload-infos.interface';
import { IFileVerifyResponse } from './interfaces/file-verify-response.interface';
import { ChartDataInputValidationPipe } from './validators/chart-data-input.validation';
import { CheckDuplicateValidationPipe } from './validators/check-duplicate.validation';
import { CheckFileStatusValidationPipe } from './validators/check-file-status.validation';
import { FileDeleteValidationPipe } from './validators/file-delete.validation';
import {
  FileDownloadIdValidationPipe,
  FileDownloadOptionValidationPipe,
} from './validators/file-download.validation';
import { FileVerifyValidationPipe } from './validators/file-verify.validation';
import { GetFileListValidationPipe } from './validators/get-file-list.validation';
import { QuickVerifyValidationPipe } from './validators/quick-verify.validation';
import { AuthGuard } from 'src/guards/auth.guard';
import { LogError } from 'src/common/log-error.decorator';

@Controller('files')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post('upload')
  @UseGuards(AuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  @LogError(__filename)
  async uploadFile(
    @Req() req: Request & { user: IUserDecoded },
    @Res() res: Response,
    @Body() body: IFileUploadInfos,
  ): Promise<
    | {
        status: HttpStatus;
        data: Partial<{ message: string; statusCode: number }>;
      }
    | Response<string>
  > {
    const response = await this.fileService.uploadFile(req.user.userId, body);
    return response.statusCode
      ? res.sendStatus(response.statusCode)
      : res.status(200).json({
          status: HttpStatus.success,
          data: response,
        });
  }

  @Get('')
  @UseGuards(AuthGuard)
  async getFiles(
    @Query('', new GetFileListValidationPipe())
    { limit, page, searchValue, dateFilter, statusFilter }: IFileFilter,
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: {
      items: FileDto[];
      totalCount: number;
    };
  }> {
    const response = await this.fileService.getFiles(req.user.userId, {
      limit,
      page,
      searchValue,
      dateFilter,
      statusFilter,
    });
    return {
      status: HttpStatus.success,
      data: response,
    };
  }

  @Get('check-duplicate/:uniqueIdentifier')
  @UseGuards(AuthGuard)
  async checkDuplicate(
    @Param('', new CheckDuplicateValidationPipe())
    { uniqueIdentifier }: { uniqueIdentifier: string },
  ): Promise<{ status: HttpStatus }> {
    const status = await this.fileService.checkDuplicate(uniqueIdentifier);
    return status;
  }

  @Post('verify/:id')
  @UseGuards(AuthGuard)
  async fileVerify(
    @Param('', new FileVerifyValidationPipe())
    { id }: { id: string },
  ): Promise<{
    status: HttpStatus;
    data: IFileVerifyResponse;
  }> {
    const file = await this.fileService.fileVerify(parseInt(id));
    return {
      status: HttpStatus.success,
      data: {
        id: file.id,
        status: file.status,
        verifyingProgress: file.verifyingProgress,
      },
    };
  }

  @Post('quick-verification')
  @UseGuards(AuthGuard)
  async quickVerify(
    @Body('', new QuickVerifyValidationPipe()) { emails }: { emails: string[] },
    @Req() req: Request & { user: IUserDecoded },
    @Res() res: Response,
  ): Promise<void | {
    status: HttpStatus;
    data: IHttpErrorResponse & {
      additionalInfos: {
        credits: number;
      };
    };
  }> {
    const data = await this.fileService.quickVerify(
      req.user.userId,
      emails,
      res,
    );
    return data
      ? {
          status: HttpStatus.success,
          data,
        }
      : undefined;
  }

  @Post('download/:id')
  @UseGuards(AuthGuard)
  async downloadFile(
    @Param('', new FileDownloadIdValidationPipe()) { id }: { id: string },
    @Body('', new FileDownloadOptionValidationPipe())
    { option }: { option: FileDownloadOptions },
    @Req() req: Request & { fileRecord: FileEntity },
    @Res() res: Response,
  ): Promise<void> {
    await this.fileService.downloadFile(
      parseInt(id),
      option,
      req.fileRecord,
      res,
    );
  }

  @Post('delete')
  @UseGuards(AuthGuard)
  async deleleFiles(
    @Body('', new FileDeleteValidationPipe()) { ids }: { ids: string[] },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: { message: string };
  }> {
    const data = await this.fileService.deleteFile(req.user.userId, ids);
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Post('status')
  @UseGuards(AuthGuard)
  async checkFileStatus(
    @Body('', new CheckFileStatusValidationPipe()) { ids }: { ids: string[] },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: {
      files: FileDto[];
      remainCredits: number;
    };
  }> {
    const data = await this.fileService.checkFileStatus(req.user.userId, ids);
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Get('chart-data/:breakdown/:label')
  @UseGuards(AuthGuard)
  async getChartData(
    @Param('', new ChartDataInputValidationPipe())
    { breakdown, label }: { breakdown: string; label: DataRangeFilter },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data:
      | {
          values: any[];
          labels: string[];
          breakdown: string;
        }
      | {
          message: string;
        };
  }> {
    const data = await this.fileService.getChartData(
      req.user.userId,
      breakdown,
      label,
    );
    return {
      status: HttpStatus.success,
      data,
    };
  }
}
