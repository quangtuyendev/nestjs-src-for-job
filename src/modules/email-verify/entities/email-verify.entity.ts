import { BaseEntity } from 'src/common/base.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity({ name: 'email_verifies' })
export class EmailVerifyEntity extends BaseEntity {
  @ManyToOne(() => UserEntity, (user) => user.emailVerifies)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column()
  email: string;

  @Column({
    length: 1000,
  })
  token: string;

  @Column()
  status: string;
}
