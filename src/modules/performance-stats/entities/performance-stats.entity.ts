import { BaseEntity } from 'src/common/base.entity';

import { Column, Entity, OneToOne } from 'typeorm';
import { UserEntity } from '../../user/entities/user.entity';

@Entity({ name: 'users_performance_stats' })
export class UserPerformanceStatsEntity extends BaseEntity {
  @Column()
  total: number;

  @Column()
  valid: number;

  @Column()
  invalid: number;

  @Column()
  unknown: number;

  @Column({ name: 'syntax_error' })
  syntaxError: number;

  @OneToOne(() => UserEntity, (user) => user.performanceStats)
  user: UserEntity;
}
