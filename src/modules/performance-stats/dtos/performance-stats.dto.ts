import { Expose } from 'class-transformer';

export class UserPerformanceStatsDto {
  @Expose()
  id: number;

  @Expose()
  total: number;

  @Expose()
  valid: number;

  @Expose()
  invalid: number;

  @Expose()
  unknown: number;

  @Expose()
  syntaxError: number;
}
