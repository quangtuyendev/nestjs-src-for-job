import {
  BadRequestException,
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  Post,
  Put,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { HttpStatus } from 'src/common/http-status.utils';
import { Constants } from 'src/constants/Constants';
import { AuthGuard } from 'src/guards/auth.guard';
import { PaypalService } from 'src/providers/paypal/paypal.service';
import { CancelSubscriptionValidationPipe } from 'src/providers/paypal/validators/cancel-subscription.validation';
import { CaptureSubscriptionValidationPipe } from 'src/providers/paypal/validators/capture-subscription.validation';
import { CreateOrderValidationPipe } from 'src/providers/paypal/validators/create-order.validation';
import { CreateSubscriptionValidationPipe } from 'src/providers/paypal/validators/create-subscription.validation';
import { AuthUtils } from 'src/utils/auth.utils';
import { CaptureOrderValidationPipe } from '../../providers/paypal/validators/capture-order.validation';
import { IUserDecoded } from '../auth/interfaces/user-decoded.interface';
import { TransactionDto } from '../transaction/dtos/transaction.dto';
import { TransactionService } from '../transaction/transaction.service';
import { GetTransactionsValidationPipe } from '../transaction/validators/get-transactions.validation';
import { UserDto } from './dtos/user.dto';
import { IUpdateUserInfos } from './interfaces/update-user.interface';
import { UserService } from './user.service';
import { ChangePasswordValidationPipe } from './validators/change-password.validation';
import { CodeResetPasswordValidationPipe } from './validators/code-reset-password.validation';
import { UpdateUserValidationPipe } from './validators/update-user.validation';

@Controller('users')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly paypalService: PaypalService,
    private readonly transactionService: TransactionService,
  ) {}

  @Get('me')
  @UseGuards(AuthGuard)
  async getUser(@Req() req: Request & { user: IUserDecoded }): Promise<{
    status: HttpStatus;
    data: {
      userInfos: UserDto;
    };
  }> {
    const data = await this.userService.getUser(req.user.userId);
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Put('me')
  @UseGuards(AuthGuard)
  async updateUser(
    @Body('', new UpdateUserValidationPipe())
    updateInfos: IUpdateUserInfos,
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: {
      message: string;
    };
  }> {
    const data = await this.userService.updateUser(
      req.user.userId,
      updateInfos,
    );
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Post('change-password')
  @UseGuards(AuthGuard)
  async changePassword(
    @Body('', new ChangePasswordValidationPipe())
    {
      currentPassword,
      newPassword,
    }: {
      currentPassword: string;
      newPassword: string;
    },
    @Req() req: Request & { user: IUserDecoded },
    @Res() res: Response,
  ): Promise<
    Response<{
      status: HttpStatus;
      data: {
        message: string;
      };
    }>
  > {
    const { token, refreshToken, message } =
      await this.userService.changePassword(
        req.user.email,
        currentPassword,
        newPassword,
      );
    AuthUtils.cookiesSigner(res, token, refreshToken);
    return res.status(200).json({
      status: HttpStatus.success,
      data: {
        message,
      },
    });
  }

  @Post('send-code-reset-password')
  async sendCodeResetPassword(
    @Body('', new CodeResetPasswordValidationPipe())
    { email }: { email: string },
  ): Promise<{ status: HttpStatus; data: { message: string } }> {
    const data = await this.userService.sendCodeResetPassword(email);
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Post('create-order')
  @UseGuards(AuthGuard)
  async createOrder(
    @Body('', new CreateOrderValidationPipe()) { amount }: { amount: number },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: {
      order: { id: string };
    };
  }> {
    try {
      const order = await this.paypalService.createOrder({
        amount: {
          currency_code: 'USD',
          value: amount.toString(),
        },
        custom_id: req.user.userId.toString(),
      });
      if (
        order.status !== Constants.PAYPAL_PURCHASE_STATUSES.createOrderSuccess
      ) {
        throw new BadRequestException(order.status.toLowerCase());
      }
      return {
        status: HttpStatus.success,
        data: { order: { id: order.id } },
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('create-subscription')
  @UseGuards(AuthGuard)
  async createSubscription(
    @Body('', new CreateSubscriptionValidationPipe())
    { subscriptionPlanId }: { subscriptionPlanId: string },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: {
      subscription: { id: string };
    };
  }> {
    try {
      const subscription = await this.paypalService.createSubscription({
        plan_id: subscriptionPlanId,
        custom_id: req.user.userId.toString(),
        subscriber: {
          email_address: req.user.email,
        },
      });
      if (
        subscription.status !==
        Constants.PAYPAL_PURCHASE_STATUSES.createSubscriptionSuccess
      ) {
        throw new BadRequestException(subscription.status.toLowerCase());
      }
      return {
        status: HttpStatus.success,
        data: { subscription: { id: subscription.id } },
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('capture-order')
  @UseGuards(AuthGuard)
  async captureOrder(
    @Body('', new CaptureOrderValidationPipe())
    { orderID }: { orderID: string },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
  }> {
    try {
      await this.userService.captureOrder(orderID, req.user.userId);
      return {
        status: HttpStatus.success,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('capture-subscription')
  @UseGuards(AuthGuard)
  async captureSubscription(
    @Body('', new CaptureSubscriptionValidationPipe())
    { subscriptionId }: { subscriptionId: string },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
  }> {
    try {
      const status = await this.userService.captureSubscription(
        subscriptionId,
        req.user.userId,
      );
      return status;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('cancel-subscription')
  @UseGuards(AuthGuard)
  async cancelSubscription(
    @Body('', new CancelSubscriptionValidationPipe())
    { reason }: { reason: string },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: { message: string };
  }> {
    const res = await this.userService.cancelSubscription(
      req.user.userId,
      reason,
    );
    return res;
  }

  @Get('transactions')
  @UseGuards(AuthGuard)
  async getTransactions(
    @Query('', new GetTransactionsValidationPipe())
    { page, limit }: { page: string; limit: string },
    @Req() req: Request & { user: IUserDecoded },
  ): Promise<{
    status: HttpStatus;
    data: {
      items: TransactionDto[];
      totalCount: number;
    };
  }> {
    const data = await this.transactionService.getTransactionByUser(
      req.user.userId,
      parseInt(page),
      parseInt(limit),
    );
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Get('credits')
  @UseGuards(AuthGuard)
  async getCredits(@Req() req: Request & { user: IUserDecoded }): Promise<{
    status: HttpStatus;
    data: {
      credits: number;
    };
  }> {
    const data = await this.userService.getCredits(req.user.userId);
    return {
      status: HttpStatus.success,
      data,
    };
  }
}
