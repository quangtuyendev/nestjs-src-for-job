export interface IUserCreditStatus {
  status: boolean;
  requiredAmount: number;
}
