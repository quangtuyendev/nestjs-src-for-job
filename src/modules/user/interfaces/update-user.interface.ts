export interface IUpdateUserInfos {
  fullName: string;
  timezone: string;
  isShowGuide: boolean;
}
