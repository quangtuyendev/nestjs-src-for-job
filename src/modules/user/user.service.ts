import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { forwardRef } from '@nestjs/common/utils';
import { ConfigService } from '@nestjs/config';
import { plainToInstance } from 'class-transformer';
import { find } from 'lodash';
import qs from 'querystring';
import { HttpStatus } from 'src/common/http-status.utils';
import { LogError } from 'src/common/log-error.decorator';
import { Constants } from 'src/constants/Constants';
import { InvalidTokenException } from 'src/exceptions/invalid-token.exception';
import { UserNotFoundException } from 'src/exceptions/user-not-found.exception';
import { FirebaseService } from 'src/providers/firebase/firebase.service';
import { KafkaService } from 'src/providers/kafka/kafka.service';
import { EmailSendStatus } from 'src/providers/mailer/enums/email-sending-status.enum';
import { MailerService } from 'src/providers/mailer/mailer.service';
import { PaypalPaymentMethod } from 'src/providers/paypal/enums/paypal-method.enum';
import { ICaptureSubscription } from 'src/providers/paypal/interfaces/capture-subscription.interface';
import { PaypalService } from 'src/providers/paypal/paypal.service';
import { Helpers } from 'src/utils/helpers.utils';
import { Repository } from 'typeorm';
import Logger from '../../utils/logger.utils';
import { AuthService } from '../auth/auth.service';
import { IAuthResponse } from '../auth/interfaces/auth-response.interface';
import { UserPerformanceStatsDto } from '../performance-stats/dtos/performance-stats.dto';
import { ResetPasswordEntity } from '../reset-password/entities/reset-password.entity';
import { UserSubScriptionEntity } from '../subscription/entities/subscription.entity';
import { TransactionEntity } from '../transaction/entities/transaction.entity';
import { UserDto } from './dtos/user.dto';
import { UserEntity } from './entities/user.entity';
import { IUpdateUserInfos } from './interfaces/update-user.interface';

@Injectable()
export class UserService {
  private logger: Logger = new Logger(__filename);

  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<UserEntity>,
    @Inject('TRANSACTION_REPOSITORY')
    private readonly transactionRepository: Repository<TransactionEntity>,
    @Inject('SUBSCRIPTION_REPOSITORY')
    private readonly subscriptionRepository: Repository<UserSubScriptionEntity>,
    @Inject('RESET_PASSWORD_REPOSITORY')
    private readonly resetPasswordRepository: Repository<ResetPasswordEntity>,
    private readonly configService: ConfigService,
    @Inject(forwardRef(() => AuthService))
    private authService: AuthService,
    private readonly paypalService: PaypalService,
    private readonly firebaseService: FirebaseService,
    private readonly httpService: HttpService,
    @Inject(forwardRef(() => KafkaService))
    private readonly kafkaService: KafkaService,
    @Inject(forwardRef(() => MailerService))
    private readonly mailerService: MailerService,
  ) {}

  @LogError(__filename)
  async getUser(userId: number): Promise<{
    userInfos: UserDto;
  }> {
    try {
      const FUNC_NAME = 'getUser';
      this.logger.info(`GET: ${FUNC_NAME}`);
      if (!userId) throw new InvalidTokenException();
      const user = await this.userRepository.findOne({
        where: { id: userId },
        relations: {
          performanceStats: true,
        },
      });
      const convertedUserPerformance = plainToInstance(
        UserPerformanceStatsDto,
        user.performanceStats,
        {
          excludeExtraneousValues: true,
        },
      );
      if (!user) throw new UserNotFoundException();
      const convertedUser = plainToInstance(UserDto, user, {
        excludeExtraneousValues: true,
      });
      return {
        userInfos: {
          ...convertedUser,
          performanceStats: convertedUserPerformance,
        },
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async updateUser(
    userId: number,
    updateInfos: IUpdateUserInfos,
  ): Promise<any> {
    const FUNC_NAME = 'updateUser';
    try {
      this.logger.info(`PATCH: ${FUNC_NAME}: ${JSON.stringify(updateInfos)}`);
      const { fullName, timezone, isShowGuide } = updateInfos;
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      if (!user) throw new UserNotFoundException();
      const convertedInfos = Helpers.removeUndefinedProperties({
        fullName,
        timezone,
        settings:
          typeof isShowGuide === 'boolean' ? { isShowGuide } : undefined,
      });
      for (const key in convertedInfos) {
        user[key] = convertedInfos[key];
      }
      await this.userRepository.update(userId, user);
      return {
        message: Constants.MESSAGES.success.auth.update_info_success,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async changePassword(
    email: string,
    currentPassword: string,
    newPassword: string,
  ): Promise<IAuthResponse['data']> {
    const FUNC_NAME = 'changePassword';
    try {
      this.logger.info(FUNC_NAME);
      const userRecord = await this.authService.signInWithPassword(
        email,
        currentPassword,
      );
      const updatedInfos = await this.authService.updateUserPassword(
        userRecord.token,
        newPassword,
      );
      const user = await this.userRepository.findOne({
        where: { uid: updatedInfos.localId },
      });
      if (!user) throw new UserNotFoundException();
      const { token, refreshToken } =
        await this.authService.getDecodedTokenClaims(
          updatedInfos.localId,
          user.id,
        );
      return {
        message: Constants.MESSAGES.success.auth.update_password_success,
        token,
        refreshToken,
      };
    } catch (error) {
      if (error.response?.data?.error?.code) {
        throw new BadRequestException(
          error.response?.data?.error?.message
            .split(':')
            .shift()
            .trim()
            .toLowerCase(),
        );
      }
      throw new InternalServerErrorException(error);
    }
  }

  async sendCodeResetPassword(email: string): Promise<{ message: string }> {
    try {
      const FUNC_NAME = '/send-code-reset-password';
      this.logger.info(FUNC_NAME);
      const actionCodeSettings = {
        url: `${Constants.APP_BASE_URI}/reset-password`,
        handleCodeInApp: true,
      };
      const link = await this.firebaseService
        .getAuth()
        .generatePasswordResetLink(email, actionCodeSettings);
      const { oobCode } = qs.parse(link) as { oobCode: string };
      try {
        await this.mailerService.emailResetPasswordSender(
          email,
          oobCode as string,
        );
      } catch (error) {
        const user = await this.userRepository.findOne({ where: { email } });
        const emailRecord = await this.resetPasswordRepository.save({
          user,
          email,
          code: oobCode,
          status: EmailSendStatus.pending,
        });
        try {
          await this.kafkaService.sendMessage({
            topic: Constants.KAFKA_CONFIG.topics.emailResetPassword.name,
            messages: [
              {
                value: JSON.stringify({
                  id: emailRecord.id,
                  email,
                  code: oobCode,
                  status: EmailSendStatus.pending,
                }),
              },
            ],
          });
        } catch (error) {
          this.logger.error(`${FUNC_NAME}: ${error}`);
          await this.resetPasswordRepository.update(emailRecord.id, {
            status: EmailSendStatus.failed,
          });
        }
      }
      return {
        message: Constants.MESSAGES.success.auth.send_reset_code_success,
      };
    } catch (error) {
      if (error.code) {
        throw new BadRequestException(error.code);
      }
      throw new InternalServerErrorException(error);
    }
  }

  async captureOrder(orderId: string, userIdReq: number): Promise<void> {
    const FUNC_NAME = 'captureOrder';
    this.logger.info(`POST: ${FUNC_NAME}`);
    try {
      const captureData = await this.paypalService.capturePayment(orderId);
      this.logger.info(`${FUNC_NAME}: ${JSON.stringify(captureData)}`);
      if (
        captureData.status !==
        Constants.PAYPAL_PURCHASE_STATUSES.captureOrderSuccess
      ) {
        throw new BadRequestException(captureData.status.toLowerCase());
      }
      const {
        id,
        status,
        payment_source: {
          paypal: {
            name: { given_name, surname },
            address: { country_code },
            email_address,
            account_id,
          },
        },
        purchase_units,
      } = captureData;
      const {
        id: purchaseId,
        amount: { currency_code, value },
        custom_id: userId,
      } = purchase_units[0].payments.captures[0];
      const existingTransaction = await this.transactionRepository.findOne({
        where: { orderId: id },
      });
      if (existingTransaction) {
        this.logger.debug(`${FUNC_NAME}: Order Id: "${id}" already exist`);
        throw new BadRequestException(
          Constants.MESSAGES.failed.checkout.transaction_already_exist,
        );
      }
      if (userId !== userIdReq.toString()) {
        throw new ForbiddenException(
          Constants.MESSAGES.failed.common.forbidden,
        );
      }
      const credits = parseInt(value) * Constants.CREDITS_PER_USD;
      const user = await this.userRepository.findOne({
        where: { id: parseInt(userId) },
      });
      await this.transactionRepository.save({
        user,
        orderId: id,
        status,
        purchaseId,
        payerId: account_id,
        country: country_code,
        currency: currency_code,
        customerName: `${given_name} ${surname}`,
        customerEmail: email_address,
        paymentMethod: PaypalPaymentMethod.paypal,
        amount: value,
        credits,
      });
      await this.userRepository.increment(
        { id: parseInt(userId) },
        'credits',
        credits,
      );
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async captureSubscription(
    subscriptionId: string,
    userIdReq: number,
  ): Promise<{
    status: HttpStatus;
  }> {
    try {
      const FUNC_NAME = '/capture-subscription';
      this.logger.info(`POST: ${FUNC_NAME}`);
      const captureData = await this.paypalService.captureSubscription(
        subscriptionId,
      );
      this.logger.info(`${FUNC_NAME}: ${JSON.stringify(captureData)}`);
      if (!captureData.status) {
        throw new BadRequestException(captureData.name.toLowerCase());
      }
      if (
        captureData.status !==
        Constants.PAYPAL_PURCHASE_STATUSES.captureSubscriptionSuccess
      ) {
        return {
          status: HttpStatus.failed,
        };
      }
      const { id, custom_id: userId } = captureData;
      const existingTransaction = await this.transactionRepository.findOne({
        where: { orderId: id },
      });
      if (existingTransaction) {
        this.logger.debug(`${FUNC_NAME}: Order Id: "${id}" already exist`);
        throw new BadRequestException(
          Constants.MESSAGES.failed.checkout.transaction_already_exist,
        );
      }
      if (userId !== userIdReq.toString()) {
        throw new ForbiddenException(
          Constants.MESSAGES.failed.common.forbidden,
        );
      }
      await this.createTransaction(captureData);
      return {
        status: HttpStatus.success,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async cancelSubscription(
    userId: number,
    reason: string,
  ): Promise<{
    status: HttpStatus;
    data: { message: string };
  }> {
    try {
      const FUNC_NAME = 'cancelSubscription';
      this.logger.info(`${FUNC_NAME}: User Id: "${userId}"`);
      const user = await this.userRepository.findOne({
        where: {
          id: userId,
        },
        relations: {
          subscriptions: true,
        },
      });

      if (!user) throw new UserNotFoundException();
      const subscription = find(user.subscriptions, (s) => !s.isCancel);

      if (subscription?.subscriptionId) {
        const cancellationReason = Helpers.getRandomCancellationReason();
        const { status } = await this.paypalService.cancelUserSubscription(
          subscription.subscriptionId,
          cancellationReason,
        );
        if (status) {
          await this.subscriptionRepository.update(subscription.id, {
            reason,
            isCancel: true,
          });
        }
        return {
          status: status ? HttpStatus.success : HttpStatus.failed,
          data: {
            message: status
              ? Constants.MESSAGES.success.user.subscription_downgrade_success
              : Constants.MESSAGES.failed.user.subscription_cancel_failed
                  .message,
          },
        };
      }
      return {
        status: HttpStatus.success,
        data: {
          message: Constants.MESSAGES.success.user.no_subscription_subscribed,
        },
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getCredits(userId: number): Promise<{ credits: number }> {
    try {
      const FUNC_NAME = 'getCredits';
      this.logger.info(FUNC_NAME);
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      if (!user) throw new UserNotFoundException();
      return {
        credits: user.credits,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  public async isUserExist(email: string): Promise<boolean> {
    const user = await this.userRepository.findOne({
      where: {
        email,
      },
    });
    return !!user;
  }

  async createSubscription(subscriptionInfos) {
    const FUNC_NAME = 'createSubscription';
    this.logger.info(`${FUNC_NAME}: ${JSON.stringify(subscriptionInfos)}`);
    try {
      const accessToken = await this.paypalService.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v1/billing/subscriptions`;
      const { data } = await this.httpService.axiosRef.post(
        url,
        {
          ...subscriptionInfos,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
            Prefer: 'return=representation',
          },
        },
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async capturePayment(orderId) {
    const FUNC_NAME = 'capturePayment';
    this.logger.info(`${FUNC_NAME}: ${orderId}`);
    try {
      const accessToken = await this.paypalService.generateAccessToken();
      const url = `${
        this.configService.get('paypal').baseUri
      }/v2/checkout/orders/${orderId}/capture`;
      const { data } = await this.httpService.axiosRef.post(
        url,
        {},
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
            Prefer: 'return=representation',
          },
        },
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async createTransaction(captureData: ICaptureSubscription) {
    const FUNC_NAME = 'createTransaction';
    this.logger.info(FUNC_NAME);
    try {
      const {
        id,
        plan_id,
        status,
        subscriber: {
          name: { given_name, surname },
          payer_id,
          email_address,
          shipping_address: {
            address: { country_code },
          },
        },
        billing_info: {
          last_payment: {
            amount: { currency_code, value },
          },
          next_billing_time,
        },
        custom_id: userId,
      } = captureData;
      const subscription = find(Constants.PRICING.monthly, (value) => {
        return (
          typeof value === 'object' &&
          value.hasOwnProperty('subscriptionPlanId') &&
          value['subscriptionPlanId'] === plan_id
        );
      });
      const credits = parseInt(subscription.price) * Constants.CREDITS_PER_USD;

      const user = await this.userRepository.findOne({
        where: { id: parseInt(userId) },
      });

      await this.transactionRepository.save({
        user,
        orderId: id,
        status,
        purchaseId: payer_id,
        country: country_code,
        currency: currency_code,
        customerName: `${given_name} ${surname}`,
        customerEmail: email_address,
        paymentMethod: PaypalPaymentMethod.paypal,
        amount: value,
        credits,
        payerId: payer_id,
        nextBillDate: next_billing_time,
        nextPaymentAmount: value,
        planName: subscription.name,
        subscriptionId: id,
        subscriptionPlanId: plan_id,
      });

      const updatedUser = await this.userRepository.increment(
        {
          id: parseInt(userId),
        },
        'credits',
        credits,
      );

      const userSubscription = await this.subscriptionRepository.findOne({
        where: {
          user,
        },
      });

      if (userSubscription?.subscriptionId) {
        const currentPlanPrice = Helpers.getNamePrice(subscription.name);
        const previousPlanPrice = Helpers.getNamePrice(
          userSubscription.planName,
        );
        const { status } = await this.paypalService.cancelUserSubscription(
          userSubscription.subscriptionId,
          `Subscriber ${
            currentPlanPrice > previousPlanPrice ? 'Upgraded' : 'Downgraded'
          } Plan`,
        );
        if (status) {
          this.logger.info(
            `${FUNC_NAME}: Subscriber ${
              currentPlanPrice > previousPlanPrice ? 'Upgraded' : 'Downgraded'
            } Plan Successfully`,
          );
        }
      }
    } catch (error) {
      throw error;
    }
  }
}
