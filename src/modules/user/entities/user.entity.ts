import { BaseEntity } from 'src/common/base.entity';

import { Entity, Column, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { UserSubScriptionEntity } from '../../subscription/entities/subscription.entity';
import { UserSettingEntity } from '../../setting/entities/setting.entity';
import { UserPerformanceStatsEntity } from '../../performance-stats/entities/performance-stats.entity';
import { EmailVerifyEntity } from 'src/modules/email-verify/entities/email-verify.entity';
import { ResetPasswordEntity } from 'src/modules/reset-password/entities/reset-password.entity';
import { TransactionEntity } from 'src/modules/transaction/entities/transaction.entity';

@Entity({ name: 'users' })
export class UserEntity extends BaseEntity {
  @Column({ nullable: true })
  uid?: string;

  @Column({ name: 'fc_restore_id', nullable: true })
  fcRestoreId?: string;

  @Column({ name: 'full_name' })
  fullName: string;

  @Column({ name: 'picture', nullable: true })
  picture?: string;

  @Column({ name: 'signin_provider' })
  signinProvider: string;

  @Column({ name: 'email' })
  email: string;

  @Column({ name: 'is_verified' })
  isVerified: boolean;

  @Column({ name: 'organization_name', nullable: true })
  organizationName?: string;

  @Column({ name: 'industry', nullable: true })
  industry?: string;

  @Column({ name: 'job_title', nullable: true })
  jobTitle?: string;

  @Column({ name: 'referring_resource', nullable: true })
  referringResource?: string;

  @Column({ name: 'timezone', nullable: true })
  timezone?: string;

  @Column({ name: 'profile_completed' })
  profileCompleted: boolean;

  @Column({ name: 'credits' })
  credits: number;

  @OneToMany(
    () => UserSubScriptionEntity,
    (userSubScription) => userSubScription.user,
  )
  subscriptions: UserSubScriptionEntity[];

  @OneToMany(() => TransactionEntity, (transactions) => transactions.user)
  transactions: TransactionEntity[];

  @OneToOne(() => UserSettingEntity, (userSetting) => userSetting.user)
  @JoinColumn({ name: 'setting_id' })
  setting: UserSettingEntity;

  @OneToOne(
    () => UserPerformanceStatsEntity,
    (userPerformance) => userPerformance.user,
  )
  @JoinColumn({ name: 'performance_id' })
  performanceStats: UserPerformanceStatsEntity;

  @OneToMany(() => EmailVerifyEntity, (emailVerifies) => emailVerifies.user)
  emailVerifies: EmailVerifyEntity[];

  @OneToMany(() => ResetPasswordEntity, (resetsPassword) => resetsPassword.user)
  resetsPasswords: ResetPasswordEntity[];
}
