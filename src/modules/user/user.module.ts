import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
  forwardRef,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { DataSource } from 'typeorm';
import { UserEntity } from './entities/user.entity';
import { AuthModule } from '../auth/auth.module';
import { UserSubScriptionEntity } from '../subscription/entities/subscription.entity';
import { DatabaseModule } from 'src/providers/typeorm/database.module';
import { TransactionEntity } from '../transaction/entities/transaction.entity';
import { PaypalModule } from 'src/providers/paypal/paypal.module';
import { HttpModule } from '@nestjs/axios';
import { RateLimiter } from 'src/utils/rate-limiter';
import { Constants } from 'src/constants/Constants';
import { FirebaseModule } from 'src/providers/firebase/firebase.module';
import { KafkaModule } from 'src/providers/kafka/kafka.module';
import { TransactionModule } from '../transaction/transaction.module';
import { MailerModule } from 'src/providers/mailer/mailer.module';
import { ResetPasswordEntity } from '../reset-password/entities/reset-password.entity';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    forwardRef(() => AuthModule),
    forwardRef(() => PaypalModule),
    KafkaModule.register(),
    MailerModule,
    DatabaseModule,
    FirebaseModule,
    TransactionModule,
  ],
  controllers: [UserController],
  providers: [
    UserService,
    {
      provide: 'USER_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(UserEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'SUBSCRIPTION_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(UserSubScriptionEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'TRANSACTION_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(TransactionEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'RESET_PASSWORD_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(ResetPasswordEntity),
      inject: ['DATA_SOURCE'],
    },
  ],
  exports: ['USER_REPOSITORY', 'SUBSCRIPTION_REPOSITORY', UserService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        RateLimiter.apply(
          100,
          Constants.MESSAGES.failed.common.too_many_request,
        ),
      )
      .forRoutes({ path: 'users/me', method: RequestMethod.POST })
      .apply(
        RateLimiter.apply(
          10,
          Constants.MESSAGES.failed.common.too_many_request,
        ),
      )
      .forRoutes({ path: 'users/me', method: RequestMethod.PUT })
      .apply(
        RateLimiter.apply(
          10,
          Constants.MESSAGES.failed.common.too_many_request,
        ),
      )
      .forRoutes({ path: 'users/change-password', method: RequestMethod.POST })
      .apply(
        RateLimiter.apply(5, Constants.MESSAGES.failed.common.too_many_request),
      )
      .forRoutes({
        path: 'users/send-code-reset-password',
        method: RequestMethod.POST,
      })
      .apply(
        RateLimiter.apply(5, Constants.MESSAGES.failed.common.too_many_request),
      )
      .forRoutes({
        path: 'users/cancel-subscription',
        method: RequestMethod.POST,
      })
      .apply(
        RateLimiter.apply(
          100,
          Constants.MESSAGES.failed.common.too_many_request,
        ),
      )
      .forRoutes({ path: 'users/transactions', method: RequestMethod.GET })
      .apply(
        RateLimiter.apply(
          100,
          Constants.MESSAGES.failed.common.too_many_request,
        ),
      )
      .forRoutes({ path: 'users/credits', method: RequestMethod.GET });
  }
}
