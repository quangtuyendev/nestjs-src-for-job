import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  currentPassword: Joi.string()
    .min(8)
    .max(30)
    .pattern(
      new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])'),
    )
    .required()
    .messages({
      'any.required':
        Constants.MESSAGES.failed.auth.current_password_required.message,
      'string.empty':
        Constants.MESSAGES.failed.auth.current_password_empty.message,
      'string.min':
        Constants.MESSAGES.failed.auth.current_password_min_length_8.message,
      'string.max':
        Constants.MESSAGES.failed.auth.current_password_max_length_30.message,
      'string.pattern.base':
        Constants.MESSAGES.failed.auth
          .current_password_complexity_requirement_not_met.message,
    }),
  newPassword: Joi.string()
    .min(8)
    .max(30)
    .pattern(
      new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])'),
    )
    .required()
    .messages({
      'any.required':
        Constants.MESSAGES.failed.auth.new_password_required.message,
      'string.empty': Constants.MESSAGES.failed.auth.new_password_empty.message,
      'string.min':
        Constants.MESSAGES.failed.auth.new_password_min_length_8.message,
      'string.max':
        Constants.MESSAGES.failed.auth.new_password_max_length_30.message,
      'string.pattern.base':
        Constants.MESSAGES.failed.auth
          .new_password_complexity_requirement_not_met.message,
    }),
})
  .custom((values, helpers) => {
    if (values.newPassword === values.currentPassword) {
      return helpers.error('any.invalid');
    }
    return values;
  })
  .messages({
    'any.invalid':
      Constants.MESSAGES.failed.auth.new_password_cannot_match_current_password
        .message,
  });
export class ChangePasswordValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
