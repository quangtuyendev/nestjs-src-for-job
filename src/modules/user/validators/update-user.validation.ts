import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { map } from 'lodash';
import { Constants } from 'src/constants/Constants';
import { TIMEZONES } from 'src/constants/timezone';

const Schema = Joi.object({
  fullName: Joi.string()
    .min(4)
    .max(256)
    .trim()
    .pattern(/^[a-zA-Z ]+$/)
    .optional()
    .messages({
      'string.empty': Constants.MESSAGES.failed.auth.fullname_empty.message,
      'string.min':
        Constants.MESSAGES.failed.auth.fullname_min_length_4.message,
      'string.max':
        Constants.MESSAGES.failed.auth.fullname_max_length_256.message,
      'string.pattern.base':
        Constants.MESSAGES.failed.auth.fullname_invalid_characters.message,
    }),
  timezone: Joi.string()
    .valid(...map(TIMEZONES, (t) => t.value))
    .optional()
    .messages({
      'string.empty': Constants.MESSAGES.failed.auth.timezone_empty.message,
      'any.only': Constants.MESSAGES.failed.auth.timezone_only.message,
    }),
  isShowGuide: Joi.boolean().optional().messages({
    'boolean.base':
      Constants.MESSAGES.failed.auth.invalid_is_show_guide.message,
  }),
});
export class UpdateUserValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
