import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .trim()
    .min(4)
    .max(255)
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.auth.email_required.message,
      'string.empty': Constants.MESSAGES.failed.auth.email_empty.message,
      'string.email': Constants.MESSAGES.failed.auth.email_invalid.message,
      'string.min': Constants.MESSAGES.failed.auth.email_min_length_4.message,
      'string.max': Constants.MESSAGES.failed.auth.email_max_length_255.message,
    }),
});

export class CodeResetPasswordValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
