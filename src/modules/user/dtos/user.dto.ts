import { Expose } from 'class-transformer';
import { UserPerformanceStatsDto } from 'src/modules/performance-stats/dtos/performance-stats.dto';
import { UserSettingDto } from 'src/modules/setting/dtos/setting.dto';
import { UserSubScriptionDto } from 'src/modules/subscription/dtos/subscription.dto';

export class UserDto {
  @Expose()
  id: number;

  @Expose()
  uid: string;

  @Expose()
  fcRestoreId: string;

  @Expose()
  fullName: string;

  @Expose()
  picture: string;

  @Expose()
  signinProvider: string;

  @Expose()
  email: string;

  @Expose()
  isVerified: boolean;

  @Expose()
  organizationName: string;

  @Expose()
  industry: string;

  @Expose()
  jobTitle: string;

  @Expose()
  referringResource: string;

  @Expose()
  timezone: string;

  @Expose()
  profileCompleted: boolean;

  @Expose()
  credits: number;

  @Expose()
  subscriptions: UserSubScriptionDto[];

  @Expose()
  setting: UserSettingDto;

  @Expose()
  performanceStats: UserPerformanceStatsDto;
}
