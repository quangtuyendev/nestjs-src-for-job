import { Expose } from 'class-transformer';
import { UserEntity } from '../../user/entities/user.entity';

export class UserSettingDto {
  @Expose()
  id: number;

  @Expose()
  usShowGuide: boolean;

  @Expose()
  user: UserEntity['id'];
}
