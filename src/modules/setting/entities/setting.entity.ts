import { BaseEntity } from 'src/common/base.entity';

import { Column, Entity, OneToOne } from 'typeorm';
import { UserEntity } from '../../user/entities/user.entity';

@Entity({ name: 'users_settings' })
export class UserSettingEntity extends BaseEntity {
  @Column({ name: 'is_show_guide' })
  isShowGuide: boolean;

  @OneToOne(() => UserEntity, (user) => user.setting)
  user: UserEntity;
}
