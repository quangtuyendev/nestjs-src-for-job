import { Expose } from 'class-transformer';
import { UserEntity } from 'src/modules/user/entities/user.entity';

export class UserSubScriptionDto {
  @Expose()
  id: number;

  @Expose()
  planName: string;

  @Expose()
  subscriberId: string;

  @Expose()
  subscriptionId: string;

  @Expose()
  subscriptionPlanId: string;

  @Expose()
  reason?: string;

  @Expose()
  user: UserEntity['id'];
}
