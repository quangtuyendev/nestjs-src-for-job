import { BaseEntity } from 'src/common/base.entity';

import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { UserEntity } from '../../user/entities/user.entity';

@Entity({ name: 'user_subscriptions' })
export class UserSubScriptionEntity extends BaseEntity {
  @Column({ name: 'plan_name' })
  planName: string;

  @Column({ name: 'subscriber_id', nullable: true })
  subscriberId?: string;

  @Column({ name: 'subscription_id', nullable: true })
  subscriptionId?: string;

  @Column({ name: 'subscription_plan_id', nullable: true })
  subscriptionPlanId?: string;

  @Column({ name: 'reason', nullable: true })
  reason?: string;

  @Column({ name: 'is_cancel', nullable: true })
  isCancel?: boolean;

  @ManyToOne(() => UserEntity, (user) => user.subscriptions)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
