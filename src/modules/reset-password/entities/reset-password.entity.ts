import { BaseEntity } from 'src/common/base.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

@Entity({ name: 'reset_passwords' })
export class ResetPasswordEntity extends BaseEntity {
  @ManyToOne(() => UserEntity, (user) => user.resetsPasswords)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @Column()
  email: string;

  @Column()
  status: string;

  @Column()
  code: string;
}
