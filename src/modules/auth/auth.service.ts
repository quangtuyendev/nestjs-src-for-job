import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Body,
  Inject,
  Injectable,
  InternalServerErrorException,
  forwardRef,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Constants } from 'src/constants/Constants';
import { InvalidTokenException } from 'src/exceptions/invalid-token.exception';
import { UserNotFoundException } from 'src/exceptions/user-not-found.exception';
import { FirebaseService } from 'src/providers/firebase/firebase.service';
import { KafkaService } from 'src/providers/kafka/kafka.service';
import { EmailSendStatus } from 'src/providers/mailer/enums/email-sending-status.enum';
import { MailerService } from 'src/providers/mailer/mailer.service';
import { AuthUtils } from 'src/utils/auth.utils';
import { UtilsValidate } from 'src/utils/validate.utils';
import { MoreThanOrEqual, Repository } from 'typeorm';
import Logger from '../../utils/logger.utils';
import { EmailVerifyEntity } from '../email-verify/entities/email-verify.entity';
import { UserPerformanceStatsEntity } from '../performance-stats/entities/performance-stats.entity';
import { ResetPasswordEntity } from '../reset-password/entities/reset-password.entity';
import { UserSettingEntity } from '../setting/entities/setting.entity';
import { UserSubScriptionEntity } from '../subscription/entities/subscription.entity';
import { CreateUserDto } from '../user/dtos/create-user.dto';
import { UserEntity } from '../user/entities/user.entity';
import { UserService } from '../user/user.service';
import { SignInProvider } from './enums/index.enum';
import { IAuthResponse } from './interfaces/auth-response.interface';
import { IFirebaseSignInPassword } from './interfaces/firebase-signin-password.interface';
import { ISignUpCompleteRequest } from './interfaces/signup-complete-request.interface';

@Injectable()
export class AuthService {
  private logger: Logger = new Logger(__filename);
  constructor(
    @Inject(forwardRef(() => KafkaService))
    private readonly kafkaService: KafkaService,
    private readonly configService: ConfigService,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<UserEntity>,
    @Inject('SUBSCRIPTION_REPOSITORY')
    private readonly userSubScriptionRepository: Repository<UserSubScriptionEntity>,
    @Inject('EMAIL_VERIFY_REPOSITORY')
    private readonly emailVerifyRepository: Repository<EmailVerifyEntity>,
    @Inject('RESET_PASSWORD_REPOSITORY')
    private readonly resetPasswordRepository: Repository<ResetPasswordEntity>,
    @Inject('USER_SETTING_REPOSITORY')
    private readonly userSettingRepository: Repository<UserSettingEntity>,
    @Inject('USER_PERFORMANCE_STATS_REPOSITORY')
    private readonly userPerformanceStatsRepository: Repository<UserPerformanceStatsEntity>,
    private readonly httpService: HttpService,
    private readonly firebaseService: FirebaseService,
    @Inject(forwardRef(() => MailerService))
    private readonly mailerService: MailerService,
  ) {}

  public async register(
    @Body() userInfo: CreateUserDto,
  ): Promise<Omit<IAuthResponse['data'], 'refreshToken'>> {
    const FUNC_NAME = 'register';
    try {
      const { fullName, email, password } = userInfo;
      this.logger.info(`${FUNC_NAME}: ${JSON.stringify({ fullName, email })}`);
      const isUserExist = await this.userService.isUserExist(email);
      if (isUserExist) {
        throw new BadRequestException(
          Constants.MESSAGES.failed.auth.email_already_registered,
        );
      }
      const passwordHasSpaces = UtilsValidate.passwordContainsSpaces(password);
      if (passwordHasSpaces) {
        throw new BadRequestException(
          Constants.MESSAGES.failed.auth.password_contains_space,
        );
      }
      if (email.includes(password)) {
        throw new BadRequestException(
          Constants.MESSAGES.failed.auth.password_include_email,
        );
      }

      const user = await this.userRepository.save({
        fullName,
        email,
        isVerified: false,
        signinProvider: SignInProvider.email,
        credits: Constants.FREE_REGISTRATION_CREDITS,
        profileCompleted: false,
      });

      await this.userSubScriptionRepository.save({
        planName: Constants.PRICING.monthly.free.name,
        user: user,
      });

      const userRecord = await this.firebaseService.getAuth().createUser({
        email,
        password,
        displayName: fullName,
      });

      const customToken = await this.firebaseService
        .getAuth()
        .createCustomToken(userRecord.uid, {
          userId: user.id,
        });

      user.uid = userRecord.uid;
      await this.userRepository.save(user);

      try {
        const verifyUrl = AuthUtils.getVerifyUrl(customToken);
        await this.mailerService.emailConfirmSender(email, verifyUrl);
      } catch (error) {
        const emailRecord = await this.emailVerifyRepository.save({
          user: user,
          email,
          token: customToken,
          status: EmailSendStatus.pending,
        });

        try {
          await this.kafkaService.sendMessage({
            topic: Constants.KAFKA_CONFIG.topics.emailConfirm.name,
            messages: [
              {
                value: JSON.stringify({
                  id: emailRecord.id,
                  userId: user.id,
                  email,
                  token: customToken,
                  status: EmailSendStatus.pending,
                }),
              },
            ],
          });
        } catch (error) {
          this.logger.error(`${FUNC_NAME}: ${error}`);
          emailRecord.status = EmailSendStatus.failed;
          await this.emailVerifyRepository.save(emailRecord);
        }
      }
      return {
        token: customToken,
        message: Constants.MESSAGES.success.auth.registration_success,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      if (error.code) {
        throw new InternalServerErrorException(error.code);
      }
      throw new InternalServerErrorException(error);
    }
  }

  public async verify(customToken: string): Promise<IAuthResponse['data']> {
    const FUNC_NAME = 'verify';
    try {
      const { token, refreshToken } = await this.verifyCustomToken(customToken);
      const decodedToken = await this.firebaseService
        .getAuth()
        .verifyIdToken(token);
      const { userId } = decodedToken;
      if (!userId) throw new UserNotFoundException();
      await this.userRepository.update(userId, {
        isVerified: true,
      });
      return {
        message: Constants.MESSAGES.success.auth.verified,
        token,
        refreshToken,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      if (error.code) {
        throw new InternalServerErrorException(error.code);
      } else if (error.response?.data?.error?.code) {
        throw new InternalServerErrorException(
          error.response?.data?.error?.message
            .split(':')
            .shift()
            .trim()
            .toLowerCase(),
        );
      }
      throw new InternalServerErrorException(error);
    }
  }

  public async signIn(
    email: string,
    password: string,
  ): Promise<Partial<IAuthResponse['data']>> {
    const FUNC_NAME = 'signIn';
    try {
      const decodedToken = await this.signInWithPassword(email, password);
      const { refreshToken, localId: uid } = decodedToken;
      const user = await this.userRepository.findOne({
        where: { uid },
      });
      if (!user) throw new UserNotFoundException();
      const customToken = await this.firebaseService
        .getAuth()
        .createCustomToken(uid, {
          userId: user.id,
        });
      if (!user.isVerified) {
        try {
          const verifyUrl = AuthUtils.getVerifyUrl(customToken);
          await this.mailerService.emailConfirmSender(email, verifyUrl);
        } catch (error) {
          const emailRecord = await this.emailVerifyRepository.save({
            user: user,
            email,
            token: customToken,
            status: EmailSendStatus.pending,
          });

          try {
            await this.kafkaService.sendMessage({
              topic: Constants.KAFKA_CONFIG.topics.emailConfirm.name,
              messages: [
                {
                  value: JSON.stringify({
                    id: emailRecord.id,
                    userId: user.id,
                    email,
                    token: customToken,
                    status: EmailSendStatus.pending,
                  }),
                },
              ],
            });
          } catch (error) {
            this.logger.error(`${FUNC_NAME}: ${error}`);
            emailRecord.status = EmailSendStatus.failed;
            await this.emailVerifyRepository.save(emailRecord);
          }
        }
        return {
          token: customToken,
          message: Constants.MESSAGES.success.auth.registration_success,
        };
      }
      const { token } = await this.verifyCustomToken(customToken);
      if (!user.profileCompleted) {
        return {
          token,
          refreshToken,
          message: Constants.MESSAGES.success.auth.verified,
        };
      }
      if (user.signinProvider === SignInProvider.google) {
        user.signinProvider = SignInProvider.email;
        await this.userRepository.save(user);
      }
      return {
        message: Constants.MESSAGES.success.auth.signin_success,
        token,
        refreshToken,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      if (error.code) {
        throw new InternalServerErrorException(error.code);
      } else if (error.response?.data?.error?.code) {
        throw new InternalServerErrorException(
          error.response?.data?.error?.message
            .split(':')
            .shift()
            .trim()
            .toLowerCase(),
        );
      }
      throw new InternalServerErrorException(error);
    }
  }

  public async signInWithGoogle(
    tokenReq: string,
  ): Promise<IAuthResponse['data']> {
    const FUNC_NAME = 'signInWithGoogle';
    try {
      const decodedToken = await this.firebaseService
        .getAuth()
        .verifyIdToken(tokenReq);
      const { uid, email, name, picture } = decodedToken;
      const user = await this.userRepository.findOne({
        where: { uid },
      });
      if (!user) {
        const newUser = await this.userRepository.save({
          uid,
          email,
          fullName: name,
          isVerified: true,
          picture,
          signinProvider: SignInProvider.google,
          profileCompleted: false,
          credits: Constants.FREE_REGISTRATION_CREDITS,
        });
        const { token, refreshToken } = await this.getDecodedTokenClaims(
          uid,
          newUser.id,
        );
        return {
          message: Constants.MESSAGES.success.auth.verified,
          token,
          refreshToken,
        };
      } else {
        if (user.signinProvider === SignInProvider.email) {
          await this.userRepository.update(user.id, {
            signinProvider: SignInProvider.google,
          });
        }
      }
      const { token, refreshToken } = await this.getDecodedTokenClaims(
        uid,
        user.id,
      );
      if (!user.profileCompleted) {
        return {
          message: Constants.MESSAGES.success.auth.verified,
          token,
          refreshToken,
        };
      }
      return {
        message: Constants.MESSAGES.success.auth.signin_success,
        token,
        refreshToken,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      if (error.code) {
        throw new InternalServerErrorException(error.code);
      } else if (error.response?.data?.error?.code) {
        throw new InternalServerErrorException(
          error.response?.data?.error?.message
            .split(':')
            .shift()
            .trim()
            .toLowerCase(),
        );
      }
      throw new InternalServerErrorException(error);
    }
  }

  public async signUpComplete(
    registrationInfos: ISignUpCompleteRequest,
  ): Promise<IAuthResponse['data']> {
    const FUNC_NAME = 'signUpComplete';
    try {
      const decodedToken = await this.firebaseService
        .getAuth()
        .verifyIdToken(registrationInfos.token);
      const { userId } = decodedToken;
      const {
        token,
        refreshToken,
        organizationName,
        industry,
        jobTitle,
        referringResource,
        timezone,
      } = registrationInfos;
      if (!userId) throw new InvalidTokenException();
      const user = await this.userRepository.findOne({
        where: { id: userId },
      });
      if (user.profileCompleted) {
        return {
          message: Constants.MESSAGES.success.auth.profile_already_completed,
          token,
          refreshToken,
        };
      }
      await this.userRepository.update(userId, {
        organizationName,
        industry,
        jobTitle,
        referringResource,
        timezone,
        profileCompleted: true,
      });

      await this.userSettingRepository.save({
        isShowGuide: true,
        user,
      });

      await this.userPerformanceStatsRepository.save({
        total: 0,
        valid: 0,
        invalid: 0,
        unknown: 0,
        syntaxError: 0,
        user,
      });
      return {
        message: Constants.MESSAGES.success.auth.signup_success,
        token,
        refreshToken,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      throw new InternalServerErrorException(error);
    }
  }

  public async signOut(token: string): Promise<{ message: string }> {
    const FUNC_NAME = 'signout';
    try {
      this.logger.info(FUNC_NAME);
      const decodedToken = await this.firebaseService
        .getAuth()
        .verifyIdToken(token);
      const { uid } = decodedToken;
      await this.firebaseService.getAuth().revokeRefreshTokens(uid);
      return {
        message: Constants.MESSAGES.success.auth.signout_success,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      throw new InternalServerErrorException(error);
    }
  }

  public async refreshToken(
    refreshTokenReq: string,
  ): Promise<IAuthResponse['data']> {
    const FUNC_NAME = 'refreshToken';
    try {
      const tokenInfos = await this.getRefreshToken(refreshTokenReq);
      const user = await this.userRepository.findOne({
        where: { uid: tokenInfos.uid },
      });
      if (!user) throw new UserNotFoundException();
      const { token, refreshToken } = await this.getDecodedTokenClaims(
        tokenInfos.uid,
        user.id,
      );
      return {
        message: Constants.MESSAGES.success.auth.refresh_token_success,
        token,
        refreshToken,
      };
    } catch (error) {
      this.logger.error(`${FUNC_NAME}: ${error}`);
      if (error.response?.data?.error?.code) {
        throw new InternalServerErrorException(
          error.response?.data?.error?.message
            .split(':')
            .shift()
            .trim()
            .toLowerCase(),
        );
      }
      throw new InternalServerErrorException(error);
    }
  }

  async getRefreshToken(token: string): Promise<{
    uid: string;
    newRefreshToken: string;
    newIdToken: string;
  }> {
    try {
      const url = `${this.configService.get('firebase').secureTokenUri}?key=${
        this.configService.get('firebase').apiKey
      }`;
      const response = await this.httpService.axiosRef.post(url, {
        grant_type: 'refresh_token',
        refresh_token: token,
      });
      const newRefreshToken = response.data.refresh_token;
      const newIdToken = response.data.id_token;
      const uid = response.data.user_id;
      return { uid, newRefreshToken, newIdToken };
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  async verifyCustomToken(
    tokenReq: string,
  ): Promise<Omit<IAuthResponse['data'], 'message'>> {
    try {
      const response = await this.httpService.axiosRef.post(
        `${
          this.configService.get('firebase').authUri
        }/accounts:signInWithCustomToken?key=${
          this.configService.get('firebase').apiKey
        }`,
        {
          token: tokenReq,
          returnSecureToken: true,
        },
      );
      const token = response.data.idToken;
      const refreshToken = response.data.refreshToken;
      return { token, refreshToken };
    } catch (error) {
      throw error;
    }
  }

  async signInWithPassword(
    email: string,
    password: string,
  ): Promise<IFirebaseSignInPassword> {
    try {
      const response = await this.httpService.axiosRef.post(
        `${
          this.configService.get('firebase').authUri
        }/accounts:signInWithPassword?key=${
          this.configService.get('firebase').apiKey
        }`,
        {
          email,
          password,
          returnSecureToken: true,
        },
      );
      const token = response.data.idToken;
      const refreshToken = response.data.refreshToken;
      const localId = response.data.localId;
      return { token, refreshToken, localId };
    } catch (error) {
      throw error;
    }
  }

  async getDecodedTokenClaims(
    uid,
    userId,
  ): Promise<Omit<IAuthResponse['data'], 'message'>> {
    try {
      const customToken = await this.firebaseService
        .getAuth()
        .createCustomToken(uid, {
          userId,
        });
      const decodedVerify = await this.verifyCustomToken(customToken);
      return decodedVerify;
    } catch (error) {
      throw error;
    }
  }

  async updateUserPassword(
    tokenReq: string,
    password: string,
  ): Promise<IFirebaseSignInPassword> {
    try {
      const response = await this.httpService.axiosRef.post(
        `${this.configService.get('firebase').authUri}/accounts:update?key=${
          this.configService.get('firebase').apiKey
        }`,
        {
          idToken: tokenReq,
          password,
          returnSecureToken: true,
        },
      );
      const token = response.data.idToken;
      const refreshToken = response.data.refreshToken;
      const localId = response.data.localId;
      return { token, refreshToken, localId };
    } catch (error) {
      throw error;
    }
  }

  async isLimitSendEmailConfirm(email: string): Promise<boolean> {
    const oneHourAgo = new Date(Date.now() - 60 * 60 * 1000);
    const records = await this.emailVerifyRepository.find({
      where: {
        email,
        status: EmailSendStatus.sent,
        createdAt: MoreThanOrEqual(oneHourAgo),
      },
    });
    return records.length >= Constants.LIMIT_EMAILS_SEND_PER_HOUR;
  }

  async isLimitSendCodeResetPassword(email: string): Promise<boolean> {
    const oneHourAgo = new Date(Date.now() - 60 * 60 * 1000);
    const records = await this.resetPasswordRepository.find({
      where: {
        email,
        status: EmailSendStatus.sent,
        createdAt: MoreThanOrEqual(oneHourAgo),
      },
    });
    return records.length >= Constants.LIMIT_EMAILS_SEND_PER_HOUR;
  }
}
