export enum SignInProvider {
  email = 'email',
  google = 'google.com',
}

export enum FirebaseAuthErrorCode {
  idTokenExpired = 'auth/id-token-expired',
  invalidIdToken = 'auth/invalid-id-token',
  authArgumentError = 'auth/argument-error',
}
