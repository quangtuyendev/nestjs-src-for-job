export interface ILoginInfos {
  email: string;
  password: string;
}
