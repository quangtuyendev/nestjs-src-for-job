export interface IUserDecoded {
  uid: string;
  userId: number;
  email: string;
  token: string;
}
