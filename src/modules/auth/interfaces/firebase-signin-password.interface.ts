export interface IFirebaseSignInPassword {
  token: string;
  refreshToken: string;
  localId: string;
}
