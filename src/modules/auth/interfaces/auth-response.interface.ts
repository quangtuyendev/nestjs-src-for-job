import { HttpStatus } from 'src/common/http-status.utils';

export interface IAuthResponse {
  status: HttpStatus;
  data: {
    message: string;
    token: string;
    refreshToken: string;
  };
}
