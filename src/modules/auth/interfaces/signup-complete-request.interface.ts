export interface ISignUpCompleteRequest {
  token: string;
  refreshToken: string;
  organizationName: string;
  industry: string;
  jobTitle: string;
  referringResource: string;
  timezone: string;
}
