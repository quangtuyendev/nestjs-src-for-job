import { Body, Controller, Post, Req, Res, UsePipes } from '@nestjs/common';
import { Request, Response } from 'express';
import { HttpStatus } from 'src/common/http-status.utils';
import { AuthUtils } from 'src/utils/auth.utils';
import { Helpers } from 'src/utils/helpers.utils';
import { CreateUserDto } from '../user/dtos/create-user.dto';
import { AuthService } from './auth.service';
import { IAuthResponse } from './interfaces/auth-response.interface';
import { ILoginInfos } from './interfaces/login-info.interface';
import { ISignUpCompleteRequest } from './interfaces/signup-complete-request.interface';
import { RefreshTokenValidationPipe } from './validators/refresh-token.validation';
import { RegisterValidationPipe } from './validators/register.validation';
import { SignInWithGoogleValidationPipe } from './validators/signin-with-google.validation';
import { SignUpCompleteValidationPipe } from './validators/signup-complete.validation';
import { VerifyTokenValidationPipe } from './validators/verify.validation';
import { SignInValidationPipe } from './validators/signin.validation';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @UsePipes(new RegisterValidationPipe())
  public async register(@Body() user: CreateUserDto): Promise<{
    status: IAuthResponse['status'];
    data: Omit<IAuthResponse['data'], 'refreshToken'>;
  }> {
    const data = await this.authService.register(user);
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Post('verify')
  @UsePipes(VerifyTokenValidationPipe)
  public async verify(
    @Body('customToken') customToken: string,
  ): Promise<IAuthResponse> {
    const data = await this.authService.verify(customToken);
    return {
      status: HttpStatus.success,
      data,
    };
  }

  @Post('signin')
  @UsePipes(new SignInValidationPipe())
  public async signIn(
    @Body() { email, password }: ILoginInfos,
    @Res() res: Response,
  ): Promise<Response<Partial<IAuthResponse>>> {
    const { token, refreshToken, message } = await this.authService.signIn(
      email,
      password,
    );
    AuthUtils.cookiesSigner(res, token, refreshToken);
    return res.status(200).json({
      status: HttpStatus.success,
      data: {
        token,
        refreshToken,
        message,
      },
    });
  }

  @Post('signin-with-google')
  @UsePipes(new SignInWithGoogleValidationPipe())
  public async signInWithGoogle(
    @Body() { token }: { token: string },
    @Res() res: Response,
  ): Promise<Response<IAuthResponse>> {
    const {
      token: idToken,
      refreshToken,
      message,
    } = await this.authService.signInWithGoogle(token);
    AuthUtils.cookiesSigner(res, idToken, refreshToken);
    return res.status(200).json({
      status: HttpStatus.success,
      data: {
        token: idToken,
        refreshToken,
        message,
      },
    });
  }

  @Post('signup-complete')
  @UsePipes(new SignUpCompleteValidationPipe())
  public async signUpComplete(
    @Body() registrationInfos: ISignUpCompleteRequest,
    @Res() res: Response,
  ): Promise<Response<IAuthResponse>> {
    const { token, refreshToken, message } =
      await this.authService.signUpComplete(registrationInfos);
    AuthUtils.cookiesSigner(res, token, refreshToken);
    return res.status(200).json({
      status: HttpStatus.success,
      data: {
        token,
        refreshToken,
        message,
      },
    });
  }

  @Post('signout')
  public async signOut(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<
    Response<{
      status: IAuthResponse['status'];
      data: Pick<IAuthResponse['data'], 'message'>;
    }>
  > {
    const { token } = req.cookies;
    const data = await this.authService.signOut(token);
    AuthUtils.clearCookie(res, 'token');
    AuthUtils.clearCookie(res, 'refreshToken');
    return res.status(200).json({
      status: HttpStatus.success,
      data,
    });
  }

  @Post('refresh-token')
  @UsePipes(RefreshTokenValidationPipe)
  public async refreshToken(
    @Body('refreshToken') refreshTokenReq: string,
    @Req() req,
    @Res() res,
  ): Promise<{
    status: IAuthResponse['status'];
    data: {
      message: IAuthResponse['data']['message'];
      newIdToken?: string;
      newRefreshToken?: string;
    };
  }> {
    const { message, token, refreshToken } =
      await this.authService.refreshToken(refreshTokenReq);
    const isBrowserRequested = Helpers.isRequestFromBrowser(req);
    AuthUtils.cookiesSigner(res, token, refreshToken);
    const data = isBrowserRequested
      ? { message }
      : {
          newIdToken: token,
          newRefreshToken: refreshToken,
          message,
        };
    AuthUtils.cookiesSigner(res, token, refreshToken);
    return {
      status: HttpStatus.success,
      data,
    };
  }
}
