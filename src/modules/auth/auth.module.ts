import { HttpModule } from '@nestjs/axios';
import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
  forwardRef,
} from '@nestjs/common';
import { Constants } from 'src/constants/Constants';
import { FirebaseModule } from 'src/providers/firebase/firebase.module';
import { KafkaModule } from 'src/providers/kafka/kafka.module';
import { MailerModule } from 'src/providers/mailer/mailer.module';
import { DatabaseModule } from 'src/providers/typeorm/database.module';
import { RateLimiter } from 'src/utils/rate-limiter';
import { DataSource } from 'typeorm';
import { EmailVerifyEntity } from '../email-verify/entities/email-verify.entity';
import { UserPerformanceStatsEntity } from '../performance-stats/entities/performance-stats.entity';
import { ResetPasswordEntity } from '../reset-password/entities/reset-password.entity';
import { UserSettingEntity } from '../setting/entities/setting.entity';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { CreditMiddleware } from 'src/middlewares/credits.middleware';

@Module({
  imports: [
    KafkaModule.register(),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    DatabaseModule,
    FirebaseModule,
    forwardRef(() => UserModule),
    forwardRef(() => MailerModule),
  ],
  providers: [
    AuthService,
    {
      provide: 'EMAIL_VERIFY_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(EmailVerifyEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'RESET_PASSWORD_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(ResetPasswordEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'USER_SETTING_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(UserSettingEntity),
      inject: ['DATA_SOURCE'],
    },
    {
      provide: 'USER_PERFORMANCE_STATS_REPOSITORY',
      useFactory: (dataSource: DataSource) =>
        dataSource.getRepository(UserPerformanceStatsEntity),
      inject: ['DATA_SOURCE', 'RESET_PASSWORD_REPOSITORY'],
    },
  ],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        RateLimiter.apply(
          1,
          Constants.MESSAGES.failed.auth.too_many_registration_request,
        ),
      )
      .forRoutes({ path: 'auth/register', method: RequestMethod.POST })
      .apply(
        RateLimiter.apply(
          5,
          Constants.MESSAGES.failed.auth.too_many_attempts_try_later,
        ),
      )
      .forRoutes({ path: 'auth/verify', method: RequestMethod.POST })
      .apply(
        RateLimiter.apply(
          10,
          Constants.MESSAGES.failed.auth.too_many_attempts_try_later,
        ),
      )
      .forRoutes(
        { path: 'auth/signin', method: RequestMethod.POST },
        {
          path: 'auth/signin-with-google',
          method: RequestMethod.POST,
        },
      )
      .apply(
        RateLimiter.apply(5, Constants.MESSAGES.failed.common.too_many_request),
      )
      .forRoutes(
        { path: 'auth/signup-complete', method: RequestMethod.POST },
        { path: 'auth/signout', method: RequestMethod.POST },
        { path: 'auth/refresh-token', method: RequestMethod.POST },
      );
  }
}
