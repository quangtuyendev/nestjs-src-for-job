import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';

const Schema = Joi.object({
  refreshToken: Joi.string().required(),
});

export class RefreshTokenValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
