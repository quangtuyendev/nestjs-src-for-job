import * as Joi from 'joi';

import { BadRequestException, PipeTransform } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';
import { TIMEZONES } from 'src/constants/timezone';
import { map } from 'lodash';

const Schema = Joi.object({
  token: Joi.string().required(),
  refreshToken: Joi.string().required(),
  organizationName: Joi.string().required(),
  industry: Joi.string().required(),
  jobTitle: Joi.string().required(),
  referringResource: Joi.string().required(),
  timezone: Joi.string()
    .valid(...map(TIMEZONES, (t) => t.value))
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.auth.timezone_required.message,
      'string.empty': Constants.MESSAGES.failed.auth.timezone_empty.message,
      'any.only': Constants.MESSAGES.failed.auth.timezone_only.message,
    }),
});

export class SignUpCompleteValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
