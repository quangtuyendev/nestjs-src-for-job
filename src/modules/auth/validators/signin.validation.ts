import * as Joi from 'joi';

import { PipeTransform, BadRequestException } from '@nestjs/common';
import { Constants } from 'src/constants/Constants';

const Schema = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .trim()
    .min(4)
    .max(255)
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.auth.email_required.message,
      'string.empty': Constants.MESSAGES.failed.auth.email_empty.message,
      'string.email': Constants.MESSAGES.failed.auth.email_invalid.message,
      'string.min': Constants.MESSAGES.failed.auth.email_min_length_4.message,
      'string.max': Constants.MESSAGES.failed.auth.email_max_length_255.message,
    }),
  password: Joi.string()
    .min(8)
    .max(30)
    .pattern(
      new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])'),
    )
    .required()
    .messages({
      'any.required': Constants.MESSAGES.failed.auth.password_required.message,
      'string.empty': Constants.MESSAGES.failed.auth.email_empty.message,
      'string.min':
        Constants.MESSAGES.failed.auth.password_min_length_8.message,
      'string.max':
        Constants.MESSAGES.failed.auth.password_max_length_30.message,
      'string.pattern.base':
        Constants.MESSAGES.failed.auth.password_complexity_requirement_not_met
          .message,
    }),
});

export class SignInValidationPipe implements PipeTransform<any> {
  public async transform(value: any): Promise<any> {
    try {
      await Schema.validateAsync(value);
      return value;
    } catch (error) {
      const errorMessages = error.details.map((d) => d.message).join();
      throw new BadRequestException(JSON.stringify({ message: errorMessages }));
    }
  }
}
