import { Response } from 'express';
import { Constants } from 'src/constants/Constants';

export class AuthUtils {
  static cookiesSigner(
    res: Response,
    idToken: string,
    refreshToken: string,
  ): void {
    const sevenDaysInMilliseconds = 7 * 24 * 60 * 60 * 1000;

    res.cookie('token', idToken, {
      httpOnly: false,
      sameSite: 'none',
      secure: true,
      domain: Constants.COOKIE_DOMAIN_SIGNED,
      path: '/',
      signed: false,
      maxAge: sevenDaysInMilliseconds,
    });

    res.cookie('refreshToken', refreshToken, {
      httpOnly: false,
      sameSite: 'none',
      secure: true,
      domain: Constants.COOKIE_DOMAIN_SIGNED,
      path: '/',
      signed: false,
      maxAge: sevenDaysInMilliseconds,
    });
  }

  static clearCookie(res: Response, cookieName: string): void {
    res.clearCookie(cookieName, {
      domain: Constants.COOKIE_DOMAIN_SIGNED,
      path: '/',
    });
  }

  static getVerifyUrl(token: string): string {
    const verifyUrl = `${Constants.APP_BASE_URI}/signup?token=${token}`;
    return verifyUrl;
  }
}
