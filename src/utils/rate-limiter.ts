import { IHttpErrorResponse } from 'src/common/http-error-response.common';
import { HttpStatus } from 'src/common/http-status.utils';
import { rateLimit, RateLimitRequestHandler } from 'express-rate-limit';

export class RateLimiter {
  static apply(
    numberOfRequests: number,
    error: IHttpErrorResponse,
    time?: number,
  ): RateLimitRequestHandler {
    return rateLimit({
      windowMs: (time || 1) * 60 * 1000,
      max: numberOfRequests,
      message: {
        status: HttpStatus.failed,
        error,
      },
    });
  }
}
