import { spawn } from 'child_process';
import dns from 'dns';
import * as fs from 'fs';
import { find, forEach, isUndefined, map, omitBy } from 'lodash';
import * as momentTz from 'moment-timezone';

import { Request } from 'express';
import { join } from 'path';
import { Constants } from 'src/constants/Constants';
import { ChartBreakdowns } from 'src/modules/file/enums/chart-breakdown-fitler.enum';
import { EmailVerifyStatus } from 'src/modules/file/enums/email-verify-status.enum';
import { ICookieConfig } from 'src/modules/file/interfaces/common.interface';

export class Helpers {
  static sleep(ms: number): unknown {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  static removeAllButLast(string: string, chars: string): string {
    const parts = string.split(chars);
    return parts.slice(0, -1).join('') + chars + parts.slice(-1);
  }

  static findEmailHeaderColumn(columns: object): string {
    for (const columnName in columns) {
      for (const emailHeader in Constants.EMAIL_HEADERS) {
        if (
          columnName.toLowerCase().replace(/\s/g, '') ===
          emailHeader.toLowerCase().trim()
        ) {
          return columnName;
        }
      }
    }
  }

  static changeFileExt(fileName: string): string {
    return fileName.split('.').slice(0, -1).join('.') + '.csv';
  }

  static removeUndefinedProperties(obj: object): object {
    const clean = omitBy(obj, isUndefined);
    return clean;
  }

  static combineValue(arr: any[], key: string) {
    return {
      label: key,
      data: arr
        .filter((row) => row.some((obj) => obj.label === key))
        .map((row) => row.find((obj) => obj.label === key).data),
    };
  }

  static addMissingTime(
    chartData: { values: any[]; labels: string[] },
    breakdown: string,
    timeInfo: {
      startDate: Date;
      endDate: Date;
      startHour: number;
      endHour: number;
      timezone: string;
    },
  ): {
    values: any[];
    labels: string[];
    breakdown: string;
  } {
    const { startDate, endDate, startHour, endHour, timezone } = timeInfo;
    if (breakdown === ChartBreakdowns.hour) {
      const hours = Array.from(
        { length: endHour - startHour + 1 },
        (_, i) => startHour + i,
      );
      hours.forEach((hour) => {
        const fullYear = momentTz(startDate).tz(timezone).format('YYYY-MM-DD');
        const label = `${fullYear} ${hour.toString().padStart(2, '0')}`;
        if (!chartData.labels.includes(label)) {
          chartData.labels.push(label);
          chartData.values.forEach((item) => {
            item.data.splice(hour, 0, 0);
          });
        }
      });
      chartData.labels = chartData.labels
        .map((l) => momentTz.tz(l, 'YYYY-MM-DD HH', timezone).format())
        .sort();
      chartData.values = chartData.values.sort((a, b) =>
        a.label.localeCompare(b.label),
      );
      return { breakdown, ...chartData };
    }
    const dateLabels = map(
      chartData.labels,
      (l) => new Date(momentTz.tz(l, 'YYYY-MM-DD', timezone).format()),
    );
    const labels = [];
    const values = [];
    const currentDate = new Date(startDate);
    while (currentDate <= endDate) {
      const isoDate = currentDate.toISOString();
      labels.push(isoDate);
      const dataIndex = dateLabels.findIndex((label) => {
        return label.toDateString() === currentDate.toDateString();
      });

      if (dataIndex !== -1) {
        values.push(
          map(chartData.values, (v) => ({
            label: v.label,
            data: v.data[dataIndex],
          })),
        );
      } else {
        values.push(
          map(chartData.values, (v) => ({
            label: v.label,
            data: 0,
          })),
        );
      }
      currentDate.setDate(currentDate.getDate() + 1);
    }
    const stats = ['valid', 'avatarCount', 'invalid', 'unknown', 'total'];
    chartData.values = map(stats, (s) => Helpers.combineValue(values, s));
    chartData.labels = map(labels, (l) => {
      return momentTz(l).tz(timezone).format();
    });
    return { breakdown, ...chartData };
  }

  static removeFilesInDir(dirPath: string): Promise<void> {
    return new Promise((resolve, reject) => {
      fs.readdir(dirPath, (error, files) => {
        if (error) {
          reject(error);
          return;
        }
        const promises = map(files, (file) => {
          const filePath = join(dirPath, file);
          return new Promise<void>((resolve, reject) => {
            fs.unlink(filePath, (error) => {
              if (error) {
                reject(error);
                return;
              }
              resolve();
            });
          });
        });
        Promise.all(promises)
          .then(() => resolve())
          .catch((error) => reject(error));
      });
    });
  }

  static getFileSizeInBytes(filePath: string): number {
    const stats = fs.statSync(filePath);
    const fileSizeInBytes = stats.size;
    return fileSizeInBytes;
  }

  static isRequestFromBrowser(req: Request): boolean {
    const userAgent = req.headers['user-agent'];
    const referer = req.headers['referer'];
    const origin = req.headers['origin'];
    if (
      userAgent &&
      referer &&
      origin &&
      userAgent.includes('Mozilla') &&
      referer.includes(Constants.APP_HOSTNAME) &&
      origin.includes(Constants.APP_HOSTNAME)
    ) {
      return true;
    } else {
      return false;
    }
  }

  static isFileValid(req: Request): boolean {
    // const allowedStreamTypes = ['application/octet-stream'];
    // if (!allowedStreamTypes.includes(req.file.mimetype)) {
    //   return false;
    // }
    return true;
  }

  static isDomainWhitelisted(req: Request): boolean {
    const whitelist = [Constants.APP_BASE_URI.split('//')[1]];
    const referer = req.get('Referer');
    if (!referer) {
      return false;
    }
    const domain = referer.split('/')[2];
    if (!domain) {
      return false;
    }
    return whitelist.includes(domain);
  }

  static checkUserCredits(
    userCredit: number,
    requiredCredit: number,
  ): {
    status: boolean;
    requiredAmount: number;
  } {
    return {
      status: userCredit - requiredCredit >= 0 ? true : false,
      requiredAmount: requiredCredit - userCredit,
    };
  }

  static isEmail(email: string): any[] | null {
    const emailRegex =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    const antiSpecialCharacterRegex =
      /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/i;
    return (
      String(email).toLowerCase().match(emailRegex) &&
      String(email).toLowerCase().match(antiSpecialCharacterRegex)
    );
  }

  static additionalVerifyColumns(
    emails: string[],
    status: EmailVerifyStatus,
    emailColumnName: string,
  ): string[] {
    return map(emails, (e) => {
      const isValid = Helpers.isEmail(e[emailColumnName]);
      const domain = e[emailColumnName].split('@').pop();
      const isFreeEmail =
        Constants.FREE_EMAILS_DOMAINS.indexOf(domain.toLowerCase()) !== -1;
      e[`${Constants.WEBSITE_NAME} Verification Status`] = status;
      e[`${Constants.WEBSITE_NAME} Free Account Status`] = isFreeEmail
        ? 'Yes'
        : isValid
        ? 'No'
        : '';
      e[`${Constants.WEBSITE_NAME} Account`] = isValid
        ? e[emailColumnName].split('@').slice(0, -1).join('@').toLowerCase()
        : '';
      e[`${Constants.WEBSITE_NAME} Domain`] = isValid ? domain : '';
      return e;
    });
  }

  static duplicateLineRemover(filename: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const command = 'awk';
      const args = [
        '-i',
        'inplace',
        '-F,',
        '!seen[$0]++{if(NR==1){gsub(/' +
          Constants.DOMAIN_NAME +
          '_[^,]*/,"",$0)} print}',
        filename,
      ];
      const process = spawn(command, args);
      process.on('error', (error) => {
        reject(error);
      });
      process.on('exit', (code, signal) => {
        if (code === 0) {
          resolve();
        } else {
          reject(signal);
        }
      });
    });
  }

  static emailsSyntaxValidAndInvalid(
    rows: string[],
    emailColumnName: string,
  ): {
    validRows: string[];
    invalidRows: string[];
  } {
    const validRows = [];
    const invalidRows = [];
    forEach(rows, (r) => {
      const isValid = Helpers.isEmail(r[emailColumnName]);
      if (isValid) {
        validRows.push(r);
      } else {
        invalidRows.push(r);
      }
    });
    return {
      validRows,
      invalidRows,
    };
  }

  static checkTypoError(email: string): boolean {
    if (
      Constants.TYPO_ERROR_DOMAINS.map((e) => e.trim().toLowerCase()).includes(
        email.split('@').pop().toLowerCase(),
      )
    ) {
      return true;
    } else {
      return false;
    }
  }

  static cleanGmailDomain(email: string) {
    return Helpers.removeAllButLast(
      email.toLowerCase().replace('@googlemail.com', '@gmail.com'),
      '.',
    );
  }

  static async checkDomainMXRecord(email: string): Promise<boolean> {
    const domain = email.split('@').pop().toLowerCase();
    try {
      const addresses = await dns.promises.resolveMx(domain);
      return addresses.length > 0;
    } catch (error) {
      return false;
    }
  }

  static setCookieUsed(
    cookies: ICookieConfig[],
    authorization: string,
    isError: boolean,
  ): void {
    const updatedCookies = cookies.map((cookie: ICookieConfig) => {
      if (cookie.authorization === authorization) {
        cookie.lastUsed = Date.now();
        if (isError) {
          cookie.retry += 1;
        }
      }
      return cookie;
    });
    fs.writeFileSync(
      join('.') + '/cookies.json',
      JSON.stringify(updatedCookies),
    );
  }

  static getChargeCredits(file: {
    valid: number;
    invalid: number;
    syntaxError: number;
  }): number {
    const amount = file.valid + file.invalid + file.syntaxError;
    return amount;
  }

  static getNamePrice(name: string): number | null {
    const { monthly } = Constants.PRICING;
    const selected = find(monthly, { name });
    return selected ? selected.price : null;
  }

  static getRandomCancellationReason(): string {
    const reasons = [
      'They no longer need the service because their circumstances have changed.',
      'They found a more suitable alternative that better meets their needs.',
      'They successfully achieved their goal with the service and no longer require it.',
      'They need to cut back on expenses and are prioritizing other areas of their budget.',
    ];
    const randomIndex = Math.floor(Math.random() * reasons.length);
    return reasons[randomIndex];
  }
}
