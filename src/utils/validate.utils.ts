export class UtilsValidate {
  static passwordContainsSpaces(password: string): boolean {
    const spaceRegex = /\s/;
    return spaceRegex.test(password);
  }
}
