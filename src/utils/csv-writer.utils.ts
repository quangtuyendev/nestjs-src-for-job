import { stringify } from 'csv-stringify';
import * as fs from 'fs';

export class CsvWriter {
  static write(
    filePath: string,
    header: { header: string; key: string }[],
    records: any[],
  ): void {
    stringify(
      records,
      { header: true, columns: header },
      (err, output: string) => {
        if (err) {
          throw err;
        }
        try {
          fs.appendFileSync(filePath, output);
        } catch (error) {
          throw error;
        }
      },
    );
  }
}
