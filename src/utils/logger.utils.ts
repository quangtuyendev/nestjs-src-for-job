import { createLogger, format, transports } from 'winston';
import util from 'util';
import { join } from 'path';

const { combine, timestamp, printf, colorize } = format;

const logFormat = printf(({ level, message, timestamp, stack, moduleName }) => {
  return `${timestamp} ${colorize().colorize(
    level,
    `[${moduleName}_${level}]`,
  )}:${message}${stack ? `\n${stack}` : ''}`;
});

class Logger {
  private logger: any;
  private moduleName: string;

  constructor(private name: string) {
    this.logger = createLogger({
      level: 'silly',
      format: combine(timestamp(), format.errors({ stack: true }), logFormat),
      transports: [
        new transports.Console(),
        new transports.File({
          filename: join('.') + '/src/logs/index.log',
        }),
      ],
      exceptionHandlers: [
        new transports.File({
          filename: join('.') + '/src/logs/index.log',
        }),
      ],
      rejectionHandlers: [
        new transports.File({
          filename: join('.') + '/src/logs/index.log',
        }),
      ],
    });

    process.on('unhandledRejection', (reason, promise) => {
      this.logger.error(
        `Unhandled rejection at: ${util.inspect(
          promise,
        )} reason: ${util.inspect(reason)}`,
      );
    });

    process.on('uncaughtException', (error) => {
      this.logger.log(`Uncaught exception at: ${error.stack}`);
    });

    this.moduleName = this.name.split('/').slice(-2).join('\\');
  }

  log(level: string, message: string) {
    this.logger.log({ level, message, moduleName: this.moduleName });
  }

  error(message: string) {
    this.logger.error({ message, moduleName: this.moduleName });
  }

  warn(message: string) {
    this.logger.warn({ message, moduleName: this.moduleName });
  }

  info(message: string) {
    this.logger.info({ message, moduleName: this.moduleName });
  }

  verbose(message: string) {
    this.logger.verbose({ message, moduleName: this.moduleName });
  }

  debug(message: string) {
    this.logger.debug({ message, moduleName: this.moduleName });
  }

  silly(message: string) {
    this.logger.silly({ message, moduleName: this.moduleName });
  }
}

export default Logger;
