export interface IRateLimit {
  numberOfRequest: number;
  error: { message: string };
}
