import { HttpStatus } from './http-status.utils';

export interface IHttpResponse {
  status: HttpStatus;
  data: { message: string };
}
