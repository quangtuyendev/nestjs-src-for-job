export interface IHttpErrorResponse {
  code?: number;
  message: string;
}
