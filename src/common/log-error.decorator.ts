import Logger from 'src/utils/logger.utils';

export function LogError(fileName): MethodDecorator {
  const logger: Logger = new Logger(fileName);
  return (
    _: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    const originalMethod = descriptor.value;

    descriptor.value = async function (...args: any[]) {
      try {
        return await originalMethod.apply(this, args);
      } catch (error) {
        logger.error(`${String(propertyKey)}: ${error.stack}`);
        throw error;
      }
    };

    return descriptor;
  };
}
