import { SetMetadata } from '@nestjs/common';
import { IRateLimit } from './rate-limit.interface';

export const RateLimit = (
  numberOfRequest: IRateLimit['numberOfRequest'],
  error: IRateLimit['error'],
) => SetMetadata('rateLimit', { numberOfRequest, error });
