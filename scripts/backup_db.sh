#!/bin/bash

# Get access token from refresh token (Google Drive API v3)
CLIENT_ID=<client_id>
CLIENT_SECRET=<client_secret>
REFRESH_TOKEN=<refresh_token>
CONTAINER_NAME=<container_name>
FOLDER_ID=<folder_id>

# Install required packages in container
docker exec $CONTAINER_NAME apt-get update
docker exec $CONTAINER_NAME apt-get install -y mongodb-org jq zip curl

# Backup MongoDB database in container
docker exec $CONTAINER_NAME mongodump --db emailVerification --username <username> --password <password> --authenticationDatabase admin --out /home/app/backup_db

# Zip MongoDB exported folder
docker exec $CONTAINER_NAME zip -r /home/app/backup_db/backup.zip /home/app/backup_db

# Get access token using Google Drive API
access_token=$(curl -s -X POST "https://oauth2.googleapis.com/token" \
-H "Content-Type: application/x-www-form-urlencoded" \
-d "client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&refresh_token=$REFRESH_TOKEN&grant_type=refresh_token" | jq -r '.access_token')

docker exec $CONTAINER_NAME curl -X POST \
  -H "Authorization: Bearer ${access_token}" \
  -F "metadata={ \
               name : 'backup_$(date +%Y%m%d%H%M%S).zip', \
               mimeType : 'application/zip', \
               parents: ['${FOLDER_ID}'] \
               };type=application/json;charset=UTF-8" \
  -F "file=@/home/app/backup_db/backup.zip;type=application/zip" \
  "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart"

# Remove backup file
docker exec $CONTAINER_NAME rm /home/app/backup_db/backup.zip

# Add a crontab job to run this script every 30 minutes
(crontab -l 2>/dev/null; echo "*/30 * * * * /bin/bash /home/app/backup_db/script.sh") | crontab -
