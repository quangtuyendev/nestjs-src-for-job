const { CompressionTypes } = require("kafkajs");
const { map } = require("lodash");
const { KAFKA_CONFIG, EMAIL_SEND_STATUS } = require("../constants");
const EmailVerify = require("../models/emailVerify.model");
const logger = require("../utils/logger")(__filename);
const appProducer = require("../queue/producer")();

async function sendEmailVerifyErrorProducer() {
  const FUNC_NAME = "sendEmailVerifyErrorProducer";
  logger.debug(`${FUNC_NAME} running...`);
  try {
    const oneHourAgo = new Date(Date.now() - 60 * 60 * 1000);
    const records = await EmailVerify.find({
      status: EMAIL_SEND_STATUS.failed,
      createdAt: { $gt: oneHourAgo },
    });
    await appProducer
      .then((producer) => {
        producer
          .send({
            topic: KAFKA_CONFIG.topics.emailConfirm.name,
            compression: CompressionTypes.GZIP,
            messages: map(records, (r) => ({
              value: JSON.stringify({ ...r._doc }),
            })),
          })
          .catch((error) => {
            logger.error(`${FUNC_NAME}: ${error}`);
          });
      })
      .catch((error) => {
        throw new Error(error);
      });
  } catch (error) {
    logger.error(`${FUNC_NAME}: ${error}`);
  }
}

module.exports = sendEmailVerifyErrorProducer;
