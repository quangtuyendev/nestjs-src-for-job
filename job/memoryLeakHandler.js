const config = require("../config");
const { FILE_STATUS, FILE_ERROR_REASON } = require("../constants");
const logger = require("../utils/logger")(__filename);

const MongoClient = require("mongodb").MongoClient;

const client = new MongoClient(config.DATABASE_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

client.connect(async (err) => {
  const FUNC_NAME = "memoryLeakHandler";
  logger.info(`${FUNC_NAME}: running...`);
  if (err) throw err;
  const collection = client.db().collection("fileslists");

  const filter = {
    status: { $in: [FILE_STATUS.chunksUploaded, FILE_STATUS.mergeFileFailed] },
    fileSize: { $gte: 100000000 }, // 100MB
    isDeleted: false,
  };

  const update = {
    $set: {
      status: FILE_STATUS.fileError,
      errorReason: FILE_ERROR_REASON.readingError,
      lastModifiedStatus: Date.now(),
    },
  };

  const sort = { createdAt: 1 };
  const limit = 2;

  const cursor = await collection
    .find(filter)
    .sort(sort)
    .limit(limit)
    .toArray();

  let i = 0;
  for (const doc of cursor) {
    await collection.updateOne({ _id: doc._id }, update);
    i++;
  }
  client.close();
});
