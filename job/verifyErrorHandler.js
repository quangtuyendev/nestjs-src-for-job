const { FILE_STATUS, FILE_ERROR_REASON } = require("../constants");
const logger = require("../utils/logger")(__filename);
const FilesList = require("../models/file.model");

async function verifyErrorHandler() {
  const FUNC_NAME = "verifyErrorHandler";
  logger.debug(`${FUNC_NAME}: running...`);
  try {
    const filter = {
      status: FILE_STATUS.fileVerifying,
      createdAt: { $lte: new Date(Date.now() - 8 * 60 * 60 * 1000) }, // 8 hours ago
      isDeleted: false,
    };

    const update = {
      $set: {
        status: FILE_STATUS.fileError,
        errorReason: FILE_ERROR_REASON.readingError,
        lastModifiedStatus: Date.now(),
      },
    };

    const files = await FilesList.find(filter);

    let i = 0;
    for (const file of files) {
      await FilesList.updateOne({ _id: file._id }, update);
      i++;
    }
  } catch (error) {
    logger.error(`${FUNC_NAME}: ${error}`);
  }
}

module.exports = verifyErrorHandler;
