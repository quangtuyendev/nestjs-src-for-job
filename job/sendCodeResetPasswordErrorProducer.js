const { CompressionTypes } = require("kafkajs");
const { map } = require("lodash");
const { KAFKA_CONFIG, EMAIL_SEND_STATUS } = require("../constants");
const ResetPassword = require("../models/resetpasswords.model");
const logger = require("../utils/logger")(__filename);
const appProducer = require("../queue/producer")();

async function sendCodeResetPasswordErrorProducer() {
  const FUNC_NAME = "sendCodeResetPasswordErrorProducer";
  logger.debug(`${FUNC_NAME} running...`);
  try {
    const oneHourAgo = new Date(Date.now() - 60 * 60 * 1000);
    const records = await ResetPassword.find({
      status: EMAIL_SEND_STATUS.failed,
      createdAt: { $gt: oneHourAgo },
    });
    await appProducer
      .then((producer) => {
        producer
          .send({
            topic: KAFKA_CONFIG.topics.emailResetPassword.name,
            compression: CompressionTypes.GZIP,
            messages: map(records, (r) => ({
              value: JSON.stringify({ ...r._doc }),
            })),
          })
          .catch((error) => {
            logger.error(`${FUNC_NAME}: ${error}`);
          });
      })
      .catch((error) => {
        throw new Error(error);
      });
  } catch (error) {
    logger.error(`${FUNC_NAME}: ${error}`);
  }
}

module.exports = sendCodeResetPasswordErrorProducer;
