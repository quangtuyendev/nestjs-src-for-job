const { CompressionTypes } = require("kafkajs");
const { map } = require("lodash");
const { KAFKA_CONFIG, FILE_STATUS } = require("../constants");
const FilesList = require("../models/file.model");
const logger = require("../utils/logger")(__filename);
const appProducer = require("../queue/producer")();

async function mergeFileErrorProducer() {
  const FUNC_NAME = "mergeFileErrorProducer";
  logger.debug(`${FUNC_NAME} running...`);
  try {
    const files = await FilesList.find(
      {
        status: FILE_STATUS.mergeFileFailed,
      },
      {
        folderName: 1,
        fileName: 1,
        originFileName: 1,
        _id: 1,
      }
    );
    await appProducer
      .then((producer) => {
        producer
          .send({
            topic: KAFKA_CONFIG.topics.chunksUploaded.name,
            compression: CompressionTypes.GZIP,
            messages: map(files, (f) => ({
              value: JSON.stringify({ ...f._doc }),
            })),
          })
          .catch((error) => {
            logger.error(`${FUNC_NAME}: ${error}`);
          });
      })
      .catch((error) => {
        throw new Error(error);
      });
  } catch (error) {
    logger.error(`${FUNC_NAME}: ${error}`);
  }
}

module.exports = mergeFileErrorProducer;
