const cron = require("node-cron");
const logger = require("../utils/logger")(__filename);
const mergeFileErrorProducer = require("./mergeFileErrorProducer");
const sendEmailVerifyErrorProducer = require("./sendEmailVerifyErrorProducer");
const sendCodeResetPasswordErrorProducer = require("./sendCodeResetPasswordErrorProducer");
const verifyErrorHandler = require("./verifyErrorHandler");
const retryHandler = require("./retryHandler");

function main() {
  const FUNC_NAME = "main";
  logger.debug(`${FUNC_NAME} running...`);
  cron.schedule("*/30 * * * *", () => {
    return Promise.all([mergeFileErrorProducer(), verifyErrorHandler()]);
  });
  cron.schedule("*/10 * * * *", () => {
    return Promise.all([
      sendEmailVerifyErrorProducer(),
      sendCodeResetPasswordErrorProducer(),
      retryHandler(),
    ]);
  });
}

module.exports = main;
