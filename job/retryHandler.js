const { FILE_STATUS } = require("../constants");
const logger = require("../utils/logger")(__filename);
const FilesList = require("../models/file.model");

async function retryHandler() {
  const FUNC_NAME = "retryHandler";
  logger.info(`${FUNC_NAME}: running...`);
  try {
    const filter = {
      status: {
        $in: [
          FILE_STATUS.chunksUploaded,
          FILE_STATUS.fileVerifying,
          FILE_STATUS.fileVerifySuccess,
        ],
      },
      lastModifiedStatus: {
        $lte: new Date(Date.now() - 3 * 60 * 60 * 1000),
      },
      isDeleted: false,
    };

    const files = await FilesList.find(filter);
    let i = 0;
    for (let file of files) {
      const record = await FilesList.findOne({ _id: file._id });
      let status;
      switch (record.status) {
        case FILE_STATUS.chunksUploaded:
          status = FILE_STATUS.mergeFileFailed;
          break;
        case FILE_STATUS.fileVerifying:
          status = FILE_STATUS.fileVerifyFailed;
          break;
        case FILE_STATUS.fileVerifySuccess:
          status = FILE_STATUS.makeFileFailed;
          break;
        default:
          break;
      }
      const result = await FilesList.updateOne(
        { _id: record._id },
        {
          $set: {
            status,
            lastModifiedStatus: Date.now(),
          },
        }
      );
      if (result.matchedCount) {
        i++;
      }
    }
  } catch (error) {
    logger.error(`${FUNC_NAME}: ${error}`);
  }
}

module.exports = retryHandler;
